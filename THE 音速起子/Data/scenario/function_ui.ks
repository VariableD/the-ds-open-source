;------------------------------------------------------------------------------------------------
;界面的描绘
;------------------------------------------------------------------------------------------------
;-------------------------------------------
;对话窗口范围显示
;-------------------------------------------
[iscript]
class getMargin extends KAGLayer
{
var DrawTimer=new Timer(CheckMargin,"");

   function getMargin(win, par)
 {
		super.Layer(win, par);
		left=0;
		top=0;
		width=256;
		height=192;
		visible=true;
		hitType = htMask;
		hitThreshold =0;
		DrawTimer.interval =10;
  } 
//按下
function onMouseDown(x, y, button, shift)
{
   if (button==mbLeft)
   {
     f.参数.x1=this.cursorX;
     f.参数.y1=this.cursorY;
      DrawTimer.enabled=true;
    }
}
//检测
function CheckMargin()
{
     f.参数.x2=this.cursorX;
     f.参数.y2=this.cursorY;
     drawMargin();
}
//抬起
function onMouseUp(x, y, button, shift)
{
   if (button==mbLeft)
   {
      DrawTimer.enabled=false;
    }
}
//消除
function finalize() 
{
	this.visible=false;
	super.finalize(...);
}
function drawMargin()
{

     var x1=f.参数.x1;
     var y1=f.参数.y1;
     var x2=f.参数.x2;
     var y2=f.参数.y2;
     var width=x2-x1;
     var height=y2-y1;
     fillRect(0,0,256,192,0xFFFFFFFF);
     fillRect(x1,y1,width,1,0xFFFF0000);
     fillRect(x1,y2,width,1,0xFFFF0000);
     fillRect(x1,y1,1,height,0xFFFF0000);
     fillRect(x2,y1,1,height,0xFFFF0000);
}
}
[endscript]
;-------------------------------------------
;右键菜单选中
;-------------------------------------------
[iscript]
function GetRclick(left,top,width,height)
{
  var x=kag.fore.base.cursorX;
  var y=kag.fore.base.cursorY;
  if (x>left && x<left+width && y>top && y<top+height)
  {
     //根据位置选中
     var num=(y-top)\20;
     f.当前脚本行=num+25*(f.当前脚本页-1);
     if (f.当前脚本行>f.脚本内容.count-1) {f.当前脚本行=f.脚本内容.count-1;}
     return true;
  }

     //什么也不做
     return false;

}
[endscript]
;------------------------------------------------------------------------------------------------
;翻页和刷新
;------------------------------------------------------------------------------------------------
[iscript]
//计算页数
function countPage(count,line)
{
   //计算页数
   f.page=count\line;
   if (count%line>0) f.page++;
   //调整值
   if (f.curpage>f.page) f.curpage=f.page;
}

//上
function page_up()
{
    if (f.window!='script')
    {
      if (f.curpage>1) f.curpage--;
      update();
    }
    else if (f.当前脚本页>1 && f.window=='script')
    {
       f.当前脚本页--;
       f.当前脚本行=(f.当前脚本页-1)*25;
       update();
    }
}

//下
function page_down()
{
    if (f.window!='script')
    {
      if (f.curpage<f.page) f.curpage++;
      update();
    }
    else if (f.当前脚本页<f.脚本页数 && f.window=='script')
    {
       f.当前脚本页++;
       f.当前脚本行=(f.当前脚本页-1)*25;
      update();
    }
}

//拖动
function page_scroll()
{
     if (f.window!="script")
     {
        if (kag.fore.base.cursorY<kag.fore.layers[7].top) page_up();
        if (kag.fore.base.cursorY>(int)kag.fore.layers[7].top+(int)kag.fore.layers[7].height) page_down();
     }
     else
     {
        if (kag.fore.base.cursorY<kag.fore.layers[2].top) page_up();
        if (kag.fore.base.cursorY>(int)kag.fore.layers[2].top+(int)kag.fore.layers[2].height) page_down();
     }
}

//描绘
function update()
{
    switch(f.window)
    {
       //为选择工程窗口的情况
        case "project":
                drawBox("文件夹",kag.fore.layers[5],20,50,320,10,f.project,f.curpage-1);
                break;
       //为一般文件选择
        case "file":
                updateFile();
                break;
        //为图片
        case "picture":
                updatePicture();
                break;
       //为脚本
       case "script":
                updateScript();
                break;
    }
}
[endscript]
;------------------------------------------------------------------------------------------------
;很少用的描绘组
;------------------------------------------------------------------------------------------------
;---------------------------------------------------------------
;描绘按钮
;---------------------------------------------------------------
[iscript]
function drawButtonCaption(caption,size=15)
{
var button=kag.current.links[kag.current.links.count-1].object;
button.font.face = "幼圆"; //改字体的地方...
button.font.height = size;     //文字大小设定
var w = button.font.getTextWidth(caption); // 取得要描绘文字的宽度
var x = (button.width - w) \ 2;    // 在按钮中央显示文字
var y = (button.height - size) \ 2-1;   //   文字在按钮上的y位置（左上角起算）
// 按钮「通常状態」部分文字显示
button.drawText(x,                           y, caption, 0x000000);
// 按钮「按下状態」部分文字显示
button.drawText(x+button.width,              y, caption, 0x000000);
// 按钮「选中状態」部分文字显示
button.drawText(x+button.width+button.width, y, caption, 0x000000);
}
[endscript]

;--------------------------------------------------------------
;主界面页背景
;--------------------------------------------------------------
[iscript]
function drawPageBoard(x=1)
{
 //底板
  with(kag.fore.layers[0])
  {
     .visible=true;
     .left=10;
     .top=81;
     .width=1000;
     .height=610;
     //底板
     .fillRect(0,0,1000,610,0xFFD4D0C8);
     //描边
     .fillRect(0,0,1000,1,0xFFFFFFFF);
     .fillRect(0,0,1,610,0xFFaca899);
     .fillRect(1,0,1,610,0xFFFFFFFF);
     .fillRect(0,609,1000,1,0xFFaca899);
     .fillRect(999,0,1,610,0xFFaca899);
     //当前显示第几页，补描绘按钮下空白
     .fillRect(x,0,68,1,0xFFD4D0C8);
  }
  //编辑器界面
  with(kag.fore.messages[1])
  {
     .visible=true;
     .left=10;
     .top=81;
     .width=1000;
     .height=610;
  }
}
[endscript]
;--------------------------------------------------------------
;凹槽
;--------------------------------------------------------------
[iscript]
function drawFill(x,y,width,height,layer=kag.fore.layers[0])
{
  with(layer)
  {
     //左
    .fillRect(x,y,1,height,0xFFaca899);
     //右
    .fillRect(x+width,y,1,height,0xFFFFFFFF);
     //上
    .fillRect(x,y,width,1,0xFFaca899);
     //下
    .fillRect(x,y+height,width,1,0xFFFFFFFF);
  }
}
[endscript]
;--------------------------------------------------------------
;功能分隔栏
;--------------------------------------------------------------
[iscript]
function drawFrame(title,line=7,x=15,y=15,layer=kag.fore.layers[0],width=314)
{
var height=(line+1)*30;

  with(layer)
  {
     .font.height=15;
  //上
      .fillRect(x,y,width,1,0xFFaca899);
      .fillRect(x,y+1,width,1,0xFFFFFFFF);
  //下
      .fillRect(x,y+height,width+1,1,0xFFaca899);
      .fillRect(x,y+height+1,width+2,1,0xFFFFFFFF);
  //左
      .fillRect(x,y,1,height,0xFFaca899);
      .fillRect(x+1,y+1,1,height-1,0xFFFFFFFF);
  //右
      .fillRect(x+width,y,1,height+1,0xFFaca899);
      .fillRect(x+width+1,y,1,height+1,0xFFFFFFFF);
  //标题描绘区域
      .fillRect(x+10,y,layer.font.getTextWidth(title)+10,1,0xFFD4D0C8);
      .fillRect(x+10,y+1,layer.font.getTextWidth(title)+10,1,0xFFD4D0C8);
  //文字
      .drawText(x+15,y-6, title, 0x000000);
  }
}
[endscript]
;------------------------------------------------------------------------------------------------
;常用控件组
;------------------------------------------------------------------------------------------------
[iscript]
//--------------------------------------------------------------
//基本edit框描绘[标题,处理值,位置,条件]
//--------------------------------------------------------------
function drawEdit(title,value,posX,posY,length=224,cond='')
{
  //描绘提示文字

  kag.tagHandlers.locate(%["x"=>posX,"y"=>posY]);
  kag.tagHandlers.ch(%["text"=>title]);
  
  var width=kag.current.font.getTextWidth(title);
  kag.tagHandlers.locate(%["x"=>posX+width+20,"y"=>posY]);
  
  if (value!=void)
  {
      //描绘编辑框
      var result=cond!;
      var setting=new Dictionary();
      setting.name=value;
      setting.length=length-width;
       //值为伪时，设定特殊颜色
       if (result==false && cond!=void)
       {
            setting.bgcolor=0xD4D0C8;
            setting.color=0xD4D0C8;
        }
       kag.tagHandlers.edit(setting);
  }
}
//--------------------------------------------------------------
//附加连接框□的描绘、图片选择窗口[处理值，路径，联动宽高]
//--------------------------------------------------------------
function drawLink_Picwin(value,path,withwidth='',withheight='')
{
   //空格
   kag.tagHandlers.ch(%["text"=>" "]);
   //连接
   var setting=new Dictionary();
   setting.exp="kag.current.commit(),tf.当前编辑值=\'"+value+"\'";
   setting.hint="点此打开图片选择窗口";
   setting.target="*选择图片";
   //根据路径,传入必须参数
   setting.exp+=",f.list=f."+path;
   kag.tagHandlers.link(setting);
   //方块
   kag.tagHandlers.ch(%["text"=>"□"]);
   kag.tagHandlers.endlink(%[]);
}
//      //选择图片文件（缩略图）
//      case "pic":
//           setting.exp+=",f.list=f."+path;
//           setting.hint="点此打开图片选择窗口";
//           setting.target="*选择图片";
//           break;
           
//--------------------------------------------------------------
//附加连接框□的描绘、图片以外[处理值，打开的窗口类型(扩展名,路径无法更改)]
//--------------------------------------------------------------
function drawLink_Win(value,type)
{
   //空格
   kag.tagHandlers.ch(%["text"=>" "]);
   //连接
   var setting=new Dictionary();
   setting.exp="kag.current.commit(),tf.当前编辑值=\'"+value+"\'";
   //根据文件类型进行设定
   switch (type)
   {
     //音乐音效
      case "music":
           setting.exp+=",f.list=getsozai('mp3','bgm')";
           setting.hint="点此打开音乐选择窗口";
           setting.target="*选择音声";
           break;
      case "sound":
           setting.exp+=",f.list=getsozai('raw','sound')";
           setting.hint="点此打开音效选择窗口";
           setting.target="*选择音声";
           break;
      //选择剧本类文件
      case "script":
           setting.exp+=",f.list=getsozai('txt','data')";
           setting.hint="点此打开剧本选择窗口";
           setting.target="*选择文件";
           break;
   }
   kag.tagHandlers.link(setting);
   //方块
   kag.tagHandlers.ch(%["text"=>"□"]);
   kag.tagHandlers.endlink(%[]);
}
//--------------------------------------------------------------
//附加连接框▽的描绘[转到的菜单target]
//--------------------------------------------------------------
function drawLink_List(target)
{
       //描绘下拉菜单
       kag.tagHandlers.ch(%["text"=>" "]);
       kag.tagHandlers.link(%[
       "hint"=>"点此打开下拉菜单",
       "target"=>target
       ]);
       kag.tagHandlers.ch(%["text"=>"▽"]);
       kag.tagHandlers.endlink(%[]);
}
[endscript]
;--------------------------------------------------------------
;特殊描绘-独立选框
;--------------------------------------------------------------
[iscript]
function drawCheck(title,value,posX,posY)
{
  //描绘提示文字
  kag.tagHandlers.locate(%["x"=>posX+25,"y"=>posY]);
  kag.tagHandlers.ch(%["text"=>title]);
  //描绘勾选框
  kag.tagHandlers.locate(%["x"=>posX,"y"=>posY+6]);
  //取得值
  var result=value!;
  //值为真
  if (result==true)
  {
    kag.tagHandlers.button(%[
    "normal"=>"edit_check_over",
    "exp"=>"kag.current.commit(),"+value+"=false",
    "target"=>"*window"
    ]);
  }
  //值为假
  else if (result==false)
  {
    kag.tagHandlers.button(%[
    "normal"=>"edit_check_normal",
    "exp"=>"kag.current.commit(),"+value+"=true",
    "target"=>"*window"
    ]);
  }

}
[endscript]
;--------------------------------------------------------------
;特殊描绘-互斥选框
;--------------------------------------------------------------
[iscript]
function drawOption(title,value,posX,posY,negvalue)
{
  //描绘提示文字
  kag.tagHandlers.locate(%["x"=>posX+25,"y"=>posY]);
  kag.tagHandlers.ch(%["text"=>title]);
  //描绘勾选框
  kag.tagHandlers.locate(%["x"=>posX,"y"=>posY+5]);
  //取得值
  var result=value!;
  //值为真
  if (result==true)
  {
    kag.tagHandlers.button(%[
    "normal"=>"edit_option_over",
    "exp"=>"kag.current.commit(),"+value+"=false",
    "target"=>"*window"
    ]);
  }
  //值为假
  else if (result==false)
  {
    kag.tagHandlers.button(%[
    "normal"=>"edit_option_normal",
    "exp"=>"kag.current.commit(),"+value+"=true,"+negvalue+"=false",
    "target"=>"*window"
    ]);
  }

}
[endscript]
;---------------------------------------------------------------
;描绘基本窗口
;---------------------------------------------------------------
[iscript]
function drawWin(layer,message,title,width,height)
{
    with(layer)
  {
     .visible=true;
     .left=(1024-width)/2;
     .top=(700-height)/2;
     .width=width;
     .height=height;
     //底板
     .fillRect(0,0,width,height,0xFFD4D0C8);
     .fillRect(0,0,width,1,0xFFFFFFFF);
     .fillRect(0,0,1,height,0xFFFFFFFF);
     .fillRect(0,height-1,width,1,0xFFaca899);
     .fillRect(width-1,0,1,height,0xFFaca899);
     //标题栏
     .fillRect(2,2,width-4,25,0xFF408080);
     //文字
     .font.height=17;
     .drawText(4,5, title, 0xFFFFFF);
     .font.height=15;
  }
  //菜单
  with(message)
  {
     .visible=true;
     .left=(1024-width)/2;
     .top=(700-height)/2;
     .width=width;
     .height=height;
  }  
}
[endscript]
;---------------------------------------------------------------
;描绘文件listbox/描绘listbutton
;---------------------------------------------------------------
[iscript]
//------------------------------------
//listbox
function drawBox(title,layer,left,top,width,line,array,page=0)
{
//设定长度
var height=(line+1)*20+5;

     with(layer)
  {
     //底板
     .fillRect(left,top,width,height,0xFFFFFFFF);
     //标题
     .fillRect(left+1,top+1,width-2,20,0xFFD8D8D8);
     .drawText(left+5,top+4,title,0x000000);
     .fillRect(left+1,top+21,width-2,1,0xFFaca899);
     //边框
     .fillRect(left,top,width,1,0xFFaca899);
     .fillRect(left,top,1,height,0xFFaca899);  
     .fillRect(left,top+height-1,width,1,0xFFaca899);
     .fillRect(left+width-1,top,1,height,0xFFaca899);
     
     //字号
     .font.height=18;
     //列表
     for (var i=line*page;i<line*page+line;i++)
     {
       if (i>=array.count) break;
       //选中
       if (f.window!="script" && f.select==i) .fillRect(left+1,top+23+(i-line*page)*20,width-2,20,0xFFCAFFFF);
       if (f.window=="script" && f.当前脚本行==i) .fillRect(left+1,top+23+(i-line*page)*20,width-2,20,0xFFCAFFFF);
       //文字
       .drawText(left+5,top+1+24+(i-line*page)*20, array[i],0x000000);
     }
  }
}
//------------------------------------
//选择一行
function setselect(num,line)
{
   //未选择
   if (f.window!="script")
   {
     f.select=num+line*(f.curpage-1);
     update();
   }
   //为脚本编辑窗口
   else if (f.window=='script')
   {
        if (f.当前脚本行!=num+line*(f.当前脚本页-1))//未选中
        {
            f.当前脚本行=num+line*(f.当前脚本页-1);
            if (f.当前脚本行>f.脚本内容.count-1) {f.当前脚本行=f.脚本内容.count-1;}
            update();
         }
         else//已选中(双击效果)
         {
           kag.process("script_rclick.ks","*编辑该行");
         }
   }
}
//------------------------------------
//按钮
function drawButtonLine(posX,posY,normal,line,height)
{
  for (var i=0;i<line;i++)
  {
     kag.tagHandlers.locate(
     %[
         "x"=>posX,
         "y"=>posY+height*i
      ]
      );
      var setting=new Dictionary();
      setting.normal=normal;
      setting.exp="setselect("+i+","+line+")";
      kag.tagHandlers.button(setting);
  }
}
[endscript]
;---------------------------------------------------------------
;描绘指令按钮
;---------------------------------------------------------------
[iscript]
function drawTagButton(file,left,top,height=29)
{
   var buttons=[];
   buttons.load(file+".tjs");
   for (var i=0;i<buttons.count;i++)
   {
           kag.tagHandlers.locate(
     %[
         "x"=>left,
         "y"=>top+height*i
      ]);
      if (buttons[i]!="-")
      {
        var setting=new Dictionary();
        setting.normal="edit_button_normal";
        setting.over="edit_button_over";
        setting.on="edit_button_on";
        setting.exp="tf.选择按钮='"+buttons[i]+"'";
        setting.target="*点下指令按钮";
        kag.tagHandlers.button(setting);
        drawButtonCaption(buttons[i],14);
      }
   }
}
[endscript]
;-----------------------------------------------------------------------------------
[return]
