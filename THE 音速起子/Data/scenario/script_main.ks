[iscript]
function updateScript()
{
   drawBox("行号",kag.fore.layers[1],0,0,50,25,f.行号,f.当前脚本页-1);
   drawBox("脚本编辑",kag.fore.layers[1],50,0,510,25,f.脚本内容,f.当前脚本页-1);
   //设定滚动条位置
   with(kag.fore.layers[2])
   {
      .visible=true;
      .top=kag.fore.layers[0].top+86;
      if (f.脚本页数>1) .top+=(int)(f.当前脚本页-1)*476/(f.脚本页数-1);
   }
}
[endscript]
;---------------------------------------------------------------
;脚本编辑
;---------------------------------------------------------------
*start
[eval exp="f.tagname=Scripts.evalStorage('tagname.tjs')"]
[rclick enabled="false"]
[eval exp="drawPageBoard(1)"]
[unlocklink]

[current layer="message1"]
[er]
;当前编辑文件名
[nowait]
[font size=18]
[locate x=420 y=28]
[emb exp="Storages.extractStorageName(sf.script)"]
[locate x=600 y=30]
[button normal=edit_button_normal over="edit_button_over" on="edit_button_on" target=*新建脚本]
[eval exp="drawButtonCaption('新建脚本')"]
[locate x=720 y=30]
[button normal=edit_button_normal over="edit_button_over" on="edit_button_on" target=*打开脚本]
[eval exp="drawButtonCaption('打开脚本')"]
;---------------------------------------------------------------
;假如存在，自动打开脚本
;---------------------------------------------------------------
[if exp="sf.script!=''"]
[iscript]
//各种框
drawFill(410,20,165,40);
drawFill(580,20,380,40);

//描绘事件编辑器背景
with (kag.fore.layers[1])
{
   .left=kag.fore.layers[0].left+420;
   .top=kag.fore.layers[0].top+70;
   .width=560;
   .height=525;
   .visible=true;
   .fillRect(0,0,560,525,0xFFD4D0C8);
   .font.height=18;
}
//读入脚本内容
if (f.脚本内容==void) f.脚本内容=loadScript(sf.script);
f.行号=[];
for (var i=0;i<f.脚本内容.count;i++)
{
  f.行号[i]=(int)i+1;
}
//计算页数
f.脚本页数=f.脚本内容.count\25;
if (f.脚本内容.count%25>0) f.脚本页数++;
f.当前脚本页=f.脚本页数;
f.当前脚本行=25*(f.当前脚本页-1);

//指令框
drawFrame("流程",9,20,20,,120);
drawFrame("画面",5,150,20,,120);
drawFrame("特效",3,150,200,,120);
drawFrame("常用",9,280,20,,120);
drawFrame("变数操作",6,20,350,,120);
drawFrame("系统",7,150,350,,120);
drawFrame("音声(打包)",6,280,350,,120);
[endscript]
[jump target=*window]
[endif]
[s]

;---------------------------------------------------------------
;刷新
;---------------------------------------------------------------
*window
;假如不幸不需要重载而跳到了这里
[if exp="sf.script==void"]
[jump target=*start]
[endif]

[eval exp="f.window='script'"]
[rclick enabled="true" jump="true" storage="script_rclick.ks" target=*脚本菜单]
[unlocklink]
[current layer="message1"]
[er]
;当前编辑文件名
[nowait]
[font size=18]
[locate x=420 y=28]
[emb exp="Storages.extractStorageName(sf.script)"]
[locate x=600 y=30]
[button normal=edit_button_normal over="edit_button_over" on="edit_button_on" target=*新建脚本]
[eval exp="drawButtonCaption('新建脚本')"]
[locate x=720 y=30]
[button normal=edit_button_normal over="edit_button_over" on="edit_button_on" target=*打开脚本]
[eval exp="drawButtonCaption('打开脚本')"]
[locate x=840 y=30]
[button normal=edit_button_normal over="edit_button_over" on="edit_button_on" target=*保存当前]
[eval exp="drawButtonCaption('保存当前')"]

;滚动条
;翻页按钮
[image layer=2 storage="edit_slider_button" left=&"(int)kag.fore.messages[1].left+980"]
[button_page x=980 y=70 length=524]
[button_slider x=980 y=86 normal="edit_slider_back3"]
[iscript]
//指令按钮
drawTagButton("流程",30,40,30);
drawTagButton("图象",160,40,29);
drawTagButton("特效",160,215,26);
drawTagButton("对话",290,40,30);
drawTagButton("系统变数",30,375);
drawTagButton("系统",160,375);
drawTagButton("音声",290,375);
//按钮
drawButtonLine(420,90,"edit_button_line3",25,20);
//刷新画面
update();
[endscript]
[s]

*新建脚本
[iscript]
sf.script=createScript("data");
[endscript]
[jump target=*写入新文件 cond="sf.script!=''"]
[jump target=*window]
[s]

*写入新文件
[eval exp="f.脚本内容=[]"]
[eval exp="f.脚本内容[0]=''"]
[eval exp="f.脚本内容.save(sf.script)"]
[jump target=*start]

*打开脚本
;保存当前脚本内容
[iscript]
if (sf.script!="") saveScript(sf.script,f.脚本内容);
tf.script=selectScript();
[endscript]
;有值
[if exp="tf.script!=false"]
[eval exp="sf.script=tf.script"]
[trace exp="sf.script"]
[jump target=*start]
[endif]
;没有返回值,当作啥也没发生刷新画面
[if exp="tf.script==false && sf.script!=''"]
[jump target=*window]
[endif]
;原来就没有值
[if exp="sf.script==''"]
[jump target=*start]
[endif]

*保存当前
[eval exp="saveScript(sf.script,f.脚本内容)"]
[jump target=*window]

*点下指令按钮
;在当前行下方新增一行
[iscript]
if (f.脚本内容[f.当前脚本行]!=void)
{
  f.当前脚本行++;
  AddLine();
}
update();
f.行内容=f.脚本内容[f.当前脚本行];
f.参数=getTagDic(f.行内容);
[endscript]
[rclick enabled="false" call="false" jump="false"]
[jump storage="script_tag.ks" target=*window]
