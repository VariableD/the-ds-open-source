;---------------------------------------------------------------
;游戏习惯设定(资源包位置)
;---------------------------------------------------------------
*window
;---------------------------------------------------------------
[eval exp="drawPageBoard(72)"]
[unlocklink]
[freeimage layer=1]
[freeimage layer=2]
[current layer="message1"]
[er]
[eval exp="drawFill(700,20,260,40)"]
[locate x=720 y=30]
[button normal="edit_button_normal" over="edit_button_over" on="edit_button_on" target=*保存设定]
[eval exp="drawButtonCaption('保存设定',14)"]
[locate x=840 y=30]
[button normal="edit_button_normal" over="edit_button_over" on="edit_button_on" target=*放弃修改]
[eval exp="drawButtonCaption('放弃修改',14)"]

[frame title="默认资源包" line=4 x=15 y=20]
[line title="背景" x=50 y=45]
[line title="背景" name="f.setting.bg" x=40 y=45]
[line title="人物" name="f.setting.chara" x=40 y=75]
[line title="音乐" name="f.setting.bgm" x=40 y=105]
[line title="音效" name="f.setting.sound" x=40 y=135]

[frame title="默认全屏文字范围" line=4 x=345 y=20]
[line title="左上点x" name="f.setting.frame.x1" x=370 y=45]
[line title="左上点y" name="f.setting.frame.y1" x=370 y=75]
[line title="右下点x" name="f.setting.frame.x2" x=370 y=105]
[line title="右下点y" name="f.setting.frame.y2" x=370 y=135]
[s]

*保存设定
[commit]
[iscript]
   (Dictionary.saveStruct incontextof f.setting)(System.exePath + "project/"+sf.project+"/setting.txt");
[endscript]
[jump target=*window]

*放弃修改
[iscript]
    f.setting=Scripts.evalStorage(System.exePath + "project/"+sf.project+"/setting.txt");
[endscript]
[jump target=*window]
