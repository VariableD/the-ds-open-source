;---------------------------------------------------------------
;文字显示范围设定
;---------------------------------------------------------------
*window
[window_up width=330 height=380 title="文字范围"]
[locate x=30 y=50]
请用鼠标在下方窗口内拖拽划定范围
[iscript]

with(kag.fore.layers[6])
{
   .width=256;
   .height=192;
   .left=kag.fore.layers[5].left+32;
   .top=kag.fore.layers[5].top+90;
   .visible=true;
   .fillRect(0,0,256,192,0xFFFFFFFF);
}
f.感应层=new getMargin(kag,kag.fore.layers[6]);
[endscript]
[s]

*确认
[iscript]
f.参数.elm[1]=f.参数.x1;
f.参数.elm[2]=f.参数.y1;
f.参数.elm[3]=f.参数.x2;
f.参数.elm[4]=f.参数.y2;
[endscript]

*关闭选单
[eval exp="f.window=''"]
[rclick enabled="false"]
[freeimage layer="5"]
[freeimage layer="6"]
[freeimage layer="7"]
[current layer="message4"]
[er]
[layopt layer="message4" visible="false"]
[return]


