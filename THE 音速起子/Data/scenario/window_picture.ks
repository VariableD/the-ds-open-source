[iscript]
function updatePicture()
{
//描绘图片显示区域
with(kag.fore.layers[6])
{
  .visible=true;
  .width=820;
  .height=435;
  .left=kag.fore.layers[5].left+30;
  .top=kag.fore.layers[5].top+70;
  
  .fillRect(0,0,.width,.height,0xFFFFFFFF);
  
  .fillRect(0,0,.width,1,0xFFaca899);
  .fillRect(0,0,1,.height,0xFFaca899);
  .fillRect(0,.height-1,.width,1,0xFFaca899);
  .fillRect(.width-1,0,1,.height,0xFFaca899);
  .font.height=12;
}
//设定滚动条位置
   with(kag.fore.layers[7])
   {
      .visible=true;
      .top=kag.fore.layers[5].top+86;
      if (f.page>1) .top+=(int)(f.curpage-1)*384/(f.page-1);
   }
//描绘缩略图
for (var i=(f.curpage-1)*6;i<f.curpage*6;i++)
{
        if (i>=f.list.count) break;
        var abs=i-(f.curpage-1)*6;
        var posX=15+(abs%3)*(256+10);
        var posY=10+(abs\3)*(192+20);
        with(kag.fore.layers[6])
       {
          .operateRect(posX, posY, f.list[i].layer, 0, 0, 256, 192);
          .drawText(posX,posY+193,f.list[i].name,0x000000);
          .fillRect(posX,posY,256,1,0xFFaca899);
          .fillRect(posX,posY,1,192,0xFFaca899);
          .fillRect(posX,posY+191,256,1,0xFFaca899);
          .fillRect(posX+255,posY,1,192,0xFFaca899);
       //选中
          if (f.select==i) 
          {
            .colorRect(posX-4,posY-4,264,215,0x0000FF,50);
            
            .colorRect(posX-4,posY-4,264,1,0x0000FF,128);
            .colorRect(posX-4,posY-4,1,215,0x0000FF,128);
            .colorRect(posX-4,posY-5+215,264,1,0x0000FF,128);
            .colorRect(posX-5+264,posY-4,1,215,0x0000FF,128);
          }
        }
}

}
[endscript]
;---------------------------------------------------------------
;图片选择
;---------------------------------------------------------------
*window
[window_up width=950 height=650 title="选择图片"]
;图片选择按钮
[iscript]
  for (var i=0;i<6;i++)
  {
     kag.tagHandlers.locate(
     %[
         "x"=>15+30+(i%3)*(256+10),
         "y"=>10+70+(i\3)*(192+20),
      ]
      );
      var setting=new Dictionary();
      setting.normal="edit_button_selpicture";
      setting.exp="setselect("+i+","+6+")";
      kag.tagHandlers.button(setting);
  }

[endscript]
;翻页按钮
[button_page x=860 y=70 length=432]
;滚动条按钮
[image layer=7 storage="edit_slider_button" left=&"(int)kag.fore.layers[5].left+860"]
[button_slider x=860 y=86 normal="edit_slider_back"]
;下拉菜单
[line title="文件夹" name="f.fold" type="list" target="*选择文件夹" x=30 y=40]
[iscript]
//计算页数
countPage(f.list.count,6);
f.curpage=1;
f.select=0;
f.window="picture";
update();
[endscript]

[s]


*选择文件夹
[list x=46 y=40 line=2 layer="message5" width=188]
[link exp="f.list=f.bg" target=*关闭下拉菜单]背景[endlink][r]
[link exp="f.list=f.chara" target=*关闭下拉菜单]人物[endlink]
[s]

*关闭下拉菜单
[current layer="message5"]
[er]
[layopt layer="message5" visible="false"]
[jump target=*window]

*确认
[if exp="f.list[f.select]!=void"]
[eval exp=&"tf.当前编辑值+'=\''+f.list[f.select].name+'\''"]
[endif]

*关闭选单
[eval exp="f.window=''"]
[rclick enabled="false"]
[freeimage layer="5"]
[freeimage layer="6"]
[freeimage layer="7"]
[freeimage layer="8"]
[current layer="message4"]
[er]
[layopt layer="message4" visible="false"]
[current layer="message5"]
[er]
[layopt layer="message5" visible="false"]
[return]
