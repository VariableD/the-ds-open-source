;------------------------------------------------------------------------------------------------
;脚本文件的选择,载入,保存
;------------------------------------------------------------------------------------------------
;------------------------------------------------------------------
;选择脚本文件
;------------------------------------------------------------------
[iscript]
function selectScript()
{
//读入
 var params = 
 %[
   filter : ["脚本文件(*.txt)|*.txt"],
   filterIndex : 1,
   name : "",
   initialDir : sf.path+"data/",
   title : "请选择脚本文件",
   save : false,
 ];
//假如选择了文件
  if (Storages.selectFile(params))
  {  
     //取得标准路径+文件名
     var temp_fullname=Storages.getLocalName(params.name);
     //清空当前脚本内容
     f.脚本内容=void;
     //返回文件路径
     return temp_fullname;
   }
   else return false;
}
[endscript]
;------------------------------------------------------------------
;新建脚本文件
;------------------------------------------------------------------
[iscript]
function createScript(path)
{

//读入
 var params = 
 %[
   filter : ["脚本文件(*.txt)|*.txt"],
   filterIndex : 1,
   name : "",
   initialDir : sf.path+"data/",
   title : "请为新建的脚本文件命名",
   save : true,
 ];
//假如进行了保存
  if (Storages.selectFile(params))
  {
    return Storages.getLocalName(Storages.chopStorageExt(params.name))+".txt";
  }
 }
[endscript]
;------------------------------------------------------------------
;读入脚本文件
;------------------------------------------------------------------
[iscript]
function loadScript(file)
{
var script=[];
script.load(file);
return script;
}
[endscript]
;------------------------------------------------------------------
;保存脚本文件
;------------------------------------------------------------------
[iscript]
function saveScript(filename,script)
{
  Plugins.link("win32ole.dll");
  var objFileSystem = new WIN32OLE("Scripting.FileSystemObject");
  var objTextFile = objFileSystem.OpenTextFile(filename , 2 ,true ,-2);
  //参数:文件名,(只读1/只写2/续写8),不存在是否新建(true/false),(默认-2/UNICODE-1/ASCII0)
  for (var i=0;i<script.count;i++)
   {
       objTextFile.WriteLine(script[i]);
    }
objTextFile.Close();
}
[endscript]
;-----------------------------------------------------------------------------------
;搜索及读入文件夹下的文件,缩略图
;-----------------------------------------------------------------------------------
[iscript]
//---------------------------------------------------------------
//读取某文件夹下文件名
//---------------------------------------------------------------
function getfile(path)
{
  var list = getDirList(Storages.getLocalName(sf.path+path+"/")+"/");
  return list;
}
//---------------------------------------------------------------
//读取其他类文件名(连同扩展名)[类型，位置]
//---------------------------------------------------------------
function getsozai(type,path)
{
  var list=getfile(path);
  var file=[];
     for (var i=0;i<list.count;i++)
   {
      //取得扩展名
      var ext=Storages.extractStorageExt(list[i]);
      dm(ext);
      //根据不同类型，返回需求的文件全名
      switch (type)
      {
        //剧本档
        case "txt":
             if (ext=='.txt') file.add(Storages.chopStorageExt(list[i]));
//             if (ext=='.spt') file.add(Storages.chopStorageExt(list[i]));
             break;
       //音乐档
       case "mp3":
             if (ext=='.mp3') file.add(list[i]);
       //音效档
       case "raw":
             if (ext=='.raw') file.add(list[i]);
       //等待打包档
       case "tospt":
             if (ext=='.txt') file.add(list[i]);
      }
   }
   file.sort('a');
   return file;
}
//---------------------------------------------------------------
//读取图片类文件名(不要扩展名)[位置]
//---------------------------------------------------------------
function getpic(path)
{
   var list=getfile(path);
   var file=[];
   for (var i=0;i<list.count;i++)
   {
      var ext=Storages.extractStorageExt(list[i]);
      dm(ext);
      //将图片读入
                 switch (ext)
            {
                case ".png":
                file.add(Storages.chopStorageExt(list[i]));
                break;
           }
   }
   //按照文件名顺序排列图片
   file.sort('a');
   return file;
}
//---------------------------------------------------------------
//读入图片缩略图
//---------------------------------------------------------------
function loadpic(path)
{

     //描绘文件夹名
       with(kag.fore.event)
      {
         //文字
         .font.height=18;
         .fillRect(110,90,210,40,0xFFFFFFFF);
         .drawText(120,100,path+"文件夹读取完毕", 0x000000);
         .update();
       }
       
   var list=getpic(path);
   var files=[];
   
    //取得文件名及缩略图
      for (var i=0;i<list.count;i++)
   {
        files[i]=new Dictionary();
        files[i].name=list[i];
        files[i].layer=new Layer(kag, kag.fore.base);
        
        //临时图层
        var temp=new Layer(kag, kag.fore.base);
        temp.loadImages(sf.path+path+"/"+files[i].name);
        temp.setSizeToImageSize();
         var zw;
         var zh;
        //设定缩略图比例
        if ((temp.height/temp.width)<=0.75)
        {
            zw=256;
            zh=zw*temp.height/temp.width;
        }
        else
        {
            zh=192;
            zw=zh*temp.width/temp.height;
        }
        
       //将图片缩放复制到层上
       files[i].layer.width=256;
       files[i].layer.height=192;
       files[i].layer.stretchCopy(0, 0, zw, zh, temp, 0, 0, temp.width, temp.height, stNearest);
   }
   
   return files;
}
[endscript]
;-----------------------------------------------------------------------------------
;搜索及读入游戏工程
;-----------------------------------------------------------------------------------
[iscript]
//---------------------------------------------------------------
//检查工程合法性
//---------------------------------------------------------------
//function checkProject(fold)
//{
//   var result=true;
//   //设定路径
//   var path=System.exePath + "project/"+fold+"/avgFiles/";
//   //所有配置文件是否存在
//
//   return result;
//}
//---------------------------------------------------------------
//读取project文件夹下的游戏工程
//---------------------------------------------------------------
function getProject()
{
   var list=getDirList(Storages.getLocalName(System.exePath + "project/")+"/");
   f.project=[];
   f.title=[];
   for (var i=0;i<list.count;i++)
   {
      if (list[i].indexOf('/')!=-1 && list[i].charAt(0)!='.') 
      {
         var fold=list[i].substring(0,list[i].length-1);
            //添加工程文件夹名
            f.project.add(fold);
      }   
   }

//计算页数
countPage(f.project.count,10);
f.curpage=1;
f.select=0;
}

//---------------------------------------------------------------
//读取游戏工程全设定
//---------------------------------------------------------------
function getSetting()
{
//------------------------------------------
//设定avgFiles目录位置
sf.path=System.exePath + "project/"+sf.project+"/avgFiles/";
//------------------------------------------
//添加自动路径:图片，音乐素材
//Storages.addAutoPath(sf.path+"bg/");
//Storages.addAutoPath(sf.path+"chara/");
//Storages.addAutoPath(sf.path+"bgm/");
//Storages.addAutoPath(sf.path+"sound/");
//Storages.addAutoPath(sf.path+"data/");
//读入人名
f.namelist=[];
if (Storages.isExistentStorage(System.exePath + "project/"+sf.project+"/namelist.txt"))
  {
    f.namelist.load(System.exePath + "project/"+sf.project+"/namelist.txt");
  }
//读入设定
f.setting=new Dictionary();
if (Storages.isExistentStorage(System.exePath + "project/"+sf.project+"/setting.txt"))
  {
    f.setting=Scripts.evalStorage(System.exePath + "project/"+sf.project+"/setting.txt");
  }
else
  {
    f.setting=Scripts.evalStorage("setting.tjs");
   (Dictionary.saveStruct incontextof f.setting)(System.exePath + "project/"+sf.project+"/setting.txt");
  }
}
[endscript]
;-----------------------------------------------------------------------------------
[return]
