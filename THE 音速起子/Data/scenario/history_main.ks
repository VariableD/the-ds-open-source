;---------------------------------------------------------------
;人物姓名编辑器
;---------------------------------------------------------------
*window
[eval exp="drawPageBoard(143)"]
[unlocklink]
[freeimage layer=1]
[freeimage layer=2]
[current layer="message1"]
[er]
[nowait]

[locate x=600 y=30]
[button normal="edit_button_normal" over="edit_button_over" on="edit_button_on" target=*新增角色]
[eval exp="drawButtonCaption('新增角色',14)"]
[locate x=720 y=30]
[button normal="edit_button_normal" over="edit_button_over" on="edit_button_on" target=*保存设定]
[eval exp="drawButtonCaption('保存设定',14)"]
[locate x=840 y=30]
[button normal="edit_button_normal" over="edit_button_over" on="edit_button_on" target=*放弃修改]
[eval exp="drawButtonCaption('放弃修改',14)"]

[iscript]
drawFill(580,20,380,40);
for (var i=0;i<f.namelist.count;i++)
{
  drawEdit("角色"+(i+1),"f.namelist["+i+"]",30,50+i*30);
   //空格
  kag.tagHandlers.ch(%["text"=>" "]);
  var setting=new Dictionary();
  setting.exp="tf.删除="+i;
  setting.target="*删除角色";
  kag.tagHandlers.link(setting);
  kag.tagHandlers.ch(%["text"=>"删除"]);
  kag.tagHandlers.endlink(%[]);
}
[endscript]
[s]

*删除角色
[commit]
[iscript]
f.namelist.erase(tf.删除);
[endscript]
[jump target=*window]

*保存设定
[commit]
[iscript]
  f.namelist.save(System.exePath + "project/"+sf.project+"/namelist.txt");
[endscript]
[jump target=*window]

*新增角色
[commit]
[iscript]
if (f.namelist.count<18) f.namelist.add("新人物");
[endscript]
[jump target=*window]

*放弃修改
[iscript]
//读入
f.namelist=[];
if (Storages.isExistentStorage(System.exePath + "project/"+sf.project+"/namelist.txt"))
{
  f.namelist.load(System.exePath + "project/"+sf.project+"/namelist.txt");
}
[endscript]
[jump target=*window]
