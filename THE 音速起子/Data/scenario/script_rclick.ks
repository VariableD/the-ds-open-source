*脚本菜单
[rclick enabled="true" jump="true" storage="script_rclick.ks" target=*返回脚本]
;改变选择行
[iscript]
f.在列表上=GetRclick(kag.fore.layers[1].left,kag.fore.layers[1].top+20,560,500);
update();
f.window='';
[endscript]
[jump target=*返回脚本 cond="f.在列表上==false"]

[locklink]
;显示菜单
[position layer="message2" visible="true" frame="edit_frame_rmenu"]
[layopt layer="message2" left=&"kag.fore.base.cursorX" top=&"kag.fore.base.cursorY"]
[layopt layer="message2" top=&"kag.fore.base.cursorY-kag.fore.messages[2].height" cond="kag.fore.messages[2].top>500"]
[current layer="message2"]
[nowait]
[font size=18]
[style linespacing=10 linesize=20]
[link target=*编辑该行]    编辑该行 [endlink]
[r]
[link target=*插入一行]    上方插入行[endlink]
[r]
[link target=*删除该行]    删除该行 [endlink]
[r]
[link target=*剪切]    剪切     [endlink]
[r]
[link target=*复制]    复制     [endlink]
[r]
[if exp="tf.复制内容==''"][font color="0xF2F2F2"]    粘贴     [endif]
[if exp="tf.复制内容!=''"][link target=*粘贴]    粘贴     [endlink][endif]
[endnowait]
[s]

*编辑该行

;跳跃到指令编辑窗口
[rclick enabled="false" call="false" jump="false" storage=""]
[layopt layer="message2" visible="false"]
[jump storage="script_tag.ks" target=*getline]

*插入一行
;在当前行上方新增一行
[iscript]
AddLine();
[endscript]
[jump target=*返回脚本]

*删除该行
[iscript]
if (f.脚本内容.count>1) DelLine();
else f.脚本内容[0]="";
[endscript]
[jump target=*返回脚本]

*剪切
[iscript]
CutLine();
[endscript]
[jump target=*返回脚本]

*复制
[iscript]
CopyLine();
[endscript]
[jump target=*返回脚本]

*粘贴
[iscript]
PasteLine();
[endscript]
[jump target=*返回脚本]

*返回脚本
[rclick enabled="true" jump="true" storage="script_rclick.ks" target=*脚本菜单]
[layopt layer="message2" visible="false"]
[current layer="message1"]
[jump storage="script_main.ks" target=*window]
