;---------------------------------------------------------------
;指令分析
;---------------------------------------------------------------
*getline
;取得当前编辑行
[iscript]
f.行内容=f.脚本内容[f.当前脚本行];
f.参数=getTagDic(f.行内容);
//有tagname
if (f.参数.tagname!=void)
{
   //取得配置表的对应内容
     tf.选择按钮=f.tagname[f.参数.tagname];
}
//无法分析
if (tf.选择按钮=="")
{
     tf.选择按钮="直接输入";
}
[endscript]
;---------------------------------------------------------------
;指令编辑
;---------------------------------------------------------------
*window
;禁止滚轮
[eval exp="f.window=''"]
;---------------------------------------------------------------
;根据tf.选择按钮的值进行处理
[jump target=&"'*'+tf.选择按钮"]
[s]
;---------------------------------------------------------------
*选择图片
[call storage="window_picture.ks"]
[jump target=*window]
*选择文件
[call storage="window_file.ks"]
[jump target=*window]
*选择音声
[call storage="window_file.ks"]
[jump target=*window]

*选择范围
[iscript]
f.文本框.visible=false;
f.参数.x1=f.参数.elm[1];
f.参数.y1=f.参数.elm[2];
f.参数.x2=f.参数.elm[3];
f.参数.y2=f.参数.elm[4];
[endscript]
[call storage="window_framesize.ks"]
[jump target=*window]

*切换效果
[list line=11 x=34 y=110 layer="message6"]
[link exp="f.参数.elm[2]=0" target=*关闭下拉菜单]无效果[endlink][r]
[link exp="f.参数.elm[2]=1" target=*关闭下拉菜单]白渐进[endlink][r]
[link exp="f.参数.elm[2]=2" target=*关闭下拉菜单]黑渐进[endlink][r]
[link exp="f.参数.elm[2]=3" target=*关闭下拉菜单]黑色拉幕效果A[endlink][r]
[link exp="f.参数.elm[2]=4" target=*关闭下拉菜单]黑色拉幕效果B[endlink][r]
[link exp="f.参数.elm[2]=5" target=*关闭下拉菜单]黑色拉幕效果C[endlink][r]
[link exp="f.参数.elm[2]=6" target=*关闭下拉菜单]黑色拉幕效果D[endlink][r]
[link exp="f.参数.elm[2]=7" target=*关闭下拉菜单]黑色拉幕效果E[endlink][r]
[link exp="f.参数.elm[2]=8" target=*关闭下拉菜单]黑色拉幕效果F[endlink][r]
[link exp="f.参数.elm[2]=9" target=*关闭下拉菜单]黑色拉幕效果G[endlink][r]
[link exp="f.参数.elm[2]=10" target=*关闭下拉菜单]黑色拉幕效果H[endlink]
[s]

*人物姓名
[iscript]
f.文本框.visible=false;
[endscript]
[list line=&"1+(int)f.namelist.count" x=34 y=80 layer="message6"]
[iscript]
for (var i=0;i<f.namelist.count;i++)
{
   var setting=new Dictionary();
   setting.exp="f.参数.elm[2]=\""+f.namelist[i]+"\"";
   setting.target="*关闭下拉菜单";
   kag.tagHandlers.link(setting);
   kag.tagHandlers.ch(%["text"=>f.namelist[i]]);
   kag.tagHandlers.endlink(%[]);
   kag.tagHandlers.r(%[]);
}
[endscript]
[link target=*直接输入人物姓名]直接输入[endlink]
[s]

*直接输入人物姓名
[input name="f.参数.elm[2]" title=直接输入人物姓名 prompt="请输入人名：      "]
[jump target=*关闭下拉菜单]

*关闭下拉菜单
[iscript]
if (f.文本框!=void) f.文本框.visible=true;
[endscript]
[er]
[layopt layer="message6" visible="false"]
[jump target=*window]
;---------------------------------------------------------------
*确认
[commit]
;特殊的处理
[iscript]
switch(f.参数.tagname)
{
   case "say":
       f.参数.elm[3]=f.文本框.text;
       break;
   case "boxText":
       f.参数.elm[6]=f.文本框.text;
       break;
   case "changeText":
   case "call":
       var fullpath="avgFiles/data/";
       fullpath+=f.参数.elm[0];
       fullpath+=".spt";
       f.参数.elm[0]=fullpath;
       break;
   case "quakex":
   case "quakey":
      if (f.参数.x==1) f.参数.tagname="quakex";
      if (f.参数.y==1) f.参数.tagname="quakey";
      break;
   
}
[endscript]
;写入
[iscript]
//f.脚本内容
if (f.参数.tagname!="=;")
{
   f.脚本内容[f.当前脚本行]="@"+f.参数.tagname+"(";
   //参数1
   if (f.参数.elm.count>0) f.脚本内容[f.当前脚本行]+=f.参数.elm[0];
   //其他参数
   if (f.参数.elm.count>1) for (var i=1;i<f.参数.elm.count;i++)
   {
     f.脚本内容[f.当前脚本行]+=","+f.参数.elm[i];
   }
     f.脚本内容[f.当前脚本行]+=");";
}
else
{
   f.脚本内容[f.当前脚本行]=f.参数.tagname;
}
[endscript]

*关闭选单
[rclick enabled="false"]
[freeimage layer="3"]
[current layer="message3"]
[er]
[layopt layer="message3" visible="false"]
[iscript]
f.文本框=void;
kag.focusable=true;
if (f.脚本内容[f.当前脚本行]==void && f.脚本内容.count>1) {DelLine();if (f.当前脚本行>0) f.当前脚本行--;}
[endscript]
[jump storage="script_main.ks" target=*window]
;---------------------------------------------------------------
;图象
;---------------------------------------------------------------
*上屏背景操作
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_bg")
{
   f.参数.tagname="res_bg";
   f.参数.elm[0]=f.setting.bg;
   f.参数.elm[1]="NULL";
   f.参数.elm[2]="2";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="图片" name="f.参数.elm[1]" x=30 y=80 type="pic" path="bg"]
[line title="效果" name="f.参数.elm[2]" x=30 y=110 type="list" target="*切换效果"]
[s]
;---------------------------------------------------------------
*下屏背景操作
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_btbg")
{
   f.参数.tagname="res_btbg";
   f.参数.elm[0]=f.setting.bg;
   f.参数.elm[1]="NULL";
   f.参数.elm[2]="2";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="图片" name="f.参数.elm[1]" x=30 y=80 type="pic" path="bg"]
[line title="效果" name="f.参数.elm[2]" x=30 y=110 type="list" target="*切换效果"]
[s]
;---------------------------------------------------------------
*显示角色
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_chara")
{
   f.参数.tagname="res_chara";
   f.参数.elm[0]=f.setting.chara;
   f.参数.elm[1]="";
   f.参数.elm[2]="1";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="图片" name="f.参数.elm[1]" x=30 y=80 type="pic" path="chara"]
[check title="渐变效果" name="f.参数.elm[2]" x=30 y=110]
[s]
;---------------------------------------------------------------
*消除角色
[iscript]
//默认值
if (f.参数.tagname!="clearChara")
{
   f.参数.tagname="clearChara";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*双人角色操作
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_lrchara")
{
   f.参数.tagname="res_lrchara";
   f.参数.elm[0]=f.setting.chara;
   f.参数.elm[1]="NULL";
   f.参数.elm[2]="NULL";
   f.参数.elm[3]="1";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="左图" name="f.参数.elm[1]" x=30 y=80 type="pic" path="chara"]
[line title="右图" name="f.参数.elm[2]" x=30 y=110 type="pic" path="chara"]
[check title="渐变效果" name="f.参数.elm[3]" x=30 y=140]
[s]
;---------------------------------------------------------------
;对话
;---------------------------------------------------------------
*显示头像
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_head")
{
   f.参数.tagname="res_head";
   f.参数.elm[0]=f.setting.chara;
   f.参数.elm[1]="NULL";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="图片" name="f.参数.elm[1]" x=30 y=80 type="pic" path="chara"]
[s]
*消除头像
[iscript]
//默认值
if (f.参数.tagname!="clearHead")
{
   f.参数.tagname="clearHead";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*一般对话
[window_down width=500 height=400 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="say")
{
   f.参数.tagname="say";
   f.参数.elm[0]=0;
   f.参数.elm[1]=0;
   f.参数.elm[2]="";
   f.参数.elm[3]="\"\"";
}
if (f.文本框==void)
{
 f.文本框=new Pad();
 f.文本框.text=f.参数.elm[3];
 f.文本框.borderStyle=bsNone;
 f.文本框.wordWrap=true;
 f.文本框.color=0xFFFFFF;
 f.文本框.left=kag.left+kag.fore.messages[3].left+40;
 f.文本框.top=kag.top+kag.fore.messages[3].top+160;
 f.文本框.width=420;
 f.文本框.height=190;
 f.文本框.fontColor=0x000000;
 f.文本框.fontSize=10;
 kag.focusable=false;
}
[endscript]
[check title="空出头像位" name="f.参数.elm[0]" x=30 y=50]
[check title="角色动画" name="f.参数.elm[1]" x=170 y=50]
[line title="人名" name="f.参数.elm[2]" x=30 y=80 type="list" target="*人物姓名"]
[locate x=310 y=80]
[link exp="f.文本框.visible=false;f.文本框.visible=true"]文本框消失的话点这里[endlink]
[iscript]
f.文本框.visible=true;
[endscript]
[s]
;---------------------------------------------------------------
*全屏文字
[window_down width=600 height=400 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="boxText")
{
   f.参数.tagname="boxText";
   f.参数.elm[0]=0;
   f.参数.elm[1]=f.setting.frame.x1;
   f.参数.elm[2]=f.setting.frame.y1;
   f.参数.elm[3]=f.setting.frame.x2;
   f.参数.elm[4]=f.setting.frame.y2;
   f.参数.elm[5]=1;
   f.参数.elm[6]="\"\"";
}
if (f.文本框==void)
{
 f.文本框=new Pad();
 f.文本框.text=f.参数.elm[6];
 f.文本框.borderStyle=bsNone;
 f.文本框.wordWrap=true;
 f.文本框.color=0xFFFFFF;
 f.文本框.left=kag.left+kag.fore.messages[3].left+40;
 f.文本框.top=kag.top+kag.fore.messages[3].top+220;
 f.文本框.width=500;
 f.文本框.height=120;
 f.文本框.fontColor=0x000000;
 f.文本框.fontSize=10;
 kag.focusable=false;
}
[endscript]
[check title="显示在下屏" name="f.参数.elm[0]" x=30 y=50]
[check title="逐字显示" name="f.参数.elm[5]" x=170 y=50]

[line title="左上点x" name="f.参数.elm[1]" x=30 y=80]
[line title="y" name="f.参数.elm[2]" x=290 y=80]
[line title="右下点x" name="f.参数.elm[3]" x=30 y=110]
[line title="y" name="f.参数.elm[4]" x=290 y=110] [link target=*选择范围 hint="点此直接划定范围"]□[endlink]
[locate x=30 y=140]
[link exp="f.文本框.visible=false;f.文本框.visible=true"]文本框消失的话点这里[endlink]
[iscript]
f.文本框.visible=true;
[endscript]
[s]
;---------------------------------------------------------------
*消除文字
[iscript]
//默认值
if (f.参数.tagname!="erase")
{
   f.参数.tagname="erase";
   f.参数.elm[0]=0;
}
[endscript]
[window_down width=180 height=200 title=&"tf.选择按钮"]
[check title="擦除下屏文字" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
*话框显示
[iscript]
//默认值
if (f.参数.tagname!="texton")
{
   f.参数.tagname="texton";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*话框隐藏
[iscript]
//默认值
if (f.参数.tagname!="textoff")
{
   f.参数.tagname="textoff";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*等待时间
[iscript]
//默认值
if (f.参数.tagname!="delay")
{
   f.参数.tagname="delay";
   f.参数.elm[0]=1000;
}
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="时间" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
;流程
;---------------------------------------------------------------
*剧本内跳转
[iscript]
//默认值
if (f.参数.tagname!="jump")
{
   f.参数.tagname="jump";
   f.参数.elm[0]=1;
}
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="行数" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
*剧本间跳转
[iscript]
//默认值
if (f.参数.tagname!="changeText")
{
   f.参数.tagname="changeText";
   f.参数.elm[0]="begin";
}
//处理剧本名
f.参数.elm[0]=Storages.chopStorageExt(Storages.extractStorageName(f.参数.elm[0]));
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="剧本" name="f.参数.elm[0]" x=30 y=50 type="script"]
[s]
;---------------------------------------------------------------
*选项分歧
[iscript]
//默认值
if (f.参数.tagname!="select")
{
   f.参数.tagname="select";
   f.参数.text1="选项一";
   f.参数.text2="选项二";
   f.参数.text3="选项三";
   
}
[endscript]
[window_down width=400 height=400 title=&"tf.选择按钮"]
[line title="#1" x=30 y=50 name="f.参数.text1"]
[line title="指令集" x=30 y=80 name="f.参数.ins1"]
[line title="对话" x=30 y=110 name="f.参数.talk1"]

[line title="#2" x=30 y=140 name="f.参数.text2"]
[line title="指令集" x=30 y=170 name="f.参数.ins2"]
[line title="对话" x=30 y=110 name="f.参数.talk2"]

[line title="#3" x=30 y=230 name="f.参数.text3"]
[line title="指令集" x=30 y=260 name="f.参数.ins3"]
[line title="对话" x=30 y=110 name="f.参数.talk3"]
[s]
;---------------------------------------------------------------
*选项变数处理
[iscript]
//默认值
if (f.参数.tagname!="var_select")
{
   f.参数.tagname="var_select";
   f.参数.elm[0]="value_name";
   f.参数.elm[1]=3;
   f.参数.elm[2]="选项一|选项二|选项三";
}
[endscript]
[window_down width=320 height=270 title=&"tf.选择按钮"]
[line title="计入变量" name="f.参数.elm[0]" x=30 y=50]
[line title="选项个数" name="f.参数.elm[1]" x=30 y=80]
[line title="选项文字" name="f.参数.elm[2]" x=30 y=110]
[s]
;---------------------------------------------------------------
*条件分歧
[iscript]
//默认值
if (f.参数.tagname!="if")
{
   f.参数.tagname="if";
   f.参数.elm[0]="$(var) == 100";
}
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="条件式" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
*条件分歧结束
[iscript]
//默认值
if (f.参数.tagname!="endif")
{
   f.参数.tagname="endif";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*呼叫剧本
[iscript]
//默认值
if (f.参数.tagname!="call")
{
   f.参数.tagname="call";
   f.参数.elm[0]="begin";
}
//处理剧本名
f.参数.elm[0]=Storages.chopStorageExt(Storages.extractStorageName(f.参数.elm[0]));
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="剧本" name="f.参数.elm[0]" x=30 y=50 type="script"]
[s]
;---------------------------------------------------------------
*呼叫返回标记
[iscript]
//默认值
if (f.参数.tagname!="=;")
{
   f.参数.tagname="=;";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*游戏结束
[iscript]
//默认值
if (f.参数.tagname!="gameOver")
{
   f.参数.tagname="gameOver";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
;特效
;---------------------------------------------------------------
*闪光
[iscript]
//默认值
if (f.参数.tagname!="flash")
{
   f.参数.tagname="flash";
   f.参数.elm[0]=0;
   f.参数.elm[1]=0;
   f.参数.elm[2]=1;
   f.参数.elm[3]=4;
}
[endscript]
[window_down width=320 height=270 title=&"tf.选择按钮"]
[check title="上屏" name="f.参数.elm[0]" x=30 y=50]
[check title="黑色闪光" name="f.参数.elm[1]" x=30 y=80]
[line title="闪光次数" name="f.参数.elm[2]" x=30 y=110]
[line title="间隔时间" name="f.参数.elm[3]" x=30 y=140]
[s]
;---------------------------------------------------------------
*震动
[iscript]
//默认值0
if (f.参数.tagname!="quakex" &&f.参数.tagname!="quakey")
{
   f.参数.tagname="quakex";
   f.参数.elm[0]=5;
   f.参数.x=1;
}
[endscript]
[window_down width=320 height=270 title=&"tf.选择按钮"]
[option title="横轴" name="f.参数.x" x=30 y=50 false="f.参数.y"]
[option title="纵轴" name="f.参数.y" x=170 y=50 false="f.参数.x"]
[line title="震动次数" name="f.参数.elm[0]" x=30 y=80]
[s]
;---------------------------------------------------------------
*渐出
[iscript]
//默认值
if (f.参数.tagname!="fadeout")
{
   f.参数.tagname="fadeout";
   f.参数.elm[0]=0;
   f.参数.elm[1]=0;
   f.参数.elm[2]=1;
}
[endscript]
[window_down width=320 height=270 title=&"tf.选择按钮"]
[line title="屏幕" name="f.参数.elm[0]" x=30 y=50]
[check title="黑色渐变" name="f.参数.elm[1]" x=30 y=80]
[line title="时间" name="f.参数.elm[2]" x=30 y=110]
[s]
;---------------------------------------------------------------
*渐入
[iscript]
//默认值
if (f.参数.tagname!="fadein")
{
   f.参数.tagname="fadein";
   f.参数.elm[0]=0;
   f.参数.elm[1]=0;
   f.参数.elm[2]=1;
}
[endscript]
[window_down width=320 height=270 title=&"tf.选择按钮"]
[line title="屏幕" name="f.参数.elm[0]" x=30 y=50]
[check title="黑色渐变" name="f.参数.elm[1]" x=30 y=80]
[line title="时间" name="f.参数.elm[2]" x=30 y=110]
[s]
;---------------------------------------------------------------
;系统
;---------------------------------------------------------------
*话框透明
[iscript]
//默认值
if (f.参数.tagname!="tbtrans_on")
{
   f.参数.tagname="tbtrans_on";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*关闭话框透明
[iscript]
//默认值
if (f.参数.tagname!="tbtrans_off")
{
   f.参数.tagname="tbtrans_off";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*允许菜单
[iscript]
//默认值
if (f.参数.tagname!="menusys_on")
{
   f.参数.tagname="menusys_on";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*禁止菜单
[iscript]
//默认值
if (f.参数.tagname!="menusys_off")
{
   f.参数.tagname="menusys_off";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*存储记录
[iscript]
//默认值
if (f.参数.tagname!="save")
{
   f.参数.tagname="save";
   f.参数.elm[0]="";
}
[endscript]
[window_down width=320 height=200 title=&"tf.选择按钮"]
[line title="书签名" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
*读取记录
[iscript]
//默认值
if (f.参数.tagname!="load")
{
   f.参数.tagname="load";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
;系统变数
;---------------------------------------------------------------
*定义
[iscript]
//默认值
if (f.参数.tagname!="set_var")
{
   f.参数.tagname="set_var";
   f.参数.elm[0]="value_name";
   f.参数.elm[1]=0;
}
[endscript]
[window_down width=320 height=200 title="定义系统变量"]
[line title="变量名" name="f.参数.elm[0]" x=30 y=50]
[line title="初始值" name="f.参数.elm[1]" x=30 y=80]
[s]
;---------------------------------------------------------------
*取消
[iscript]
//默认值
if (f.参数.tagname!="unset_var")
{
   f.参数.tagname="unset_var";
   f.参数.elm[0]="value_name";
}
[endscript]
[window_down width=320 height=200 title="取消已申请的变量"]
[line title="变量名" name="f.参数.elm[0]" x=30 y=50]
[s]
;---------------------------------------------------------------
*赋值
[iscript]
//默认值
if (f.参数.tagname!="set_value")
{
   f.参数.tagname="set_value";
   f.参数.elm[0]="$(value_name)+1";
}
[endscript]
[window_down width=320 height=200 title="为系统变量赋值"]
[line title="表达式" name="f.参数.elm[0]" x=30 y=50]
[locate x=30 y=80]
可用操作（单步）：+ - * / % =
[s]
;---------------------------------------------------------------
*保存
[iscript]
//默认值
if (f.参数.tagname!="var_save")
{
   f.参数.tagname="var_save";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*读取
[iscript]
//默认值
if (f.参数.tagname!="var_load")
{
   f.参数.tagname="var_load";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
;音声
;---------------------------------------------------------------
*播放音乐
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_playBGM")
{
   f.参数.tagname="res_playBGM";
   f.参数.elm[0]=f.setting.bgm;
   f.参数.elm[1]="";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="音乐" name="f.参数.elm[1]" x=30 y=80 type="music"]
[s]
;---------------------------------------------------------------
*停止音乐
[iscript]
//默认值
if (f.参数.tagname!="stopBGM")
{
   f.参数.tagname="stopBGM";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
*播放音效
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_playSFX")
{
   f.参数.tagname="res_playSFX";
   f.参数.elm[0]=f.setting.sound;
   f.参数.elm[1]="";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="音效" name="f.参数.elm[1]" x=30 y=80 type="sound"]
[s]
;---------------------------------------------------------------
*循环播放音效
[window_down width=450 height=230 title=&"tf.选择按钮"]
[iscript]
//默认值
if (f.参数.tagname!="res_loopSFX")
{
   f.参数.tagname="res_loopSFX";
   f.参数.elm[0]=f.setting.sound;
   f.参数.elm[1]="";
}
[endscript]
[line title="资源" name="f.参数.elm[0]" x=30 y=50]
[line title="音效" name="f.参数.elm[1]" x=30 y=80 type="sound"]
[s]
;---------------------------------------------------------------
*停止音效
[iscript]
//默认值
if (f.参数.tagname!="stopSFX")
{
   f.参数.tagname="stopSFX";
   f.参数.elm=[];
}
[endscript]
[jump target=*确认]
;---------------------------------------------------------------
;其他
;---------------------------------------------------------------
*直接输入
[input name=f.行内容 title=直接输入指令 prompt="请输入指令内容：      "]
[iscript]
if (f.行内容!='') 
{
f.脚本内容[f.当前脚本行]=f.行内容;
f.分析[f.当前脚本行]=getTagDic(f.行内容);
}
[endscript]
[jump target=*关闭选单]
