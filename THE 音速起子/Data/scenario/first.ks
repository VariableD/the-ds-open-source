*start
;---------------------------------------------------------------
;DLL插件
;---------------------------------------------------------------
[loadplugin module="dirlist.dll"]
;---------------------------------------------------------------
;层设定
;---------------------------------------------------------------
[laycount messages=7 layers=9]
;底板(框架)
[layopt layer=stage index=1000]
;游戏主菜单
[layopt layer=message0 index=2000]
;-------------------------------------
;底板2(单页背景,描绘文字)
[layopt layer=0 index=3000]
;底板3(事件编辑器描绘)
[layopt layer=1 index=4000]
;编辑器按钮
[layopt layer=message1 index=5000]
;滑动条位置按钮
[layopt layer=2 index=6000]
;右键菜单
[layopt layer=message2 index=7000]
;-------------------------------------
;上层界面底板(指令窗口)
[layopt layer=3 index=8000]
;上层界面窗口
[layopt layer=message3 index=9000]
;预留
[layopt layer=4 index=10000]
;下拉菜单
[layopt layer=message6 index=11000]
;忘记预留了.==b
;-------------------------------------
;最上层界面底板(文件选择，位置设定)
[layopt layer=5 index=12000]
;预留(特殊描绘)
[layopt layer=6 index=13000]
;菜单
[layopt layer=message4 index=14000]
;滚动条标志位置按钮
[layopt layer=7 index=15000]
;预览图片
[layopt layer=8 index=16000]
;最上层下拉菜单
[layopt layer=message5 index=17000]
;-------------------------------------
;最上层提示
[layopt layer=event index=20000]
;---------------------------------------------------------------
;宏
;---------------------------------------------------------------
[call storage="function_load.ks"]
[call storage="function_ui.ks"]
[call storage="function_script.ks"]
[call storage="macro_ui.ks"]

;初始化

[startanchor]
;------------------------------------------------------------------------------------------------------------------------
;预载
;------------------------------------------------------------------------------------------------------------------------
*载入工程
[if exp="sf.project!=void"]
;载入工程相关设定
[eval exp="getSetting()"]

;加载图片
[image layer=event visible="true" left=350 top=200 storage="edit_logo"]
[iscript]
//载入提示
with(kag.fore.event)
{
  //文字
  .font.height=18;
  .drawText(110,50,"..ooOO素材载入中OOoo..", 0x000000);
}
[endscript]
[wait time=1]
[eval exp="f.bg=loadpic('bg')"]
[wait time=1]
[eval exp="f.chara=loadpic('chara')"]
[wait time=1]
[freeimage layer=event]
[endif]


;------------------------------------------------------------------------------------------------------------------------
;开始描绘菜单
;------------------------------------------------------------------------------------------------------------------------
*window
[rclick enabled=false]

[iscript]
//描绘背景
with(kag.fore.base)
{
.width=1024;
.height=700;
//底板
.fillRect(0,0,1024,700,0xFFD4D0C8);

//菜单栏分隔线
.fillRect(0,50,1024,1,0xFFaca899);
.fillRect(0,51,1024,1,0xFFFFFFFF);

//菜单栏竖线
.fillRect(220,5,1,40,0xFFaca899);

//状态栏
//.fillRect(0,690,1024,1,0xFFFFFFFF);
//.fillRect(0,691,1024,1,0xFFaca899);
//.fillRect(0,692,1024,1,0xFFFFFFFF);
}
[endscript]

;---------------------------------------------------------------
;主菜单
;---------------------------------------------------------------
[rclick enabled="false"]
[history enabled="false" output="false"]
;---------------------------------------------------------------
[current layer="message0"]
[er]
;---------------------------------------------------------------
;创建按钮
[locate x=15 y=10]
[button normal=edit_button_open storage="first.ks" target=*打开工程 hint="打开工程：打开project文件夹下的游戏工程"]

;-------------------------------------------------
;当有工程时才显示
;-------------------------------------------------
[if exp="sf.project!=void"]
;不能用的转换OTL
;[locate x=235 y=10]
;[button normal=edit_button_imageconvert storage="first.ks" target=*图形转换 hint="打开图形转换工具"]
;[locate x=235 y=10]
;[button normal=edit_button_sptgen storage="first.ks" target=*脚本转换 hint="convert.bat自动写入"]
;[locate x=335 y=10]
;[button normal=edit_button_package storage="first.ks" target=*文件打包 hint="打开文件打包工具"]
;分页按钮
[locate x=10 y=60]
[button normal=edit_button_page storage="script_main.ks" exp="f.window='',sf.main='script_main.ks'"]
[eval exp="drawButtonCaption('脚本编辑')"]
[locate x=81 y=60]
[button normal=edit_button_page storage="project_main.ks" exp="f.window='',sf.main='project_main.ks'"]
[eval exp="drawButtonCaption('工程设定')"]
[locate x=152 y=60]
[button normal=edit_button_page storage="history_main.ks" exp="f.window='',sf.main='history_main.ks'"]
[eval exp="drawButtonCaption('角色姓名')"]
;-------------------------------------------------
;默认打开脚本编辑窗口，但之后会根据之前记录
[jump storage=&"sf.main" cond="sf.main!=void"]
[jump storage="script_main.ks"]
[endif]
[s]

*图形转换
[iscript]
System.shellExecute(Storages.getLocalName(System.exePath+'tool/'+'image_conversion'+'/') + 'image_conversion.exe');
[endscript]
;返回
[jump storage=&"sf.main" target=*window cond="sf.main!=void"]
[jump storage="script_main.ks"]

*脚本转换
[iscript]
//System.shellExecute(Storages.getLocalName(System.exePath+'tool/'+'spt_gen'+'/') + 'spt_gen_win.exe');
//读入所有txt
f.txt=getsozai("tospt","data");
//按行写入
f.convert=[];
for (var i=0;i<f.txt.count;i++)
{
//spt_gen+" "+"TXT路径"+"输出路径"
f.convert[i]="spt_gen ";
f.convert[i]+=Storages.getLocalName(sf.path+'data/');
f.convert[i]+=f.txt[i]+" ";
//f.convert[i]+=Storages.getLocalName(sf.path+'data/');
}
//保存convert.bat
saveScript(Storages.getLocalName(System.exePath+'tool/'+'spt_gen'+'/')+'convert.bat',f.convert);
////运行convert.bat
////System.shellExecute(Storages.getLocalName(sf.path+'data/') + 'convert.bat');
[endscript]
[jump storage=&"sf.main" target=*window cond="sf.main!=void"]
[jump storage="script_main.ks"]

*文件打包
[iscript]
System.shellExecute(Storages.getLocalName(System.exePath+'tool/'+'res_packer'+'/') + 'res_packer.exe');
[endscript]
;返回
[jump storage=&"sf.main" target=*window cond="sf.main!=void"]
[jump storage="script_main.ks"]
;------------------------------------------------------------------------------------------------------------------------
*打开工程
;------------------------------------------------------------------------------------------------------------------------
;描绘窗口
[window_up width=450 height=400 title="打开工程"]
[iscript]
//取得文件夹名+计算页数
getProject();

//测试用
f.select=0;

//当前窗口
f.window='project';

//描绘BOX
update();
drawButtonLine(20,74,"edit_button_line",10,20);
[endscript]

;翻页按钮
[button_page x=345 y=50 length=220]

[s]

*确认
[if exp="f.project[f.select]!=void"]
[eval exp="sf.project=f.project[f.select]"]
[eval exp="sf.main=''"]
[eval exp="sf.script=''"]
[gotostart ask="false"]
[endif]

*关闭选单
[eval exp="f.window=''"]
[rclick enabled="false"]
[freeimage layer="5"]
[current layer="message4"]
[er]
[layopt layer="message4" visible="false"]
;返回
[jump storage=&"sf.main" target=*window cond="sf.main!=void"]
[gotostart ask="false"]
