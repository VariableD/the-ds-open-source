*start
;--------------------------------------------
[backlay]
[image layer="base" storage="bgd_dot" page="back"]
[image layer=1 page="back" visible="true" storage="没名字的解说_默认_一般"]
[trans method="crossfade" time=500]
[wt]
[wait time=1000]
[layopt layer="message1" visible="true" left=200 top=200]
;--------------------------------------------
[current layer="message1"]
[er]
[解说]嗯，初次见面。[lr]首先自我介绍一下，如对话框上所表示，我是“没名字的解说”。[w]
[fg layer=1 storage="没名字的解说_默认_愤怒"]
[解说]……总之，就是负责这个工具的使用讲解的，[l]没有名字的，[l]被工具作者虐待的[l]……人造人。[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]……嘛，无聊的吐槽就先免了，直接进入正题吧。[w]

*说明菜单
[解说]请选择你想要知道的项目：[r]
[nowait]
[link target=*总体说明]总体说明[endlink]　　　
[link target=*一般操作]一般操作[endlink][r]
[link target=*人造人系统]人造人系统[endlink]　　
[link target=*奇美拉系统]奇美拉系统[endlink][r]
[link target=*版权相关]版权相关[endlink]　　　
[link target=*相关站点]相关站点[endlink]　　　
[link target=*返回标题]返回标题[endlink]
[endnowait]
[s]

*相关站点
[解说]请点击对应的项目：[r]
[nowait]
[link hint="作者个人站" exp="System.shellExecute('http://d.mega-zone.org/')"]
重次元车间
[endlink][r]
[link hint="开发这个软件所用的工具——Krkr/KAG的中文版本制作及讨论站" exp="System.shellExecute('http://kcddp.keyfc.net/bbs/')"]
KCDDP
[endlink]　　
[link hint="Krkr/KAG原开发作者官网（日站）" exp="System.shellExecute('http://kikyou.info/')"]
Kikyou.info
[endlink][r]
[link target=*说明菜单]返回说明[endlink]
[endnowait]
[s]


*版权相关
[解说]总之，这是一个免费且开放原代码的小东西，但是不代表就没有需要注意的问题了。[w]
[解说]首先是，软件代码属于作者[link hint="fs_countd@126.com"]全局变量D（VariableD）[endlink]，而现在随软件发布的男性素材包是由
[link hint="kinr707@hotmail.com"]水螅（Hydrozoa）[endlink]绘制的。[w]
[解说]演示用的素材，背景图片来自
[font color="0x65BCFC"]
[link exp="System.shellExecute('http://guttari8.web.infoseek.co.jp/')"]
ぐったりにゃんこ[endlink][font color="0xFFFFFF"]，而演示动画用的立绘图片来自
[font color="0x65BCFC"]
[link exp="System.shellExecute('http://www8.plala.or.jp/izumitei/')"]いずみ亭
[endlink][font color="0xFFFFFF"]，均为共享素材。[w]
[解说]这东西某种意义上就是为了推广共享素材，让大家制作出真正意义上的“自制游戏”而开发的。[w]
[fg layer=1 storage="没名字的解说_默认_郁闷"]
[解说]虽然说了也大概没人听，我还是要这么说的。[w]
[fg layer=1 storage="没名字的解说_默认_愤怒"]
[解说]不要用这个东西来进行非法素材的编辑，尤其不要把随软件发布的共享素材包和无版权素材进行合成。[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]原代码是开放的，但是请不要在未经作者同意的情况下转载、再次发布或者修改后再次发布。[w]
[fg layer=1 storage="没名字的解说_默认_愤怒"]
[解说]如果你这么做了，别说我没警告你，[lr]
[font color="0xFF0000"]绝对会被作者追着拍到死[font color="0xFFFFFF"]，我说认真的。[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]转载的话，没有意外情况（例如站点本身有发布侵权素材行为）一般都会同意的，所以请至少和作者说一声吧。[w]
[解说]至于应用于游戏中，就不用特意标明使用了这个软件了。[lr]但是，请根据素材包中定下的使用规约，明记素材作者的名字。[w]
[解说]当然，假如发布了游戏愿意告诉一声，作者会很高兴的。[w]
[解说]毕竟，就是为了能看到更多好玩的游戏，才做了这个东西嘛。[w]
[解说]现在的素材包，和以后可能会发布的其他素材包，素材作者享有素材全部版权，可以自定义使用规约。[w]
[解说]所以……[l]那个……[l]可能的话请为这个软件添加更多的素材并和大家分享吧，拜托了！[w]

[jump target=*说明菜单]

*返回标题
[gotostart ask="false"]
[s]


*总体说明
[fg layer=1 storage="没名字的解说_默认_愤怒"]
[解说]这是一个懒得处理图片的作者为了偷懒而写的程序，以上。[w]
[fg layer=1 storage="没名字的解说_默认_惊讶"]
[解说]什么？太短了？不说长点就把我扔进回收站？[w]
[fg layer=1 storage="没名字的解说_默认_愤怒"]
[解说]……[lr]
其实，THE 炼成工坊是一个图片处理的辅助小软件，包括
[font color="0x65BCFC"][link target=*人造人系统 hint="点此跳至人造人系统说明"]纸娃娃系统[endlink][font color="0xFFFFFF"]
和
[font color="0x65BCFC"][link target=*奇美拉系统 hint="点此跳至奇美拉系统说明"]动画合成系统[endlink][font color="0xFFFFFF"]两部分。[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]目前版本2.0，现在依然在开发中，原代码是开放的，
可以在
[font color="0x65BCFC"]
[link exp="System.shellExecute('http://www.svnchina.com/project_detail.php?id=2172')"]这里[endlink][font color="0xFFFFFF"]下载到。[w]
[解说]
开发工具是
[font color="0x65BCFC"]
[link hint="KCDDP Krkr/KAG特别版" exp="System.shellExecute('http://kcddp.keyfc.net/bbs/')" ]
KAGeXpress[endlink][font color="0xFFFFFF"]。[w]
[jump target=*说明菜单]

*一般操作
[fg layer=1 storage="没名字的解说_默认_惊讶"]
[解说]这个……有必要说么？——左键点击、右键取消……连人造人都知道吧？[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]嗯…不过补充一下，在一般界面下，方向键是可以用来调节层的位置的。[w]
[解说]此外，同时按下Shift和方向键，一次移动10象素。[lr]
同时按下Alt和方向键的话，将会所有层一起移动。[w]
[解说]窗口的菜单栏可以用来存取，这也不用说啦。[w]
[jump target=*说明菜单]

*人造人系统
[解说]
“人造人系统”是通过各种部件组合成人物图片，用于制作ACG游戏立绘（角色图）的一个工具。[w]
[解说]
就像一般的作图软件例如photoshop之类的一样，是由“图层”构成的。[w]
[解说]
自由地选择在每个图层中载入身体、头发、眼睛、服装之类的部件，最后输出成完整的人物，就是这样的系统。[w]
[fg layer=1 storage="没名字的解说_默认_郁闷"]
[解说]虽然听起来恶心八拉的有点像分尸，但是连我也是这么造出来的呢。[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]
这个系统包括造型、服装、表情编辑，以及最后的输出设定系统。[w]
[bg storage="step1" time=1000]
[解说]
基本的界面是一样的，请看界面图。[w]
[解说]接下来点一下“素体”旁边的“浏览”按钮，试着载入一张图片。[w]
[bg storage="step2" time=1000]
[解说]现在画面变成了这样……[w]
[fg layer=1 storage="没名字的解说_默认_郁闷"]
[解说]嗯，不要看着我，我知道我们长得像……[w]
[fg layer=1 storage="没名字的解说_默认_一般"]
[解说]接着再试试点其他的“…”符号，继续载入图片吧。[w]
[bg storage="step3" time=1000]
[bg storage="step4" time=1000]
[解说]点下面消息窗口上的“调整颜色”的话……[w]
[bg storage="step5" time=1000]
[bg storage="step6" time=1000]
[解说]就会变成这样。[w]
[bg storage="step7" time=1000]
[解说]接下来看看服装的操作吧。[w]
[bg storage="step8" time=1000]
[解说]继续载入各种服装层的话，就会变成这样了……[w]
[解说]点“服装管理”的话，则会进入这个界面。[w]
[bg storage="step9" time=1000]
[解说]THE 炼成工坊2.0开始支持多套服装的设定，最多可以为一个角色设定十套服装。[w]
[bg storage="step8" time=1000]
[解说]点（<=）和（=>），可以在服装之间互相切换。[w]
[bg storage="step10" time=1000]
[bg storage="step8" time=1000]
[bg storage="step10" time=1000]
[解说]这么说……[l]
[fg layer=1 storage="没名字的解说_新增_一般"]
我也换一套好了……[w]
[bg storage="step11" time=1000]
[解说]表情部分的操作和服装是差不多的，所以不详细说了。[w]
[bg storage="step12" time=1000]
[解说]切换表情……[w]
[bg storage="step11" time=1000]
[bg storage="step12" time=1000]
[解说]继续邪魅一笑（喂）。[w]
[bg storage="step13" time=1000]
[解说]最后到了编辑完，准备输出的界面。[w]
[bg storage="step14" time=1000]
[解说]为了查看角色在游戏中各个场景的配色是不是合适，可以点“载入底图”查看效果，之后请“清除底图”，就会恢复默认的透明背景。[w]
[bg storage="step13" time=1000]
[解说]觉得差不多了，就可以点下横向窗口中的“格式设定”，确认输出的图片大小。[w]
[bg storage="step15" time=1000]
[解说]画面上的红色细线代表实际会输出的范围，可以任意调整。[w]
[解说]一切都完成以后，点“确认输出”，就会按照【角色名_服装名_表情名.png】的格式输出立绘到savedata的【角色名】文件夹中。[w]
[解说]角色名可以在“角色档案”里设定。[w]
[fg layer=1 storage="没名字的解说_新增_愤怒"]
[解说]就像这样……[w]
[bg storage="step16" time=1000]
[解说]“视图调整”可以调节各窗口的排列。[w]
[解说]也可以把窗口拖到自己喜欢的位置。[fg layer=1 storage="没名字的解说_新增_郁闷"]如果觉得我说话妨碍到你看窗口的话，也可以拖动这个对话框。[w]
[fg layer=1 storage="没名字的解说_新增_一般"]
[解说]好了，这个系统已经全部说完啦，接下来请实际去试试吧。[w]
[backlay]
[image layer="base" storage="bgd_dot" page="back"]
[image layer=1 page="back" visible="true" storage="没名字的解说_默认_一般"]
[trans method="crossfade" time=500]
[wt]
[jump target=*说明菜单]

*奇美拉系统
[bg storage="anim1" time=1000]
[fg layer="1" storage="没名字的解说_默认_愤怒"]
[解说]啊，这就是那名字起得很牛X实际上只是偷懒功能的动画合成系统。[w]
[fg layer="1" storage="没名字的解说_默认_一般"]
[解说]简单的说，开发游戏的程序和美工，有时候会为图片分层的问题烦恼。[w]
[解说]像是眨眼动画之类的效果，通常要把各帧图片合成一张动画图。[w]
[fg layer="2" storage="动画格式" l=400 t=0]
[解说]用photoshop制作这种东西虽然不难，却很没技术含量。[w]
[fg layer=3 storage="立绘格式" l=400 t=120]
[解说]对美工来说，像上面这样立绘+一张通用的动画的分割形式是最方便的做法。[lr]
[freeimage layer="2"]
[fg layer=3 storage="联排形式" l=-50 t=122]
而对程序来说，把图层合并，让每张立绘都变成动画图，操作起来会更方便一些。[w]
[freeimage layer="3"]
[fg layer="1" storage="没名字的解说_默认_郁闷"]
[解说]所以，这是懒惰的程序和懒惰的美工之间不可调和的矛盾。[w]
[fg layer="1" storage="没名字的解说_默认_一般"]
[解说]俗话说，这个世界的发展是懒人推动的。[lr]为了避免这样的情况，把大家的工作量都减到最低，某个懒惰的程序制作了这个系统……[w]
[解说]那么，来实际演示一下吧。[w]
[bg storage="anim2" time=1000]
[解说]首先点“载入静态图”，将单张立绘载入。[w]
[bg storage="anim3" time=1000]
[解说]接下来点“载入动态图”，载入如图所示的头像动画。[w]
[freeimage layer=1]
[bg storage="anim4" time=1000]
[解说]点选“动画设定”的话，就能看到现在这样的画面。[w]
[bg storage="anim5" time=1000]
[解说]修改左边的“帧数”和“行数”，可以对动画图片进行分格。[lr]
修改数字后点“确认”，就可以看到黄色的细线标志出当前的分格情况。[w]
[解说]下方的“时间”则是在预览效果时的每帧间隔时间。[w]
[bg storage="anim6" time=1000]
[fg layer="1" storage="没名字的解说_默认_一般"]
[解说]设定好分格之后，可以将动态和静态层重叠，用方向键对好位置。[w]
[解说]点“预览效果”的话，就可以看到动画播放出来的情况。[w]
[解说]觉得满意的话，设定好“输出范围”，接着就可以点“确认输出”了。[w]
[解说]将会以静态图片为文件名，输出到savedata的animation文件夹下。[w]
[解说]以上，解说完毕。[w]
[backlay]
[image layer="base" storage="bgd_dot" page="back"]
[image layer=1 page="back" visible="true" storage="没名字的解说_默认_一般"]
[trans method="crossfade" time=500]
[wt]
[jump target=*说明菜单]



























