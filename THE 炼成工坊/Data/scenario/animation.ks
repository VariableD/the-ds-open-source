*start
[image layer=base storage="bgd_dot" left=0 top=0 visible="true"]
[eval exp="f.静态图='empty'"]
[eval exp="f.动态图='empty'"]

[eval exp="f.clipwidth=kag.fore.layers[25].width\f.帧数"]
[eval exp="f.clipheight=kag.fore.layers[25].height\f.行数"]
;-------------------------------------------------------
*刷新画面|动画合成
[动画层操作]
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[eval exp="DrawScale()"]
[image layer=24 storage=&f.静态图 visible="true" left=&f.left top=&f.top]
[image layer=25 storage=&f.动态图 visible="true" clipleft=0 cliptop=0 clipwidth=&f.clipwidth clipheight=&f.clipheight left=&f.x top=&f.y]
[layopt layer="message2" visible="true"]
[current layer="message2" page="fore"]
[er]
[style align="center"]
[nowait]
〔动画合成〕[r]
[r]
[r]
静态图层　　
[button normal=view exp="LoadAnimation(24,'in')" target=*刷新画面][r]
[r]
动画图层　　
[button normal=view exp="LoadAnimation(25,'in')" target=*刷新画面][r]
[r]
[link target=*动画设定]动画设定[endlink][r]
[r]
[link target=*输出范围]输出范围[endlink][r]
[r]
[link target=*预览效果]预览效果[endlink][r]
[r]
[link target=*确认输出]确认输出[endlink][r]

[s]
;----------------------------------------------------------------------------------------------
*确认输出
[禁止方向键]

[iscript]
var Animation = new Layer(kag, kag.fore.base);
Animation.setImageSize(f.width*f.帧数*f.行数,f.height);
[endscript]

[eval exp="tf.行数=0"]

*行数
[eval exp="tf.帧数=0"]

*帧数
[image layer=25 storage=&f.动态图 visible="true" clipleft=&"f.clipwidth*tf.帧数" cliptop=&"f.clipheight*tf.行数" clipwidth=&f.clipwidth clipheight=&f.clipheight]
[wait time=&"f.每帧时间"]
[eval exp="tf.帧数++"]
[eval exp="OutAnimation()"]
[jump target=*帧数 cond="tf.帧数<f.帧数"]

[eval exp="tf.行数++"]
[jump target=*行数 cond="tf.行数<f.行数"]

[iscript]
var pa = System.exePath + "/savedata/animation/"+Storages.chopStorageExt(Storages.extractStorageName(f.静态图))+".png";
Animation.saveLayerImagePng(pa);
[endscript]

[jump storage="animation.ks" target=*刷新画面]
;----------------------------------------------------------------------------------------------
*输出范围
[call storage="setting.ks" target="*start"]
[jump storage="animation.ks" target="*刷新画面"]
;----------------------------------------------------------------------------------------------
*预览效果
[禁止方向键]
[eval exp="tf.行数=0"]

*行数
[eval exp="tf.帧数=0"]

*帧数
[image layer=25 storage=&f.动态图 visible="true" clipleft=&"f.clipwidth*tf.帧数" cliptop=&"f.clipheight*tf.行数" clipwidth=&f.clipwidth clipheight=&f.clipheight]
[wait time=&"f.每帧时间"]
[eval exp="tf.帧数++"]
[jump target=*帧数:2 cond="tf.帧数<f.帧数"]

[eval exp="tf.行数++"]
[jump target=*行数:2 cond="tf.行数<f.行数"]

[jump storage="animation.ks" target=*刷新画面]
;----------------------------------------------------------------------------------------------
*动画设定|
[禁止方向键]
[locklink]
[rclick jump="true" enabled="true" storage="animation.ks" target=*关闭]
[backlay]
[image layer=27 page="back" left=0 top=0 storage="bgd_empty" visible="true"]
[layopt layer="message6" visible="true" page="back"]
[current layer="message6" withback="true"]
[er][nowait]
动画设定[r]
[r]
帧数：[r]
[edit name=f.帧数 bgcolor="0x000000" opacity=0 color="0xFFFFFF" length=160][r]
行数：[r]
[edit name=f.行数 bgcolor="0x000000" opacity=0 color="0xFFFFFF" length=160][r]
时间：[r]
[edit name=f.每帧时间 bgcolor="0x000000" opacity=0 color="0xFFFFFF" length=160][r]
[r]
[link storage="animation.ks" target=*确认]确认[endlink]　
[link storage="animation.ks" target=*关闭]关闭[endlink]　
[endnowait]
[image layer=26 page="back" left="224" top="50" storage="&f.动态图" visible=true]
[trans method="crossfade" time=50]
[wt]
[eval exp="DrawAnimation()"]
[s]
;----------------------------------------------------------------------------------------------
*关闭
[rclick enabled="false"]
[layopt layer="message6" visible="false"]
[freeimage layer=26]
[freeimage layer=27]
[jump storage="animation.ks" target=*刷新画面]
;----------------------------------------------------------------------------------------------
*确认
[commit]
;刷新画面
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[jump storage="animation.ks" target=*动画设定]
