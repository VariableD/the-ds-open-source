*start|&f.name
[image layer=base storage="bgd_dot" left=0 top=0 visible="true"]
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[eval exp="DrawScale()"]

[eval exp="f.current=7"]
[eval exp="f.currentname='内衣'"]
[eval exp="f.path='cloth'"]

;------------------------------------------------------------------------------------------------------------------------
*刷新画面|&f.name
[rclick enabled="false"]
[纸娃娃操作]
;------------------------------------------------------------------------------------------------------------------------
;保存服装
[freeimage layer=1]
[freeimage layer=2]
[freeimage layer=7]
[freeimage layer=8]
[freeimage layer=9]

[freeimage layer=20]
[freeimage layer=21]
[freeimage layer=23]

[eval exp="LoadCloth()"]
[eval exp="UpdateAll()"]
;------------------------------------------------------------------------------------------------------------------------

[layopt layer="message1" visible="true"]
[current layer="message1" page="fore"]
[er]
[style linespacing=10]

[nowait]
当前操作层：[emb exp="f.currentname"][r]
;无图片时的说明
[if exp="f.操作层[f.current].storage==''"]
～请先点选层旁边的"浏览"字样载入图片～
[endif]

;有图片时的显示
[if exp="f.操作层[f.current].storage!=''"]
[link storage="cloth.ks" hint="方向键连续移动／Shift+方向键移动10像素/Alt+方向键移动全部层" target=*调整位置]调整位置[endlink]　
[link storage="cloth.ks" target=*改变颜色 hint="点这里可以改变部件的颜色"]改变颜色[endlink]　
[link storage="cloth.ks" target=*清空图层 hint="点这里可以清空当前层"]清空图层[endlink][r]
[link hint="就是载入的图片文件名"]部件：[endlink]
[emb exp="Storages.extractStorageName(f.操作层[f.current].storage)"][r]
[eval exp="f.x=kag.fore.layers[f.current].left-sf.bgdx"]
[eval exp="f.y=kag.fore.layers[f.current].top-sf.bgdy"]
[link hint="这里的坐标是相对于背景左上点的坐标"]坐标：[endlink](x=[emb exp="f.x"],y=[emb exp="f.y"],index=[emb exp="kag.fore.layers[f.current].absolute"])
[endif]

[endnowait]
;------------------------------------------------------------------------------------------------------------------------
[layopt layer="message2" visible="true"]
[current layer="message2" page="fore"]
[er]
[style align="center"]
[nowait]
〔服饰设定〕[r]
[r]
[link exp="(SaveCloth()),(f.当前服装-- if (f.当前服装>0))" target=*刷新画面]<=[endlink]　
[emb exp="f.当前服装"][emb exp="f.服装名[f.当前服装]"]　
[link exp="(SaveCloth()),(f.当前服装++ if (f.服装[f.当前服装+1]!=''))" target=*刷新画面]=>[endlink]

[r]
[r]
[Layer name="背后" path="accesary" num=1 target=*刷新画面 storage="cloth.ks"]
[Layer name="发带" path="cloth" num=2 target=*刷新画面 storage="cloth.ks"]
[r]
[Layer name="内衣" path="cloth" num=7 target=*刷新画面 storage="cloth.ks"]
[Layer name="下装" path="cloth" num=8 target=*刷新画面 storage="cloth.ks"]
[Layer name="上装" path="cloth" num=9 target=*刷新画面 storage="cloth.ks"]
[r]
[Layer name="外套" path="cloth" num=20 target=*刷新画面 storage="cloth.ks"]
[Layer name="饰一" path="accesary" num=21 target=*刷新画面 storage="cloth.ks"]
[Layer name="饰二" path="accesary" num=23 target=*刷新画面 storage="cloth.ks"]
[r]
[r]
[link storage="cloth.ks" target=*服装管理]服装管理[endlink][r]
[link storage="cloth.ks" target=*全部清空]全部清空[endlink][r]
----------
[r]
[link storage="image.ks" target=*start exp="SaveCloth()"]造型基础[endlink][r]
[link storage="emotion.ks" target=*start exp="SaveCloth()"]表情编辑[endlink][r]
[link storage="output.ks" target=*start exp="SaveCloth()"]预览输出[endlink][r]

[endnowait]
[s]
;------------------------------------------------------------------------------------------------------------------------

*调整位置
[call storage="position.ks"]
[eval exp="SaveCloth()"]
[jump target=*刷新画面]

*改变颜色
[call storage="color.ks"]
[eval exp="SaveCloth()"]
[jump target=*刷新画面]

*清空图层
[eval exp="ClearLayer(f.current)"]
[eval exp="SaveCloth()"]
[jump target=*刷新画面]


*全部清空
[eval exp="f.确认=askYesNo('确认要清空造型层吗？')"]

[if exp="f.确认==true"]

[freeimage layer=1]
[freeimage layer=2]
[freeimage layer=7]
[freeimage layer=8]
[freeimage layer=9]

[freeimage layer=20]
[freeimage layer=21]
[freeimage layer=23]

[eval exp="ClearLayer(1)"]
[eval exp="ClearLayer(2)"]

[eval exp="ClearLayer(7)"]
[eval exp="ClearLayer(8)"]
[eval exp="ClearLayer(9)"]

[eval exp="ClearLayer(20)"]
[eval exp="ClearLayer(21)"]
[eval exp="ClearLayer(23)"]

[eval exp="SaveCloth()"]
[endif]
[jump target=*刷新画面]

*服装管理
[call storage="manage.ks"]
[jump target=*刷新画面]
