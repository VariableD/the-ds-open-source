*start
[iscript]
//-----------------------------------------------------------
//按键调整位置
//-----------------------------------------------------------
function AnimationKeyDown(key,shift)
{

if (key==17) return true;

//按键判断/方向键
        if(key == VK_LEFT && shift==void)
               kag.fore.layers[25].left--;
        if(key == VK_RIGHT && shift==void)
               kag.fore.layers[25].left++;
        if(key == VK_UP && shift==void)
               kag.fore.layers[25].top--;
        if(key == VK_DOWN && shift==void)
               kag.fore.layers[25].top++;
        if(key == VK_LEFT && shift==ssRepeat)
               kag.fore.layers[25].left--;
        if(key == VK_RIGHT && shift==ssRepeat)
               kag.fore.layers[25].left++;
        if(key == VK_UP && shift==ssRepeat)
               kag.fore.layers[25].top--;
        if(key == VK_DOWN && shift==ssRepeat)
               kag.fore.layers[25].top++;
               
//按键判断/方向键+Shift键
        if(key == VK_LEFT && shift==ssShift)
               kag.fore.layers[25].left-=10;
        if(key == VK_RIGHT && shift==ssShift)
               kag.fore.layers[25].left+=10;
        if(key == VK_UP && shift==ssShift)
               kag.fore.layers[25].top-=10;
        if(key == VK_DOWN && shift==ssShift)
               kag.fore.layers[25].top+=10;

//按键判断/方向键+Ctrl键
        if(key == VK_LEFT && shift==ssCtrl)
{
               kag.fore.layers[24].left-=1;
               kag.fore.layers[25].left-=1; 
               f.left-=1;
}
        if(key == VK_RIGHT && shift==ssCtrl)
{
               kag.fore.layers[24].left+=1;
               kag.fore.layers[25].left+=1; 
               f.left+=1;
}
        if(key == VK_UP && shift==ssCtrl)
{
               kag.fore.layers[24].top-=1;
               kag.fore.layers[25].top-=1;
               f.top-=1;
}
        if(key == VK_DOWN && shift==ssCtrl)
{
               kag.fore.layers[24].top+=1;
               kag.fore.layers[25].top+=1; 
               f.top+=1;
}     
//按键判断/方向键+ALT键
        if(key == VK_LEFT && shift==ssAlt)
{
               kag.fore.layers[24].left-=10;
               kag.fore.layers[25].left-=10; 
               f.left-=10;
}
        if(key == VK_RIGHT && shift==ssAlt)
{
               kag.fore.layers[24].left+=10;
               kag.fore.layers[25].left+=10; 
               f.left+=10;
}
        if(key == VK_UP && shift==ssAlt)
{
               kag.fore.layers[24].top-=10;
               kag.fore.layers[25].top-=10;
               f.top-=10;
}
        if(key == VK_DOWN && shift==ssAlt)
{
               kag.fore.layers[24].top+=10;
               kag.fore.layers[25].top+=10; 
               f.top+=10;
}

f.x=kag.fore.layers[25].left;
f.y=kag.fore.layers[25].top;
        kag.process('', '*刷新画面');


        return true;           
}

//---------------------------------------------------------------------------
function myOnKeyDown(key, shift)
{

if (key==17) return true;

//按键判断/方向键
        if(key == VK_LEFT && shift==void)
               f.操作层[f.current].left--;
        if(key == VK_RIGHT && shift==void)
               f.操作层[f.current].left++;
        if(key == VK_UP && shift==void)
               f.操作层[f.current].top--;
        if(key == VK_DOWN && shift==void)
               f.操作层[f.current].top++;
        if(key == VK_LEFT && shift==ssRepeat)
               f.操作层[f.current].left--;
        if(key == VK_RIGHT && shift==ssRepeat)
               f.操作层[f.current].left++;
        if(key == VK_UP && shift==ssRepeat)
               f.操作层[f.current].top--;
        if(key == VK_DOWN && shift==ssRepeat)
               f.操作层[f.current].top++;
      
//按键判断/方向键+Shift键
        if(key == VK_LEFT && shift==ssShift)
               f.操作层[f.current].left-=10;
        if(key == VK_RIGHT && shift==ssShift)
               f.操作层[f.current].left+=10;
        if(key == VK_UP && shift==ssShift)
               f.操作层[f.current].top-=10;
        if(key == VK_DOWN && shift==ssShift)
               f.操作层[f.current].top+=10;
               
//按键判断/方向键+ALT键
        if(key == VK_LEFT && shift==ssAlt)
                f.left-=10;
        if(key == VK_RIGHT && shift==ssAlt)
                f.left+=10;
        if(key == VK_UP && shift==ssAlt)
                f.top-=10;
        if(key == VK_DOWN && shift==ssAlt)
                f.top+=10;
                
           SaveCloth();
           SaveEmotion();
       //刷新画面
        UpdateLayer();
        kag.process('', '*刷新画面');
        return true;

}
//-----------------------------------------------------------
//对齐所有层
//-----------------------------------------------------------
function UpdateLayer()
{

     for (var i=1;i<=23;i++)
     {
kag.fore.layers[i].left=(f.操作层[i].left)*1+f.left*1;
kag.fore.layers[i].top=(f.操作层[i].top)*1+f.top*1;
     }
     
}

//-----------------------------------------------------------
//选择层
//-----------------------------------------------------------
function EditImage(num,name,path)
 {
//选择当前层
   f.current=num;
   f.currentname=name;
   f.path=path;
 }
 
 //-----------------------------------------------------------
//读入新图片
//-----------------------------------------------------------
function LoadAnimation(layer,path)
{

var params = %[
filter : ["图象文件(*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg)|*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg"],
filterIndex : 1,
name : "",
initialDir : System.exePath+"/"+"default/"+path+"/",
title : "选择文件",
save : false,
];
  if (Storages.selectFile(params)) 
     {
     //读取图片
    kag.fore.layers[layer].loadImages(%['storage'=>params.name,'visible'=>false,'left'=>f.bgdx, 'top'=>f.bgdy]);
     if (layer==24)
     f.静态图=params.name;
     
     if (layer==25)
     {
     f.动态图=params.name;
     f.clipwidth=kag.fore.layers[25].width/f.帧数;
     f.clipheight=kag.fore.layers[25].height/f.行数;
     }
     
       }
}


function LoadImage(path)
{

var params = %[
filter : ["图象文件(*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg)|*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg"],
filterIndex : 1,
name : "",
initialDir : System.exePath+"/"+"default/"+path+"/",
title : "选择文件",
save : false,
];

  if (Storages.selectFile(params)) 
     {
     //读取图片
     kag.fore.layers[f.current].loadImages(%['storage'=>params.name,'visible'=>true,'left'=>f.left, 'top'=>f.top]);
     

     var rfloor=f.操作层[f.current].rfloor;
     var gfloor=f.操作层[f.current].gfloor;
     var bfloor=f.操作层[f.current].bfloor;
     var rceil=f.操作层[f.current].rceil;
     var gceil=f.操作层[f.current].gceil;
     var bceil=f.操作层[f.current].bceil;
     //初始化数据
     f.操作层[f.current]=%[
     "storage"=>params.name,
     "left"=>0,
     "top"=>0,
     "rfloor"=>rfloor,
     "gfloor"=>gfloor,
     "bfloor"=>bfloor,
     "rceil"=>rceil,
     "gceil"=>gceil,
     "bceil"=>bceil,
     ];
       }

  }
 //-----------------------------------------------------------
//清除层消息
//-----------------------------------------------------------
function ClearLayer(num)
{
f.操作层[num]=%[
    "storage" => "",
    "left" => 0,
    "top" =>0,
    "rfloor"=>0,
    "gfloor" =>0,
    "bfloor" =>0,
    "rceil"=>255,
    "gceil" =>255,
    "bceil" =>255,
]; // 创建字典

}

//-----------------------------------------------------------
//刷新颜色
//-----------------------------------------------------------

var Backcolor=new Timer(UpdateColor,"");
Backcolor.interval=25;

function UpdateColor()
{
//图片层本身的修改
kag.fore.layers[f.current].loadImages(%['storage'=>f.操作层[f.current].storage,'visible'=>true,'rfloor'=>f.操作层[f.current].rfloor,'gfloor'=>f.操作层[f.current].gfloor,'bfloor'=>f.操作层[f.current].bfloor,'rceil'=>f.操作层[f.current].rceil,'gceil'=>f.操作层[f.current].gceil,'bceil'=>f.操作层[f.current].bceil]);

//一起修改的图片层
   if (f.withlayer==true && f.withcurrent>0 && f.withcurrent<23 && f.操作层[f.withcurrent].storage!='')
 {
     f.操作层[f.withcurrent].rfloor=f.操作层[f.current].rfloor;
     f.操作层[f.withcurrent].gfloor=f.操作层[f.current].gfloor;
     f.操作层[f.withcurrent].bfloor=f.操作层[f.current].bfloor;
     
     f.操作层[f.withcurrent].rceil=f.操作层[f.current].rceil;
     f.操作层[f.withcurrent].gceil=f.操作层[f.current].gceil;
     f.操作层[f.withcurrent].bceil=f.操作层[f.current].bceil;
     
     kag.fore.layers[f.withcurrent].loadImages(%['storage'=>f.操作层[f.withcurrent].storage,'visible'=>true,'rfloor'=>f.操作层[f.current].rfloor,'gfloor'=>f.操作层[f.current].gfloor,'bfloor'=>f.操作层[f.current].bfloor,'rceil'=>f.操作层[f.current].rceil,'gceil'=>f.操作层[f.current].gceil,'bceil'=>f.操作层[f.current].bceil]);
  }

}

//-----------------------------------------------------------
//读入背景
//-----------------------------------------------------------
function Loadbgd()
{
var params = %[
filter : ["图象文件(*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg)|*.bmp;*.png;*.jpg;*.jpeg;*.eri;*.tlg"],
filterIndex : 1,
name : "",
initialDir : System.exePath+"/"+"bgd"+"/",
title : "选择文件",
save : false,
];
  //读入图片
  if (Storages.selectFile(params))
     {
     kag.fore.layers[0].loadImages(%['storage'=>params.name,'visible'=>true,'left'=>sf.bgdx, 'top'=>sf.bgdy]);
     }

}
//-----------------------------------------------------------
//描绘输出范围
//-----------------------------------------------------------
function DrawScale()
{
            //横
            kag.fore.base.colorRect(f.bgdx, f.bgdy                       , f.width-1, 1, 0xFF0000, 128);
            kag.fore.base.colorRect(f.bgdx, f.bgdy*1+f.height*1-1, f.width-1, 1, 0xFF0000, 128);
            //竖
            kag.fore.base.colorRect(f.bgdx                      , f.bgdy, 1, f.height-1, 0xFF0000, 128);
            kag.fore.base.colorRect(f.bgdx*1+f.width*1-1, f.bgdy, 1, f.height-1, 0xFF0000, 128);
}

function DrawAnimation()
{
            //横
            kag.fore.layers[27].colorRect(224, 50                                              , kag.fore.layers[26].width-1, 1, 0xFFFF00, 128);

            //竖
            kag.fore.layers[27].colorRect(224                                            , 50, 1, kag.fore.layers[26].height-1, 0xFFFF00, 128);

            //帧
         for (var i=1;i<=f.帧数;i++)
           {
            kag.fore.layers[27].colorRect(224+(kag.fore.layers[26].width/f.帧数)*i-1, 50, 1, kag.fore.layers[26].height-1, 0xFFFF00, 128);    
           }
          for (var i=1;i<=f.行数;i++)
          {
            kag.fore.layers[27].colorRect(224, 50+(kag.fore.layers[26].height/f.行数)*i-1, kag.fore.layers[26].width-1, 1, 0xFFFF00, 128);
          }

//计算截取位置
f.clipwidth=kag.fore.layers[26].width/f.帧数;
f.clipheight=kag.fore.layers[26].height/f.行数;

}
//-----------------------------------------------------------
//输出图片
//-----------------------------------------------------------
function Output()
{

var temp = new Layer(kag, kag.fore.base);
var out = new Layer(kag, kag.fore.base);
temp.setImageSize(1024, 768);
out.setImageSize(f.width, f.height);

//-----------------------------------------------------------------------
//取得全画面数据
for (var i=0;i<=23;i++)
{
if (kag.fore.layers[i].visible==true)
temp.operateRect(0, 0,kag.fore.layers[i],0,0, 1024, 768);
}
//进行截取
out.operateRect(0,0,temp,f.bgdx,f.bgdy,f.width,f.height);
var pa = System.exePath + "/savedata/"+f.name+"/"+f.name+"_"+f.服装名[f.当前服装]+"_"+f.表情名[f.当前表情];
out.saveLayerImagePng(pa+".png");

}
//------------------------------------------------------------
function OutAnimation()
{
var temp = new Layer(kag, kag.fore.base);
var face = new Layer(kag, kag.fore.base);
var out = new Layer(kag, kag.fore.base);
temp.setImageSize(1024, 768);
face.setImageSize(f.clipwidth, f.clipheight);
out.setImageSize(f.width, f.height);

//取得画面数据
if (kag.fore.layers[24].visible==true && kag.fore.layers[24].hasImage==true)
temp.operateRect(kag.fore.layers[24].left, kag.fore.layers[24].top,kag.fore.layers[24],0,0, 1024, 768);
//取得画面数据
if (kag.fore.layers[25].visible==true && kag.fore.layers[25].hasImage==true)
face.operateRect(0, 0,kag.fore.layers[25],f.clipwidth*(tf.帧数-1),f.clipheight*tf.行数, f.clipwidth, f.clipheight);
temp.operateRect(kag.fore.layers[25].left, kag.fore.layers[25].top,face ,0,0, 1024, 768);
//每张输出的图片
out.operateRect(0,0,temp,f.bgdx,f.bgdy,f.width,f.height);


//假如需要输出成单帧可以用这个
//var pa = System.exePath + "/out/"+tf.行数+tf.帧数+".png";
//out.saveLayerImagePng(pa);

Animation.operateRect(f.width*(tf.帧数-1),f.height*tf.行数,out,0,0,f.width,f.height);
}


//-----------------------------------------------------------
//增加服装/表情
//-----------------------------------------------------------
function AddCloth()
{

f.服装.add(%[
    "back" => f.范例层,
    "hair" => f.范例层,
    "under" => f.范例层,
    "down" => f.范例层,
    "up" => f.范例层,
    "coat" => f.范例层,
    "acce1" => f.范例层,
    "acce2" => f.范例层,
]);
}

function AddEmotion()
{
f.表情.add(%[
    "shadow" => f.操作层[10],
    "braw" => f.操作层[11],
    "eye" =>f.操作层[12],
    "pupil" =>f.操作层[13],
    "mouth" => f.操作层[15],
    "emot1" => f.操作层[17],
    "emot2" => f.操作层[19],
]);
}
//-----------------------------------------------------------
//存储服装数据/表情数据
//-----------------------------------------------------------
function SaveCloth()
{
f.服装[f.当前服装]=%[
    "back" => f.操作层[1],
    "hair" => f.操作层[2],
    "under" => f.操作层[7],
    "down" => f.操作层[8],
    "up" => f.操作层[9],
    "coat" => f.操作层[20],
    "acce1" => f.操作层[21],
    "acce2" => f.操作层[23],
];
}

function SaveEmotion()
{
f.表情[f.当前表情]=%[
    "shadow" => f.操作层[10],
    "braw" => f.操作层[11],
    "eye" => f.操作层[12],
    "pupil" => f.操作层[13],
    "mouth" => f.操作层[15],
    "emot1" => f.操作层[17],
    "emot2" => f.操作层[19],
];
}
//-----------------------------------------------------------
//载入服装数据/表情数据
//-----------------------------------------------------------
function LoadCloth()
{
f.操作层[1]=f.服装[f.当前服装].back;
f.操作层[2]=f.服装[f.当前服装].hair;
f.操作层[7]=f.服装[f.当前服装].under;
f.操作层[8]=f.服装[f.当前服装].down;
f.操作层[9]=f.服装[f.当前服装].up;
f.操作层[20]=f.服装[f.当前服装].coat;
f.操作层[21]=f.服装[f.当前服装].acce1;
f.操作层[23]=f.服装[f.当前服装].acce2;
}

function LoadEmotion()
{
f.操作层[10]=f.表情[f.当前表情].shadow;
f.操作层[11]=f.表情[f.当前表情].braw;
f.操作层[12]=f.表情[f.当前表情].eye;
f.操作层[13]=f.表情[f.当前表情].pupil;
f.操作层[15]=f.表情[f.当前表情].mouth;
f.操作层[17]=f.表情[f.当前表情].emot1;
f.操作层[19]=f.表情[f.当前表情].emot2;
}
//-----------------------------------------------------------
//刷新全部
//-----------------------------------------------------------
function UpdateAll()
{

for (var i=1;i<=23;i++)
{
if (f.操作层[i].storage!='')
kag.fore.layers[i].loadImages(%['storage'=>f.操作层[i].storage,'visible'=>true,'left'=>f.操作层[i].left+f.left*1,'top'=>f.操作层[i].top+f.top*1,'rfloor'=>f.操作层[i].rfloor,'gfloor'=>f.操作层[i].gfloor,'bfloor'=>f.操作层[i].bfloor,'rceil'=>f.操作层[i].rceil,'gceil'=>f.操作层[i].gceil,'bceil'=>f.操作层[i].bceil]);
}

}
[endscript]
;------------------------------
[return]
