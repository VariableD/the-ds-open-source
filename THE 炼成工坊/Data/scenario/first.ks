[cm]
[call storage="macro.ks"]
[call storage="function.ks"]

[jump target=*start cond="sf.默认设定==true"]
;-------------------------------------------
;默认数值设定
;-------------------------------------------
;背景图片位置
[eval exp="sf.bgdx=0"]
[eval exp="sf.bgdy=0"]
[eval exp="sf.版面选择=4"]
[eval exp="sf.默认设定=true"]
;当前操作层

*start
[startanchor]

*title
[history enabled="false" output="false"]
[rclick enabled="false"]

[stoptrans]

[backlay]
[image layer="base" page="back" storage="bgd_title"]
[current layer="message0" page="back"]
[layopt layer="message0" visible="true" page="back"]
[style align="center"]
[nowait]
[r][r][r][r][r][r][r][r][r][r][r]
[r][r]
[link target=*开始制作 hint="纸娃娃制作系统"]人造人[endlink][r]
[link target=*动画合成 hint="动画合成系统"]奇美拉[endlink][r]
[link target=*使用说明 hint="使用说明"]炼成手册[endlink][r]
[link exp="kag.shutdown()"]关闭程序[endlink]
[endnowait]
[style align="left"]
[trans method="crossfade" time=1000]
[wt]

[s]

*使用说明
[current layer="message0"]
[er]
[jump storage=guide.ks target=*start]

*动画合成
;------------------------
;进行默认设定
;------------------------
[eval exp="f.帧数=4"]
[eval exp="f.行数=1"]
[eval exp="f.每帧时间=100"]
;-----------------------------------------
;素体图片初始读入位置
[版面位置]
[eval exp="f.width=800"]
[eval exp="f.height=600"]
[eval exp="f.x=f.bgdx"]
[eval exp="f.y=f.bgdy"]
[eval exp="f.left=f.bgdx"]
[eval exp="f.top=f.bgdy"]
[current layer="message0"]
[er]
[jump storage="animation.ks" target=*start]


*开始制作
;------------------------
;进行默认设定
;------------------------
[eval exp="f.name=''"]
[eval exp="f.gender=''"]
[eval exp="f.age=''"]
[eval exp="f.withlayer=true"]
;------------------------
;指定当前服装编号
[eval exp="f.当前服装=0"]

[eval exp="f.服装名=[]"]
[eval exp="f.服装名[0]='默认'"]
;------------------------
;指定当前表情编号
[eval exp="f.当前表情=0"]
[eval exp="f.表情名=[]"]
[eval exp="f.表情名[0]='默认'"]

;------------------------
[iscript]
//创建默认数据
f.范例层=%[
    "storage" => "",
    "left" => 0,
    "top" =>0,
    "rfloor"=>0,
    "gfloor" =>0,
    "bfloor" =>0,
    "rceil"=>255,
    "gceil" =>255,
    "bceil" =>255,
]; // 创建字典

f.操作层=[];

for (var i=1;i<=23;i++)
{
f.操作层[i]=f.范例层;
}
//------------------------------------
f.服装=[];
f.服装[0]=%[
    "back" => f.范例层,
    "hair" => f.范例层,
    "under" => f.范例层,
    "down" => f.范例层,
    "up" => f.范例层,
    "coat" => f.范例层,
    "acce1" => f.范例层,
    "acce2" => f.范例层,
];

//------------------------------------
//创建默认数据

f.表情=[];

f.表情[0]=%[
    "shadow" => f.范例层,
    "braw" => f.范例层,
    "eye" => f.范例层,
    "pupil" => f.范例层,
    "mouth" => f.范例层,
    "emot1" => f.范例层,
    "emot2" => f.范例层,
];

[endscript]
;--------------------------------------------
[版面位置]
[eval exp="f.width=800"]
[eval exp="f.height=600"]

[current layer="message0"]
[er]
[jump storage="image.ks" target=*start]




