*start
[loadplugin module="layerExSave.dll"]
;------------------------------------------------------------------------------------------
;层顺序设定
;------------------------------------------------------------------------------------------
[laycount layer=28 message=7]
;base层[透明网格]
;0层[角色背景层]
[layopt layer=0 index=100]
;------------------------------------
;背后装饰
[layopt layer=1 index=200]
;发带
[layopt layer=2 index=300]
;-------------------------------------
;后发
[layopt layer=3 index=400]
;素体
[layopt layer=4 index=500]
;素体附件
[layopt layer=5 index=600]
;脸形
[layopt layer=6 index=700]
;-------------------------------------
;内衣
[layopt layer=7 index=800]
;下装
[layopt layer=8 index=900]
;上装
[layopt layer=9 index=1000]
;-------------------------------------
;阴影
[layopt layer=10 index=1100]
;眉
[layopt layer=11 index=1200]
;眼
[layopt layer=12 index=1300]
;瞳
[layopt layer=13 index=1400]
;鼻
[layopt layer=14 index=1500]
;口
[layopt layer=15 index=1600]
;面妆1
[layopt layer=16 index=1700]
;表情符1
[layopt layer=17 index=1800]
;面妆2
[layopt layer=18 index=1900]
;表情符2
[layopt layer=19 index=2000]
;-------------------------------------
;外套
[layopt layer=20 index=2100]
;饰物1
[layopt layer=21 index=2200]
;前发
[layopt layer=22 index=2300]
;饰物2
[layopt layer=23 index=2400]
;动画底层
[layopt layer=24 index=2500]
;动画层
[layopt layer=25 index=2610]

;-------------------------------------
;标题菜单层
[layopt layer="message0" index=2400]
;当前操作层(当前可操作按钮)
[layopt layer="message1" index=2600]
;基本菜单层
[layopt layer="message2" index=2500]
;右键菜单层
[layopt layer="message3" index=2700]
;档案信息层
[layopt layer="message4" index=2800]
;位置/颜色调节层
[layopt layer="message5" index=2800]
;动画设定调节层
[layopt layer="message6" index=2800]
;动画设定调节层
[layopt layer=26 index=2900]
[layopt layer=27 index=3000]
;------------------------------------------------------------------------------------------
;消息层设定
;------------------------------------------------------------------------------------------
;功能选单(废弃)
;[position layer="message3" visible="false" left=0 top=0 width=124 height=230 color="0x000000" opacity=128 draggable="true" margint=10]
[position layer="message1" visible="false" draggable="true" width=800 height=168 color="0x000000" opacity=128 margint=10 marginl=10 marginr=10 marginb=10]
;基本菜单层
[position layer="message2" visible="false" width=224 height=768 color="0x000000" opacity=128 draggable="true" margint=10]
;档案信息层[角色档案输入层]
[position layer="message4" visible="false" left=&sf.bgdx top=&sf.bgdy width=640 height=480 color="0x000000" opacity=128 draggable="true" marginb=32 marginl=32 marginr=32 margint=32]
;档案信息层[颜色／位置/视图调整层]
[position layer="message5" visible="false" left=&sf.bgdx top=&sf.bgdy width=350 height=480 color="0x000000" opacity=128 draggable="true" marginb=32 marginl=32 marginr=32 margint=32]
;动画调节层
[position layer="message6" visible="false" left=0 top=0 width=1024 height=768 color="0x000000" opacity="128" draggable="false" marginb=32 marginl=32 marginr=32 margint=32]
;------------------------------------------------------------------------------------------
;方向键控制
;------------------------------------------------------------------------------------------
@macro name="纸娃娃操作"
@eval exp="kag.keyDownHook.remove(AnimationKeyDown)"
@eval exp="kag.keyDownHook.add(myOnKeyDown)" cond="kag.keyDownHook.find(myOnKeyDown)==-1"
@endmacro

@macro name="动画层操作"
@eval exp="kag.keyDownHook.remove(myOnKeyDown)"
@eval exp="kag.keyDownHook.add(AnimationKeyDown)" cond="kag.keyDownHook.find(AnimationKeyDown)==-1"
@endmacro

@macro name="禁止方向键"
@eval exp="kag.keyDownHook.remove(myOnKeyDown)"
@eval exp="kag.keyDownHook.remove(AnimationKeyDown)"
@endmacro

;------------------------------------------------------------------------------------------
;层操作
;------------------------------------------------------------------------------------------
[macro name=bg]
[backlay]
[image layer="base" page="back" storage=%storage]
[trans method="crossfade" time=300]
[wt]
[endmacro]

[macro name=fg]
[backlay]
[image layer=%layer|0 page="back" storage=%storage]
[trans method="crossfade" time=300]
[wt]
[endmacro]

[macro name=Layer]
[link storage="%storage" target="%target" exp=&('EditImage('+mp.num+',"'+mp.name+'",),(SaveCloth()),(SaveEmotion())')]
[ch text=&"mp.name"]
[endlink]　　　
[button normal=view storage="%storage" target="%target" exp=&('EditImage('+mp.num+',"'+mp.name+'",),(LoadImage("'+mp.path+'")),(SaveCloth()),(SaveEmotion())')]
[r]
[endmacro]

[macro name=解说]
[er]
[nowait]
[font color="0xFFFF80"]
【没名字的解说】
[font color="0xFFFFFF"]
[indent]
[endnowait]
[endmacro]

[macro name=w]
[endindent]
[l][er]
[endmacro]

[macro name=版面位置]
[if exp="sf.版面选择==1"]
[eval exp="sf.bgdx=224"]
[eval exp="sf.bgdy=168"]
[layopt layer="message1" left=&sf.bgdx top=0]
[layopt layer="message2" left=0 top=0]
[endif]

[if exp="sf.版面选择==2"]
[eval exp="sf.bgdx=0"]
[eval exp="sf.bgdy=168"]
[layopt layer="message1" left=&sf.bgdx  top=0]
[layopt layer="message2" left=800 top=0]
[endif]

[if exp="sf.版面选择==3"]
[eval exp="sf.bgdx=224"]
[eval exp="sf.bgdy=0"]
[layopt layer="message1" left=&sf.bgdx top=600]
[layopt layer="message2" left=0 top=&sf.bgdy]
[endif]

[if exp="sf.版面选择==4"]
[eval exp="sf.bgdx=0"]
[eval exp="sf.bgdy=0"]
[layopt layer="message1" left=&sf.bgdx  top=600]
[layopt layer="message2" left=800 top=&sf.bgdy]
[endif]

[layopt layer=0 left=&sf.bgdx top=&sf.bgdy]
;素体图片初始读入位置
[eval exp="f.bgdx=sf.bgdx"]
[eval exp="f.bgdy=sf.bgdy"]
[eval exp="f.left=sf.bgdx+290"]
[eval exp="f.top=sf.bgdy"]
[endmacro]
;------------------------------------------------------------------------------------------
[return]
