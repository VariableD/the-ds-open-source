*start|
[禁止方向键]
[eval exp="Backcolor.enabled=true"]
;----------------------------------------
[locklink]
[rclick jump="true" enabled="true" storage="color.ks" target=*关闭]

*一般输入|
[禁止方向键]
[backlay]
[layopt layer="message5" visible="true" page="back"]
[current layer="message5" withback="true"]
[er][nowait]
改变颜色[r][r]

辉度最低值：[r]
R　[slider value="f.操作层[f.current].rfloor" min=0 max=255 width=255 height=2][r]
G　[slider value="f.操作层[f.current].gfloor" min=0 max=255 width=255 height=2][r]
B　[slider value="f.操作层[f.current].bfloor"  min=0 max=255 width=255 height=2][r]
辉度最高值：[r]
R　[slider value="f.操作层[f.current].rceil" min=0 max=255 width=255 height=2][r]
G　[slider value="f.操作层[f.current].gceil" min=0 max=255 width=255 height=2][r]
B　[slider value="f.操作层[f.current].bceil"  min=0 max=255 width=255 height=2][r]
;------------------------------------------
;先不连带任何层
[eval exp="f.withcurrent=0"]
[eval exp="f.withname=''"]

;对某些特殊层，将连带层
[if exp="f.currentname=='前发'"]
[eval exp="f.withcurrent=3"]
[eval exp="f.withname='后发'"]
[endif]
[if exp="f.currentname=='后发'"]
[eval exp="f.withcurrent=22"]
[eval exp="f.withname='前发'"]
[endif]

;是否连带层的设定
[if exp="f.withcurrent!=0"]
[link exp="f.withlayer=!f.withlayer" target=*start]同时调整[emb exp="f.withname"]颜色　
[ch text="[是]" cond="f.withlayer==true"]
[ch text="[否]" cond="f.withlayer==false"]
[endlink][r]
[endif]
;------------------------------------------
[r]
[link storage="color.ks" target=*直接输入]直接输入[endlink]　
[link storage="color.ks" target=*复位]复位[endlink]　
[link storage="color.ks" target=*关闭]关闭[endlink]
[endnowait]
[trans method="crossfade" time=50]
[wt]
[s]


*关闭
[eval exp="Backcolor.enabled=false"]
[rclick jump="true" enabled="true" storage="menu.ks" target=*start]
[layopt layer="message5" visible="false"]
[return]

*直接输入|
[禁止方向键]
[backlay]
[layopt layer="message5" visible="true" page="back"]
[current layer="message5" withback="true"]
[er][nowait]
改变颜色[r][r]

辉度最低值：[r]
R　[edit name="f.操作层[f.current].rfloor" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
G　[edit name="f.操作层[f.current].gfloor" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
B　[edit name="f.操作层[f.current].bfloor" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
辉度最高值：[r]
R　[edit name="f.操作层[f.current].rceil" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
G　[edit name="f.操作层[f.current].gceil" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
B　[edit name="f.操作层[f.current].bceil" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160][r]
;------------------------------------------
;先不连带任何层
[eval exp="f.withcurrent=0"]
[eval exp="f.withname=''"]

;对某些特殊层，将连带层
[if exp="f.currentname=='前发'"]
[eval exp="f.withcurrent=3"]
[eval exp="f.withname='后发'"]
[endif]
[if exp="f.currentname=='后发'"]
[eval exp="f.withcurrent=22"]
[eval exp="f.withname='前发'"]
[endif]

;是否连带层的设定
[if exp="f.withcurrent!=0"]
[link exp="f.withlayer=!f.withlayer" target=*start]同时调整[emb exp="f.withname"]颜色　
[ch text="[是]" cond="f.withlayer==true"]
[ch text="[否]" cond="f.withlayer==false"]
[endlink][r]
[endif]
;------------------------------------------
[r]
[link storage="color.ks" target=*一般输入]一般输入[endlink]　
[link storage="color.ks" target=*确认]确认[endlink]　
[link storage="color.ks" target=*关闭]关闭[endlink]
[endnowait]
[trans method="crossfade" time=50]
[wt]
[s]

*确认
[commit]
[eval exp="UpdateColor()"]
[jump storage="color.ks" target=*直接输入]

*复位
[eval exp="f.操作层[f.current].rfloor=0"]
[eval exp="f.操作层[f.current].gfloor=0"]
[eval exp="f.操作层[f.current].bfloor=0"]
[eval exp="f.操作层[f.current].rceil=255"]
[eval exp="f.操作层[f.current].gceil=255"]
[eval exp="f.操作层[f.current].bceil=255"]

[eval exp="UpdateColor()"]
[jump storage="color.ks" target=*一般输入]


