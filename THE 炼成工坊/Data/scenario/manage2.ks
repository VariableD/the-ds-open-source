*start|
[禁止方向键]
;----------------------------------------
[locklink]
[rclick jump="true" enabled="true" storage="manage2.ks" target=*关闭]

[backlay]
[layopt layer="message5" visible="true" page="back"]
[current layer="message5" withback="true"]
[er][nowait]
表情管理[r]
[r]

[eval exp="tf.count=0"]
[endnowait]
*循环
[nowait]
[emb exp="tf.count"][edit name=&"'f.表情名['+tf.count+']'" opacity=0 bgcolor="0x000000" color="0xFFFFFF" length=160]
[if exp="tf.count>0"]
[link exp=&"'tf.删除='+tf.count" target=*删除]删除[endlink]
[endif]

[r]
[eval exp="tf.count++"]
[endnowait]
[jump target=*循环 cond="f.表情[tf.count]!=''"]

[nowait]
[link storage="manage2.ks" target=*新增]新增[endlink]　
[link storage="manage2.ks" target=*更名]更名[endlink]　
[link storage="manage2.ks" target=*关闭]关闭[endlink]
[endnowait]

[trans method="crossfade" time=50]
[wt]
[s]

*新增
[eval exp="AddEmotion();f.表情名.add('未命名')" cond="f.表情.count<=9"]
[jump storage="manage2.ks" target=*start]

*关闭
[rclick call="true" enabled="true" storage="menu.ks" target=*start]
[layopt layer="message5" visible="false"]
[return]

*更名
[commit]
[jump storage="manage2.ks" target=*start]

*删除
[eval exp="f.表情.erase(tf.删除);f.表情名.erase(tf.删除)"]
[eval exp="f.当前表情=0" cond="f.当前表情>f.表情名.count"]
[jump storage="manage2.ks" target=*start]

