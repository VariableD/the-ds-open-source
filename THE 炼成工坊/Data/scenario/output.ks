*start|&f.name
[rclick enabled="false"]
[禁止方向键]
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[eval exp="DrawScale()"]
;-------------------------------------------
[layopt layer="message1" visible="true"]
[current layer="message1" page="fore"]
[er]
[nowait]
[style linespacing=10]
当前输出范围：[r]
left=[emb exp="f.bgdx"]，top=[emb exp="f.bgdy"]，width=[emb exp="f.width"]，height=[emb exp="f.height"][r]
[endnowait]

[layopt layer="message2" visible="true"]
[current layer="message2" page="fore"]
[er]
[style align="center"]
[nowait]
〔预览输出〕[r]
[r]
[r]
[link exp="Loadbgd()"]载入底图[endlink][r]
[link storage="output.ks" target=*清除底图]清除底图[endlink][r]
[r]
[link storage="output.ks" target=*角色档案]角色档案[endlink][r]
[link storage="output.ks" target=*输出范围]输出范围[endlink][r]
[r]
[link storage="output.ks" target=*确认输出]确认输出[endlink][r]
[r][r][r][r][r]
[r][r]
[link storage="output.ks" target=*视图调整]视图调整[endlink][r]
----------
[r]
[link storage="image.ks" target=*start]造型基础[endlink][r]
[link storage="cloth.ks" target=*start]服饰设定[endlink][r]
[link storage="emotion.ks" target=*start]表情编辑[endlink][r]
[endnowait]

[s]

*视图调整
[call storage="option.ks" target="*start"]
[jump storage="output.ks" target="*start"]

*角色档案
[call storage="chara.ks" target="*start"]
[jump storage="output.ks" target="*start"]

*清除底图
[freeimage layer=0]
[jump storage="output.ks" target="*start"]

*输出范围
[call storage="setting.ks" target="*start"]
[jump storage="output.ks" target="*start"]

*确认输出
;-------------------------------------------------------
[eval exp="f.当前服装=0"]

*循环
[eval exp="LoadCloth()"]

[eval exp="f.当前表情=0"]
*内循环
[eval exp="LoadEmotion()"]
[eval exp="UpdateAll()"]
[wait time=100]
[eval exp="Output()"]
[eval exp="f.当前表情++"]
[jump target=*内循环 cond="f.当前表情<f.表情名.count"]

[eval exp="f.当前服装++"]
[jump target=*循环 cond="f.当前服装<f.服装名.count"]
;-------------------------------------------------------
[eval exp="f.当前服装=0"]
[eval exp="f.当前表情=0"]
[jump storage="output.ks" target="*start"]


