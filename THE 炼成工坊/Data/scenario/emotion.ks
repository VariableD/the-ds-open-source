*start
[image layer=base storage="bgd_dot" left=0 top=0 visible="true"]
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[eval exp="DrawScale()"]

[eval exp="f.current=15"]
[eval exp="f.currentname='口'"]
[eval exp="f.path='mouth'"]

;将默认表情数据进行记录
;------------------------------------------------------------------------------------------------------------------------
*刷新画面|&f.name
[rclick enabled="false"]
[纸娃娃操作]
;------------------------------------------------------------------------------------------------------------------------
;载入表情
[freeimage layer=10]
[freeimage layer=11]
[freeimage layer=12]
[freeimage layer=13]
[freeimage layer=14]

[freeimage layer=17]
[freeimage layer=19]

[eval exp="LoadEmotion()"]
[eval exp="UpdateAll()"]
;------------------------------------------------------------------------------------------------------------------------

[layopt layer="message1" visible="true"]
[current layer="message1" page="fore"]
[er]
[style linespacing=10]

[nowait]
当前操作层：[emb exp="f.currentname"][r]
;无图片时的说明
[if exp="f.操作层[f.current].storage==''"]
～请先点选层旁边的"浏览"字样载入图片～
[endif]

;有图片时的显示
[if exp="f.操作层[f.current].storage!=''"]
[link storage="emotion.ks" hint="方向键连续移动／Shift+方向键移动10像素/Alt+方向键移动全部层" target=*调整位置]调整位置[endlink]　
[link storage="emotion.ks" target=*清空图层 hint="点这里可以清空当前层"]清空图层[endlink][r]
[link hint="就是载入的图片文件名"]部件：[endlink]
[emb exp="Storages.extractStorageName(f.操作层[f.current].storage)"][r]
[eval exp="f.x=kag.fore.layers[f.current].left-sf.bgdx"]
[eval exp="f.y=kag.fore.layers[f.current].top-sf.bgdy"]
[link hint="这里的坐标是相对于背景左上点的坐标"]坐标：[endlink](x=[emb exp="f.x"],y=[emb exp="f.y"],index=[emb exp="kag.fore.layers[f.current].absolute"])
[endif]

[endnowait]
;------------------------------------------------------------------------------------------------------------------------
[layopt layer="message2" visible="true"]
[current layer="message2" page="fore"]
[er]
[style align="center"]
[nowait]
〔表情编辑〕[r]
[r][r]
[link exp="(SaveEmotion()),(f.当前表情-- if (f.当前表情>0))" target=*刷新画面]<=[endlink]　
[emb exp="f.当前表情"][emb exp="f.表情名[f.当前表情]"]　
[link exp="(SaveEmotion()),(f.当前表情++ if (f.表情[f.当前表情+1]!=''))" target=*刷新画面]=>[endlink]
[r]
[r][r]
[Layer name="阴影" path="emotion" num=10 target=*刷新画面 storage="emotion.ks"]
[Layer name="眉　" path="braw" num=11 target=*刷新画面 storage="emotion.ks"]
[Layer name="眼　" path="eye" num=12 target=*刷新画面 storage="emotion.ks"]
[Layer name="瞳　" path="eye" num=13 target=*刷新画面 storage="emotion.ks"]
[Layer name="口　" path="mouth" num=15 target=*刷新画面 storage="emotion.ks"]
[r]
[Layer name="附一" path="emotion" num=17 target=*刷新画面 storage="emotion.ks"]
[Layer name="附二" path="emotion" num=19 target=*刷新画面 storage="emotion.ks"]


[r]
[r]
[link storage="emotion.ks" target=*表情管理]表情管理[endlink][r]
[link storage="emotion.ks" target=*全部清空]全部清空[endlink][r]
----------
[r]
[link storage="image.ks" target=*start exp="SaveEmotion()"]造型基础[endlink][r]
[link storage="cloth.ks" target=*start exp="SaveEmotion()"]服饰设定[endlink][r]
[link storage="output.ks" target=*start exp="SaveEmotion()"]预览输出[endlink][r]
[endnowait]
[s]

*调整位置
[call storage="position.ks"]
[eval exp="SaveEmotion()"]
[jump target=*刷新画面]

*清空图层
[eval exp="ClearLayer(f.current)"]
[eval exp="SaveEmotion()"]
[jump target=*刷新画面]


*表情管理
[call storage="manage2.ks"]
[jump target=*刷新画面]

*全部清空
[eval exp="f.确认=askYesNo('确认要清空造型层吗？')"]

[if exp="f.确认==true"]

[freeimage layer=10]
[freeimage layer=11]
[freeimage layer=12]
[freeimage layer=13]
[freeimage layer=15]

[freeimage layer=17]
[freeimage layer=19]

[eval exp="ClearLayer(10)"]
[eval exp="ClearLayer(11)"]

[eval exp="ClearLayer(12)"]
[eval exp="ClearLayer(13)"]
[eval exp="ClearLayer(15)"]

[eval exp="ClearLayer(17)"]
[eval exp="ClearLayer(19)"]

[eval exp="SaveEmotion()"]
[endif]
[jump target=*刷新画面]
