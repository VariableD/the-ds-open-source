*start|&f.name
[image layer=base storage="bgd_dot" left=0 top=0 visible="true"]
[image layer=stage left=0 top=0 storage="bgd_empty" visible="true"]
[eval exp="DrawScale()"]

[eval exp="f.current=4"]
[eval exp="f.currentname='素体'"]
[eval exp="f.path='body'"]

[eval exp="UpdateLayer()"]
;------------------------------------------------------------------------------------------------------------------------
*刷新画面|&f.name
[rclick enabled="false"]
[纸娃娃操作]
;------------------------------------------------------------------------------------------------------------------------
[layopt layer="message1" visible="true"]
[current layer="message1" page="fore"]
[er]
[style linespacing=10]

[nowait]
当前操作层：[emb exp="f.currentname"][r]
;无图片时的说明
[if exp="f.操作层[f.current].storage==''"]
～请先点选层旁边的"浏览"字样载入图片～
[endif]

;有图片时的显示
[if exp="f.操作层[f.current].storage!=''"]
[link storage="image.ks" hint="方向键连续移动／Shift+方向键移动10像素/Alt+方向键移动全部层" target=*调整位置]调整位置[endlink]　
[link storage="image.ks" target=*改变颜色 hint="点这里可以改变部件的颜色"]改变颜色[endlink]　
[link storage="image.ks" target=*清空图层 hint="点这里可以清空当前层"]清空图层[endlink][r]
[link hint="就是载入的图片文件名"]部件：[endlink]
[emb exp="Storages.extractStorageName(f.操作层[f.current].storage)"][r]
[eval exp="f.x=kag.fore.layers[f.current].left-sf.bgdx"]
[eval exp="f.y=kag.fore.layers[f.current].top-sf.bgdy"]
[link hint="这里的坐标是相对于背景左上点的坐标"]坐标：[endlink](x=[emb exp="f.x"],y=[emb exp="f.y"],index=[emb exp="kag.fore.layers[f.current].absolute"])
[endif]

[endnowait]
;------------------------------------------------------------------------------------------------------------------------
[layopt layer="message2" visible="true"]
[current layer="message2" page="fore"]
[er]
[style align="center"]
[nowait]
〔造型基础〕[r]
[r]
[Layer name="后发" path="back" num=3 target=*刷新画面 storage="image.ks"]
[Layer name="素体" path="body" num=4 target=*刷新画面 storage="image.ks"]
[Layer name="附件" path="body" num=5 target=*刷新画面 storage="image.ks"]
[Layer name="脸型" path="face" num=6 target=*刷新画面 storage="image.ks"]
[r]
[Layer name="眉　" path="braw" num=11 target=*刷新画面 storage="image.ks"]
[Layer name="眼　" path="eye" num=12 target=*刷新画面 storage="image.ks"]
[Layer name="瞳　" path="eye" num=13 target=*刷新画面 storage="image.ks"]
[Layer name="鼻　" path="face" num=14 target=*刷新画面 storage="image.ks"]
[Layer name="口　" path="mouth" num=15 target=*刷新画面 storage="image.ks"]
[r]
[Layer name="辅一" path="accesary" num=16 target=*刷新画面 storage="image.ks"]
[Layer name="辅二" path="accesary" num=18 target=*刷新画面 storage="image.ks"]
[Layer name="前发" path="front" num=22 target=*刷新画面 storage="image.ks"]
[r]
;------------------------------------------------------------------------------------------------------------------------
[link storage="image.ks" target=*全部清空]全部清空[endlink][r]
----------
[r]
[link storage="cloth.ks" target=*start]服饰设定[endlink][r]
[link storage="emotion.ks" target=*start]表情编辑[endlink][r]
[link storage="output.ks" target=*start]预览输出[endlink][r]

[endnowait]
[s]
;--------------------------------------------------
*调整位置
[call storage="position.ks"]
[jump target=*刷新画面]
;--------------------------------------------------
*改变颜色
[call storage="color.ks"]
[jump target=*刷新画面]
;--------------------------------------------------
*清空图层
[freeimage layer=&f.current]
[eval exp="ClearLayer(f.current)"]
[jump target=*刷新画面]
;--------------------------------------------------
*全部清空
[eval exp="f.确认=askYesNo('确认要清空造型层吗？')"]

[if exp="f.确认==true"]
[freeimage layer=3]
[freeimage layer=4]
[freeimage layer=5]
[freeimage layer=6]

[freeimage layer=11]
[freeimage layer=12]
[freeimage layer=13]
[freeimage layer=14]
[freeimage layer=15]

[freeimage layer=16]
[freeimage layer=18]
[freeimage layer=22]

[eval exp="ClearLayer(3)"]
[eval exp="ClearLayer(4)"]
[eval exp="ClearLayer(5)"]
[eval exp="ClearLayer(6)"]

[eval exp="ClearLayer(11)"]
[eval exp="ClearLayer(12)"]
[eval exp="ClearLayer(13)"]
[eval exp="ClearLayer(14)"]
[eval exp="ClearLayer(15)"]

[eval exp="ClearLayer(16)"]
[eval exp="ClearLayer(18)"]
[eval exp="ClearLayer(22)"]
[endif]


[jump target=*刷新画面]
