[iscript]
//--------------------------------------------------------------------------
//游戏的主角类
//--------------------------------------------------------------------------
class Hero extends KAGLayer
{
var face;//朝向
var x;//位置X
var y;//位置Y
var step=0;//动作
var cd=new Timer(OnTimer,'');
//-----------------------------------------------------------
//创建函数
//-----------------------------------------------------------
    function Hero(char="hero",posX=0,posY=0,face=0,interval=80,win=kag,par=kag.fore.base)
    {
       super.KAGLayer(win, par);
       x=posX;
	   y=posY;
	   loadImages(char);
	   visible=true;
	   //层大小为角色大小
	   setSize(imageWidth/4,imageHeight/4);
	   //显示角色
	   Update();
	   //定时刷新
	   cd.interval=interval;
       cd.enabled=true;
    }
//-----------------------------------------------------------
//删除函数
//-----------------------------------------------------------
    function finalize()
    {
      	super.finalize(...);
    }
//-----------------------------------------------------------
//停止角色
//-----------------------------------------------------------
    function freeze()
    {
      	cd.enabled=false;
    }
//-----------------------------------------------------------
//启动角色
//-----------------------------------------------------------
    function control()
    {
      	cd.enabled=true;
    }
//-----------------------------------------------------------
//刷新角色
//-----------------------------------------------------------
    function Update()
    {
    //根据朝向显示角色图
	setImagePos(-imageWidth*step/4, -imageHeight*face/4);
	left=x*5-width/2;
	top=y*5-height*2/3;
    }

//-----------------------------------------------------------
//定时调用
//-----------------------------------------------------------
    function OnTimer()
    {
    //有键按下时的情况
     //---------------鼠标--------------
     if (kag.getKeyState(VK_LBUTTON))
     {
     
       //向左
       if (kag.fore.base.cursorX<left) 
       {
         if (kag.fore.base.cursorY<top) MoveLeftUp();
         else if (kag.fore.base.cursorY>top+height) MoveLeftDown();
         else MoveLeft();
       }
       //向右
       else if (kag.fore.base.cursorX>left+width)
       {
         if (kag.fore.base.cursorY<top) MoveRightUp();
         else if (kag.fore.base.cursorY>top+height) MoveRightDown();
         else MoveRight();
       }
       //纵向
       else if (kag.fore.base.cursorY<top) MoveUp();
       else if (kag.fore.base.cursorY>top+height) MoveDown();
       //到达鼠标位置
       else
       {
         if (step==3) step=0;
         if (step==1) step=2;
        }
     }
     //----------------下---------------
     else if (kag.getKeyState(VK_DOWN) && kag.getKeyState(VK_RIGHT))
     {
       MoveRightDown();
     }
     else if (kag.getKeyState(VK_DOWN) && kag.getKeyState(VK_LEFT))
     {
       MoveLeftDown();
     }
     else if (kag.getKeyState(VK_DOWN)) 
     {
       MoveDown();
     }
     //----------------上------------------------
     else if (kag.getKeyState(VK_UP) && kag.getKeyState(VK_RIGHT))
     {
       MoveRightUp();
     }
     else if (kag.getKeyState(VK_UP) && kag.getKeyState(VK_LEFT))
     {
       MoveLeftUp();
     }
     else if (kag.getKeyState(VK_UP)) 
     {
       MoveUp();
     }
     //----------------左---------------
     else if (kag.getKeyState(VK_LEFT)) 
     {
       MoveLeft();
     }
     //----------------右---------------
     else if (kag.getKeyState(VK_RIGHT)) 
     {
       MoveRight();
     }
     //----------------按键抬起时----------------
     else
      {
         if (step==3) step=0;
         if (step==1) step=2;
       }
     Update();
    }
//-----------------------------------------------------------
//移动相关
//-----------------------------------------------------------
    function MoveStop()
    {
     step=0;
     Update();
    }
    function Face(direction)
    {
     face=direction;
     step=0;
     Update();
    }
    function MoveBack()
    {
      if (face==0) {face=3;MoveUp();}
      else if (face==1) {face=2;MoveRight();}
      else if (face==2) {face=1;MoveLeft();}
      else {face=0;MoveDown();}
     Update();
    }
    function MoveOn()
    {
     if (face==0) MoveDown();
     if (face==1) MoveLeft();
     if (face==2) MoveRight();
     if (face==3) MoveUp();
     Update();
    }
    function MoveUp()
    {
      if (face!=3) 
      {face=3;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y>1) y--;
    }
    function MoveDown()
    {
      if (face!=0)
      {face=0;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y<119) y++;
    }
    function MoveLeft()
    {
      if (face!=1)
      {face=1;step=0;}
      else if (step<3) step++;
      else step=0;
      if (x>1) x--;
    }
    function MoveRight()
    {
      if (face!=2)
      {face=2;step=0;}
      else if (step<3) step++;
      else step=0;
      if (x<159) x++;
    }
    function MoveLeftUp()
    {
      if (face!=3) 
      {face=3;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y>1) y--;
      if (x>1) x--;
    }
    function MoveLeftDown()
    {
      if (face!=0) 
      {face=0;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y<119) y++;
      if (x>1) x--;
    }
    function MoveRightUp()
    {
      if (face!=3) 
      {face=3;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y>1) y--;
      if (x<159) x++;
    }
    function MoveRightDown()
    {
      if (face!=0) 
      {face=0;step=0;}
      else if (step<3) step++;
      else step=0;
      if (y<119) y++;
      if (x<159) x++;
    }
//-----------------------------------------------------------   
}

function OnTimer()
{
   if (f.角色.x>=13 && f.角色.x<=15 && f.角色.y>=13 && f.角色.y<=15)
   {
     test.enabled=false;
     kag.process("first.ks","*测试");
    }
}

var test=new Timer(OnTimer,'');
test.enabled=true;
test.interval=10;

f.角色=new Hero(,10,10,3);
[endscript]

[image layer=0 storage="item" visible="true" left=70 top=70]

*等待
[eval exp="f.角色.control()"]
[s]

*测试
[eval exp="f.角色.freeze()"]
[layopt layer="message0" visible="true"]
你踩到了奇怪的东西[p]
[wait time=200]
[eval exp="f.角色.MoveBack()"]
[wait time=120]
[eval exp="f.角色.MoveOn()"]
[wait time=120]
[eval exp="f.角色.MoveOn()"]
[wait time=120]
[eval exp="f.角色.MoveStop()"]
[eval exp="test.enabled=true"]
[wait time=500]
[jump target=*等待]
