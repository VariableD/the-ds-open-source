;描绘脚本实验

*start
;------------------------------------------------------------
;层数量
;------------------------------------------------------------
[laycount layers=20]
[laycount messages=8]
;------------------------------------------------------------
;层顺位
;------------------------------------------------------------
;背景层
[layopt layer=stage index=100 withback=true]
;立绘
[layopt layer="0" index=200 withback=true]
[layopt layer="1" index=300 withback=true]
[layopt layer="2" index=400 withback=true]
[layopt layer="3" index=500 withback=true]
[layopt layer="4" index=600 withback=true]
[layopt layer="5" index=700 withback=true]
;动态
[layopt layer="6" index=800 withback=true]
[layopt layer="7" index=900 withback=true]
;----------------------------------------------
;名称栏
[layopt layer="event" index=1000 withback=true]
;对话框
[layopt layer="message0" index=1100 withback=true]
;头像层
[layopt layer="8" index=1200 withback=true]
;选择支/地图
[layopt layer="message1" index=1300 withback=true]
;豆辞典
[layopt layer="9" index=1400 withback=true]
;预留
[layopt layer="10" index=1500 withback=true]
;预留
[layopt layer="11" index=1600 withback=true]
;----------------------------------------------
;系统按钮(sysbutton)
[layopt layer="message2" index=2000 withback=true]
;----------------------------------------------
;菜单层1
[layopt layer="message3" index=3100 withback=true]
;预留
[layopt layer="12" index=3200 withback=true]
;预留
[layopt layer="13" index=3300 withback=true]
;----------------------------------------------
;系统背景
;----------------------------------------------
[layopt layer="14" index=3400 withback=true]
;菜单层2
[layopt layer="message4" index=3500 withback=true]
[position layer="message4" width=800 height=600 frame="" color="0xFFFFFF" opacity=0]
;预留
[layopt layer="15" index=3600 withback=true]
;预留
[layopt layer="16" index=3700 withback=true]
;预留
[layopt layer="17" index=3800 withback=true]
;----------------------------------------------
;编辑器用
;----------------------------------------------
;菜单层3
[layopt layer="message5" index=3900 withback=true]
;预留
[layopt layer="18" index=4000 withback=true]
;菜单层4
[layopt layer="message6" index=4100 withback=true]
;滚动条
[layopt layer="19" index=4200 withback=true]
;菜单层5
[layopt layer="message7" index=4300 withback=true]

;------------------------------------------------------------------
;滚动条翻页效果
;------------------------------------------------------------------
[iscript]
//滚动条拉动的情况
function script_page()
{
   if (kag.fore.base.cursorY<kag.fore.layers[9].top && tf.当前脚本页>1)
     script_up();
   if (kag.fore.base.cursorY-56>kag.fore.layers[9].top && tf.当前脚本页<tf.脚本页数)
    script_down();
}
//向上翻页的情况
function script_up()
{
  if (tf.当前脚本页>1) 
  {
    tf.当前脚本页--;
    tf.当前编辑行=1;
    //刷新画面
    showscript();

  }
}
//向下翻页的情况
function script_down()
{
  if (tf.当前脚本页<tf.脚本页数) 
  {
    tf.当前脚本页++;
    tf.当前编辑行=1;
    //刷新画面
    showscript();
  }
}
[endscript]
;------------------------------------------------------------------
;读入脚本文件
;------------------------------------------------------------------
[iscript]
function loadscript(file)
{
var LoadScript=[];
LoadScript.load(file);
return LoadScript;
}
[endscript]
;------------------------------------------------------------------
;指令类型识别
;------------------------------------------------------------------
[iscript]
function gettype(line)
{
//空行
 if (line=='')
 {
   return "空行";
 }
//非空
 else
 {
    var type;
       switch (line.charAt(0))
    {
       case "[":
       case "@":
            type="指令";
            break;
       case "*":
            type="标签";
            break;
       case ";":
            type="注释";
            break;
       default:
            type="对话";
            break;
    }
   return type;
 }
 
}
[endscript]

;------------------------------------------------------------------
;描绘脚本
;------------------------------------------------------------------
[iscript]
function showscript()
{
   kag.fore.layers[9].left=763;
   kag.fore.layers[9].top=79;   
   if (tf.脚本页数>1) kag.fore.layers[9].top=(tf.当前脚本页-1)*313/(tf.脚本页数-1)+79*1;
//开辟描绘区域
with(kag.fore.layers[0])
{
.fillRect(0,0,800,600,0xFFACC4C0);
.fillRect(0,0,800,40,0xFF67768D);
.fillRect(370,55,25,430,0xFFFFFFFF);
.fillRect(400,55,360,430,0xFFFFFFFF);

 for (var i=0;i<25;i++)
  {
   if (i+(tf.当前脚本页-1)*25>f.脚本.count-1) break;
   var line=f.脚本[i+(tf.当前脚本页-1)*25];
    .drawText(372,58+17*i,i+1+(tf.当前脚本页-1)*25,0xC8C8C8);
    .drawText(405,58+17*i,line.line,line.color);
  }
}
}
[endscript]
;------------------------------------------------------------------
;开始执行
;------------------------------------------------------------------
*start
[eval exp="tf.当前脚本页=1"]

[iscript]

//载入脚本
var script=loadscript("test.ks");
//计算页数
  tf.脚本页数=(script.count)\25;
  if ((script.count)%25>0) tf.脚本页数++;
//分析脚本
f.脚本=[];
for (var i=0;i<script.count;i++)
{
  f.脚本[i]=new Dictionary();
  //原行内容(去头尾空白)
  f.脚本[i].line=script[i].trim();
  //类型
  f.脚本[i].type=gettype(f.脚本[i].line);
  //为tag,分析
  
  //颜色
  f.脚本[i].color=0x000000;
}
//描绘
with(kag.fore.layers[0])
{
.font.height=15;
.visible=true;
.width=800;
.height=600;
}
//.fillRect(0,0,800,600,0xFFACC4C0);
showscript();
[endscript]
[image layer=9 storage="edit_slider_button" visible="true"]
[current layer="message0" page="fore"]
[er]
[locate x=763 y=79]
[button normal="edit_slider" ontimer="script_page()" interval=1]
[locate x=763 y=47]
[button normal="edit_slider_up" exp="script_up()" interval=150 ontimer="script_up()"]
[locate x=763 y=447]
[button normal="edit_slider_down"exp="script_down()" interval=150 ontimer="script_down()"]
[s]
