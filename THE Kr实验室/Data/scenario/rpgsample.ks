*start
[iscript]
//---------------------------------------------------------------
//角色类-通用
//---------------------------------------------------------------
class Character extends KAGLayer
{
   //定义变量
   
     var name;//角色的名字(ID)
     var char="001-Fighter01";//图像：使用的角色图     
     var face;//朝向：面对的方向
     var step;//帧数：行走动画当前的帧数
     
     var count_step=new Timer(OnStep,"");//计时器：播放角色动画
     var check_state=new Timer(OnCheck,"");//计时器：监视角色状态
     
     var camera=false;//视角跟随：当值为真时,大于屏幕的场景跟随角色卷动
     
     var moving=false;//动作：播放行走动画
     var passable=false;//可穿透：当值为真时，除了地图边缘外，不进行碰撞检测
     
     var onedge=[0,0,0,0];
     var onblock=false;
     var onnpc=-1;
   
   //定义属性
     //中心点X
      property x
   {
       setter(a)
       {
          left=a-width/2;
       }
        getter ()
        {
          return left+width/2;
        }
   }
     //中心点Y
   property y
   {
       setter(a)
       {
          top=a-height/2;
       }
        getter ()
        {
          return top+height/2;
        }
   }
     //绝对深度Z
   property z
   {
       setter(a)
       {
          absolute=a;
       }
        getter ()
        {
          return absolute;
        }
   }
   //创建函数
   function Character(char,x=400,y=300,rate=110,speed=10)
   {
     //创建层
      super.KAGLayer(kag, kag.fore.base);
     //载入图片
      if (char!='') this.char=char;
      loadImages(this.char);
      visible=true;
	 //层大小为角色大小
	  setSize(imageWidth/4,imageHeight/4);
	 //移动角色位置
	  this.x=x;
	  this.y=y;
	  this.z=y;
	 //开始计步和监视状态
	  count_step.interval=rate;
	  count_step.enabled=true;
	  check_state.interval=speed;
	  check_state.enabled=true;
   }
   //销毁函数
    function finalize()
    {
        invalidate count_step;
        invalidate check_state;
      	super.finalize(...);
    }
   //刷新行走动画
        function OnStep()
    {
       //一般移动
       if (moving)
       {
         if (step<3) step++;
         else step=0;
        }
       else if (step==3) step=0;
       else if (step==1) step=2;
       //根据朝向显示角色图
	   setImagePos(-imageWidth*step/4, -imageHeight*face/4);
    }
     
    //监视并改变角色状态
        function OnCheck()
     {
       onnpc=-1;
       //开始判断角色状态
       MapEdge();
       MapBlock();
       if (f.地图.npc!=void) CharBlock();
       //角色移动
       Move();
     }
     
       //移动到了地图边缘
        function MapEdge()
     {
       onedge=[0,0,0,0];
       if (f.地图.height==-f.地图.top+top+height+2) onedge[0]=true;
       if (left==f.地图.left+2) onedge[1]=true;
       if (f.地图.width==-f.地图.left+left+width+2) onedge[2]=true;
       if (top==f.地图.top+2) onedge[3]=true;
     }
       //与地图障碍物接触
        function MapBlock()
     {
        onblock=[0,0,0,0];
       //不可向下
       if (f.地图.getMainPixel(left-f.地图.left,top-f.地图.top+height+1)==0x000000) onblock[0]=true;
       if (f.地图.getMainPixel(left-f.地图.left+width,top-f.地图.top+height+1)==0x000000) onblock[0]=true;
       //不可向左
       if (f.地图.getMainPixel(left-f.地图.left-1,top-f.地图.top)==0x000000) onblock[1]=true;
       if (f.地图.getMainPixel(left-f.地图.left-1,top+height-f.地图.top)==0x000000) onblock[1]=true;
       //不可向右
       if (f.地图.getMainPixel(left-f.地图.left+1+width,top-f.地图.top)==0x000000) onblock[2]=true;
       if (f.地图.getMainPixel(left-f.地图.left+1+width,top+height-f.地图.top)==0x000000) onblock[2]=true;
       //不可向上
       if (f.地图.getMainPixel(left-f.地图.left,top-f.地图.top-1)==0x000000) onblock[3]=true;
       if (f.地图.getMainPixel(left-f.地图.left+width,top-f.地图.top-1)==0x000000) onblock[3]=true;
     }
       //与除自己以外的其他不可穿透角色接触：面前一格的范围内有NPC（判断NPC中心点坐标在范围内）
        function CharBlock()
     {
       var scale=[];
       if (face==0) scale=[left,left+width,top+height/2,top+height*3/2];
       else if (face==1) scale=[left-width/2,left+width/2,top+height/4,top+height*3/4];
       else if (face==2) scale=[left+width/2,left+width*3/2,top+height/4,top+height*3/4];
       else  scale=[left,left+width,top,top+height/2];
       //与主角碰撞
       if (f.地图.hero!=this)
       {
          if (f.地图.hero.x>scale[0] && f.地图.hero.x<scale[1] && f.地图.hero.y>scale[2] && f.地图.hero.y<scale[3])
          if (face==0) onblock[0]=true;
          else if (face==1) onblock[1]=true;
          else if (face==2) onblock[2]=true;
          else  onblock[3]=true;
        }
       //比较NPC
       for (var i=0;i<f.地图.npc.count;i++)
       {
        //对方为可穿透
          if (f.地图.npc[i].passable==true) continue;
         //不比较自己
          if (f.地图.npc[i]==this) continue;
          //面前有NPC
          if (f.地图.npc[i].x>scale[0] && f.地图.npc[i].x<scale[1] && f.地图.npc[i].y>scale[2] && f.地图.npc[i].y<scale[3])
          onnpc=i;
       }
     }
     //移动角色
        function Move()
     {
       if (moving && onnpc==-1)
       {
           if (face==0 && onedge[0]==false && onblock[0]==false) MoveDown();
           if (face==1 && onedge[1]==false && onblock[1]==false) MoveLeft();
           if (face==2 && onedge[2]==false && onblock[2]==false) MoveRight();
           if (face==3 && onedge[3]==false && onblock[3]==false) MoveUp();
       }
     }
       //向下移动
        function MoveDown()
     {
       if (camera && f.地图.top+f.地图.height>600 && y>=300) f.地图.scroll(0);
       else {y++;z=y;}
     }
       //向左移动
        function MoveLeft()
     {
      if (camera && f.地图.left<0 && x<400) f.地图.scroll(1);
       else x--;
     }
       //向右移动
        function MoveRight()
     {
      if (camera && f.地图.left+f.地图.width>800 && x>=400) f.地图.scroll(2);
       else x++;
     }
       //向上移动
        function MoveUp()
     {
      if (camera && f.地图.top<0 && y<300) f.地图.scroll(3);
       else {y--;z=y;}
     }
       //斜向移动
     
   //游戏演出
   
     //改变朝向
     
     //角色行走
     
     //角色跳跃
     
     //切换角色行走图
     function ChangeChar(char)
     {
         loadImages(char);
          visible=true;
	 //层大小为角色大小
	  setSize(imageWidth/4,imageHeight/4);
     }
 }
//---------------------------------------------------------------
//角色类-主角
//---------------------------------------------------------------
class Hero extends Character
{
   //定义变量
   
   //定义属性
   
   //创建函数
   function Hero()
   {
     super.Character(...);
     camera=true;
   }
   //销毁函数


}
//---------------------------------------------------------------
//角色类-NPC
//---------------------------------------------------------------
class Npc extends Character
{
   //定义变量
     var random_move=new Timer(OnAction,"");//计时器：监视角色状态
   //定义属性
   
   //创建函数
   function Npc()
   {
    super.Character(...);
    if(typeof kag.cursorPointed !== "undefined")
     cursor = kag.cursorPointed;
     check_state.interval=30;
     count_step.interval=150;
     random_move.interval=3000;
     random_move.enabled=true;
   }
   //销毁函数
   
   //行动AI（随机/固定路线/跟随主角/逃离主角）
     function OnAction()
   {
      face=intrandom(0,3);
   }
   //事件脚本
   function talk()
   {
     //
     moving=false;
     f.对话中=f.地图.hero.onnpc;
     f.地图.hero.onnpc=-1;
     //执行脚本
     kag.process("","*测试");
   }
}
//---------------------------------------------------------------
//角色类-事件点
//---------------------------------------------------------------
class Event extends Character
{
   //定义变量
   
   //定义属性
   
   //创建函数
   
   //销毁函数
   
   //事件脚本
   
}
//---------------------------------------------------------------
//角色类-宝箱
//---------------------------------------------------------------
class Chest extends Character
{
   //定义变量
   
   //定义属性
   
   //创建函数
   
   //销毁函数
   
   //事件脚本
   
}
//---------------------------------------------------------------
//地图类
//---------------------------------------------------------------
class RPGMap extends KAGLayer
{
   //定义变量
   var frame;
   var hero;
   var npc;
   //定义属性
   
   //创建函数
    function RPGMap(map="field",x=400,y=300)
    {
     //创建层
     super.KAGLayer(kag, kag.fore.base);
     absolute=0;
     hitType = htMask;
     hitThreshold = 256;
     //读入层图片
     loadImages(map);
     setSizeToImageSize();
     visible=true;
      //创建主角
     hero=new Hero();
     hero.x=x;
     hero.y=y;
     //创建NPC
     npc=[];
     npc[0]=new Npc();
     //创建监视器
     frame=new Timer(check,"");
     frame.interval=5;
     frame.enabled=true;
    }
   //销毁函数
    function finalize()
    {
      	super.finalize(...);
    }
    
   //监视
   function check()
   {
      //取得按键情况
        var vk_lbutton = kag.getKeyState(VK_LBUTTON);
        var vk_left = kag.getKeyState(VK_LEFT);
        var vk_up = kag.getKeyState(VK_UP);
        var vk_right = kag.getKeyState(VK_RIGHT);
        var vk_down = kag.getKeyState(VK_DOWN);
        var vk_space = kag.getKeyState(VK_SPACE);
        var vk_return= kag.getKeyState(VK_RETURN);
       //主角移动到事件点上
       
       //对话：ENTER键按下+主角面前一格的范围内有NPC（判断NPC中心点坐标在范围内）
        if (vk_return && hero.onnpc!=-1) 
        {
          hero.moving=false;
          frame.enabled=false;
          npc[hero.onnpc].talk();
        }
        else if (vk_space && hero.onnpc!=-1) 
        {
          hero.moving=false;
          frame.enabled=false;
          npc[hero.onnpc].talk();
        }
             //鼠标按下
             else if (vk_lbutton) MouseControl();
            //方向键按下
      	else if (vk_left) {hero.face=1;hero.moving=true;}
      	else if (vk_up) {hero.face=3;hero.moving=true;}
      	else if (vk_right) {hero.face=2;hero.moving=true;}
      	else if (vk_down) {hero.face=0;hero.moving=true;}
      	else hero.moving=false;

    }
   //鼠标左键按下
   function MouseControl()
   {
     //接触中
     if (hero.onnpc!=-1) 
     {
          var x1=npc[hero.onnpc].left;
          var x2=npc[hero.onnpc].left+npc[hero.onnpc].width;
          var y1=npc[hero.onnpc].top;
          var y2=npc[hero.onnpc].top+npc[hero.onnpc].height;   
        if (kag.fore.base.cursorX>x1 && kag.fore.base.cursorX<x2 && kag.fore.base.cursorY>y1 && kag.fore.base.cursorY<y2)
         {
           hero.moving=false;
           frame.enabled=false;
           npc[hero.onnpc].talk();
           return;
          }
      }
     //一般移动
        hero.moving=true;
       //一象限
        if (kag.fore.base.cursorX>hero.x && kag.fore.base.cursorY<hero.y)
        {
          if (kag.fore.base.cursorX-hero.x<hero.y-kag.fore.base.cursorY)
          hero.face=3;
          else
          hero.face=2;
        }
       //二象限
        else if (kag.fore.base.cursorX>hero.x && kag.fore.base.cursorY>hero.y)
        {
          if (kag.fore.base.cursorX-hero.x>kag.fore.base.cursorY-hero.y)
          hero.face=2;
          else
          hero.face=0;
        }
       //三象限
        else if (kag.fore.base.cursorX<hero.x && kag.fore.base.cursorY>hero.y)
        {
          if (hero.x-kag.fore.base.cursorX<kag.fore.base.cursorY-hero.y)
          hero.face=0;
          else
          hero.face=1;
        }
       //四象限
        else if (kag.fore.base.cursorX<hero.x && kag.fore.base.cursorY<hero.y)
        {
          if (hero.x-kag.fore.base.cursorX<hero.y-kag.fore.base.cursorY)
          hero.face=3;
          else
          hero.face=1;
        }
        else hero.moving=false;
   }
   //地图卷动
   function scroll(dir)
   {
     if (dir==0)
     {
        top--;
        if (npc!=void)
        {
           for (var i=0;i<npc.count;i++)
          {
           npc[i].y--;
           npc[i].z=npc[i].y;
          }
        }
     }
     if (dir==1)
     {
        left++;
           if (npc!=void)
        {
            for (var i=0;i<npc.count;i++)
         {
          npc[i].x++;
         }
        }
     }
     if (dir==2)
     {
        left--;
           if (npc!=void)
        {
            for (var i=0;i<npc.count;i++)
         {
          npc[i].x--;
         }
        }
     }
     if (dir==3)
     {
        top++;
           if (npc!=void)
        {
           for (var i=0;i<npc.count;i++)
          {
              npc[i].y++;
              npc[i].z=npc[i].y;
        }
        }
     }
   }
    
}
f.地图=new RPGMap();
[endscript]
[s]

*测试
[eval exp="f.地图.frame.enabled=false"]
[eval exp="f.地图.npc[f.对话中].face=3-f.地图.hero.face"]
你不觉得这世界很囧吗[p]
[eval exp="f.地图.npc[f.对话中].moving=true"]

*等待
[wait time=500]
[eval exp="f.地图.frame.enabled=true"]
[s]
