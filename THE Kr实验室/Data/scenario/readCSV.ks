
Plugins.link("win32ole.dll");

Debug.console.visible = Debug.controller.visible = true;

// DBEngineオブジェクトを獲得
var objDBE = new WIN32OLE("DAO.DBEngine.36");

// Workspaceオブジェクトを獲得
var objWS = objDBE.Workspaces(0);

// Databaseオブジェクトを獲得
var objDB = objWS.OpenDatabase("E:\\实验用工程", false, false, "Text");

// リスト.csvから男性のデータのみ抽出し、年齢順にソート
var objRS = objDB.OpenRecordset("SELECT * FROM [リスト.csv] WHERE 性別=\'男\' ORDER BY 年齢");

// 結果をコンソールに出力
while (!objRS.EOF) {
  Debug.message("氏名=%s, 年齢=%s".sprintf(objRS("氏名").Value, objRS("年齢").Value));
  objRS.MoveNext();
}

// 後始末
objRS.Close();
objDB.Close();
invalidate objRS;
invalidate objDB;
invalidate objWS;
invalidate objDBE;
