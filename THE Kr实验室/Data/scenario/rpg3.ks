[iscript]
//----------------------------------------------------------------------------
//地图层
//----------------------------------------------------------------------------
class MapLayer extends KAGLayer
{
//读入地图层
  function MapLayer(storage)
  {
     //创建层
     super.KAGLayer(kag, kag.fore.base);
     //读入层图片
     loadImages(storage);
     setSizeToImageSize();
  }
//销毁
    function finalize()
  {
      super.finalize(...);
  }
}
//----------------------------------------------------------------------------
//地图
//----------------------------------------------------------------------------
class Map extends KAGLayer
{
   //地图层,主角,npc
   var mapinfo;
   var hero;
   var frame;
   var npc=[];
//-----------------------------------------------------------
//创建函数
    function Map()
    {
     //创建层
     super.KAGLayer(kag, kag.fore.base);
     //读入层图片
     loadImages("black");
     setSizeToImageSize();
     visible=true;
     //地图底层
    // mapinfo=new MapLayer("bg1_1");
      //创建主角
      hero=new Hero();
     //创建监视器
     frame=new Timer(check,"");
     frame.interval=10;
     frame.enabled=true;
    }
//-----------------------------------------------------------
//删除函数
    function finalize()
    {
      	super.finalize(...);
    }
//-----------------------------------------------------------
//按键检测
    function check()
    {
      	//取得当前按下的键
      	var vk_lbutton = kag.getKeyState(VK_LBUTTON);
      	var vk_left = kag.getKeyState(VK_LEFT);
      	var vk_up = kag.getKeyState(VK_UP);
      	var vk_right = kag.getKeyState(VK_RIGHT);
      	var vk_down = kag.getKeyState(VK_DOWN);
      	if (vk_lbutton) MouseControl();
      	else if (vk_left) {hero.face=2;hero.move=true;}
      	else if (vk_up) {hero.face=3;hero.move=true;}
      	else if (vk_right) {hero.face=1;hero.move=true;}
      	else if (vk_down) {hero.face=0;hero.move=true;}
      	else hero.move=false;
    }
//鼠标按键
    function MouseControl()
    {
      hero.move=true;
      if (kag.fore.base.cursorX<hero.x && kag.fore.base.cursorY<hero.y) hero.face=2;
      else if (kag.fore.base.cursorX>hero.x && kag.fore.base.cursorY<hero.y) hero.face=3;
      else if (kag.fore.base.cursorX>hero.x && kag.fore.base.cursorY>hero.y) hero.face=1;
      else if (kag.fore.base.cursorX<hero.x && kag.fore.base.cursorY>hero.y) hero.face=0;
      else hero.move=false;
    }
//-----------------------------------------------------------
//碰撞检测:地图边缘
    function EdgeCrash(disX,disY)
    {
     //对x,y方向进行判断
     var able=%[];
     able[x]=true;
     able[y]=true;
     if (left+disX==0) able[x]=false;
     else if (left+width+disX==799) able[x]=false;
     
     if (top+disY==0) able[y]=false;
     else if (top+height+disY==599) able[y]=false;
     
     return able;
    }
//-----------------------------------------------------------
//碰撞检测:地图障碍
    function MoveLeft()
    {
      if (f.地图.getMainPixel(left-f.地图.left-1,top-f.地图.top)==0x9BB3C7) return;
      else
      if (f.地图.getMainPixel(left-f.地图.left-1,top+height-f.地图.top)==0x9BB3C7) return;
      else 
      if (f.地图.left<0 && x<400) f.地图.left++;
      else left--;
    }
    function MoveRight()
    {
      if (f.地图.getMainPixel(left-f.地图.left+1+width,top-f.地图.top)==0x9BB3C7) return;
      else
      if (f.地图.getMainPixel(left-f.地图.left+1+width,top+height-f.地图.top)==0x9BB3C7) return;
      else 
      if (f.地图.left+f.地图.width>800 && x>=400) f.地图.left--;
      else left++;
    }
    function MoveUp()
    {
      if (f.地图.getMainPixel(left-f.地图.left,top-f.地图.top-1)==0x9BB3C7) return;
      else
      if (f.地图.getMainPixel(left-f.地图.left+width,top-f.地图.top-1)==0x9BB3C7) return;
      else
      if (f.地图.top<0 && y<300) f.地图.top++;
      else top--;
    }
    function MoveDown()
    {
      if (f.地图.getMainPixel(left-f.地图.left,top-f.地图.top+1)==0x9BB3C7) return;
      else
      if (f.地图.getMainPixel(left-f.地图.left+width,top-f.地图.top+1)==0x9BB3C7) return;
      else
      if (f.地图.top+f.地图.height>600 && y>=300) f.地图.top--;
      else top++;
    }
//-----------------------------------------------------------
//碰撞检测:NPC

}
//----------------------------------------------------------------------------
//主角层
//----------------------------------------------------------------------------
class Hero extends KAGLayer
{
//--------------------变量定义--------------------
   var CountStep=new Timer(OnStep,"");//播放行走动画
   var CountMove=new Timer(OnMove,"");//人物位移
   var move=false;//移动状态
   var face;//当前朝向
   var step;//当前帧
   var play=false;//演出状态
   var stepnum;//计算前进步数
   var direction;//角色移动方向
   
//--------------------取得中点坐标值--------------------
      property x
   {
       setter(a)
       {
          x=a;
       }
        getter ()
        {
          return left+width/2;
        }
   }
   property y
   {
       setter(a)
       {
          y=a;
       }
        getter ()
        {
          return top+height/2;
        }
   }
//--------------------创建函数--------------------
   function Hero(char="hero",face=0,rate=105,speed=10)
   {
     //创建层
      super.KAGLayer(kag, kag.fore.base);
     //载入图片
      loadImages(char);
      visible=true;
	 //层大小为角色大小
	 setSize(imageWidth/4,imageHeight/4);
	 CountStep.interval=rate;
	 CountStep.enabled=true;
	 CountMove.interval=speed;
	 CountMove.enabled=true;
	 left=400;
	 top=300;
   }
//--------------------删除函数--------------------
    function finalize()
    {
        invalidate CountStep;
      	super.finalize(...);
    }
//--------------------播放行走动画--------------------
        function OnStep()
    {
       //演出中
       if (play==true && stepnum>0) {stepnum--;}
       if (play==true && stepnum==0) {play=false;move=false;direction=0;}
       //一般移动
       if (move) 
       {
         if (step<3) step++;
         else step=0;
        }
       else if (step==3) step=0;
       else if (step==1) step=2;
       //根据朝向显示角色图
	   setImagePos(-imageWidth*step/4, -imageHeight*face/4);
    }
//--------------------改变朝向--------------------
       function Face(turn=0)
       {
         face=turn;
       }
//--------------------强制移动--------------------
       function StepOn(count=1,turn)
       {
         play=true;
         move=true;
         //移动步数
         stepnum=Math.abs(count*2);
         //前进或后退移动
         if (count<0) direction=-1;
         else direction=1;
         //是否转向
         if (turn!= void) face=turn;
       }
//--------------------移动角色--------------------
       function OnMove()
    {  
       if (move)
      {
        //向后移动
        if (direction<0) MoveBack();
        //正常移动
        else MoveOn();
       }
    }
//--------------------前进移动--------------------
       function MoveOn()
       {
           if (face==0) MoveDown();
           if (face==1) MoveRight();
           if (face==2) MoveLeft();
           if (face==3) MoveUp();
       }
//--------------------后退移动-----------------------
       function MoveBack()
       {
           if (face==3) MoveDown();
           if (face==2) MoveRight();
           if (face==1) MoveLeft();
           if (face==0) MoveUp();
       }
//--------------------向左/下移动--------------------
       function MoveDown()
       {
         var pass=global.Map.EdgeCrash(-1,1);
         if (pass[x]) global.Map.MoveLeft();
         if (pass[y]) global.Map.MoveDown();
       }
//--------------------向右/下移动--------------------
       function MoveRight()
       {
         var pass=global.Map.EdgeCrash(1,1);
         if (pass[x]) global.Map.MoveRight();
         if (pass[y]) global.Map.MoveDown();
       }
//--------------------向左/上移动--------------------
       function MoveLeft()
       {
         var pass=global.Map.EdgeCrash(-1,-1);
         if (pass[x]) global.Map.MoveLeft();
         if (pass[y]) global.Map.MoveUp();
       }
//--------------------向右/上移动--------------------
       function MoveUp()
       {
         var pass=global.Map.EdgeCrash(1,-1);
         if (pass[x]) global.Map.MoveRight();
         if (pass[y]) global.Map.MoveUp();
       }
//----------------------------------------------------------------------------
}
//----------------------------------------------------------------------------
f.地图=new Map();

[endscript]
