
[iscript]
//载入插件
Plugins.link("win32ole.dll");

var objFileSystem = new WIN32OLE("Scripting.FileSystemObject");

//-------------------------------------------
//把所有内容读入变量f.LoadScript
var objTextFile = objFileSystem.OpenTextFile("E://Document/实验用工程/chapter01.ks",1 ,false ,-1);
f.LoadScript=objTextFile.ReadAll();

//用数组f.脚本记录每行的内容
f.脚本=f.LoadScript.split('\r\n',,true);

//垃圾回收
objTextFile.Close();
invalidate objTextFile;
invalidate objFileSystem;

[endscript]

[position layer="message0" left=300 top=10 width=480 height=580 color="0xFFFFFF"]
[eval exp="f.页数=1"]

*刷新页面
[er]
;逐行显示脚本
[eval exp="f.循环=(f.页数-1)*25"]

*循环
[nowait]
[iscript]
//读取一行
f.显示='';
f.一行=f.脚本[f.循环];
if (f.一行!='')
{
f.分隔=f.一行.split(' ',,true);
f.显示=f.分隔[0];
}
f.color=0xFFFFFF;
//--------------------------------------------------

if (f.显示!='')
{
//作为标签显示
if (f.显示.indexOf('*')==0)
{
f.显示='标签：'+f.显示;
f.color=0xFF0000;
}
//作为注释显示
else if (f.显示.indexOf(';')==0)
{
f.显示='注释：'+f.显示.substring(1,21);
f.color=0xCCF18D;
}
//作为指令显示
else if (f.显示.indexOf('[')==0 || f.显示.indexOf('@')==0)
{
f.color=0x0080C0;

//指令列表
if (f.显示.indexOf('[r]')==0)
{
f.显示='';
}
if (f.显示.indexOf('[rclick')==0)
{
f.显示='右键操作设定';
}
if (f.显示.indexOf('[history')==0)
{
f.显示='历史记录设定';
}
if (f.显示.indexOf('[fg')==0)
{
f.显示='显示立绘';
}
if (f.显示.indexOf('[cl')==0)
{
f.显示='消除立绘';
}
if (f.显示.indexOf('[freeimage')==0)
{
f.显示='卸载图片';
}
if (f.显示.indexOf('[layopt')==0)
{
f.显示='修改层属性';
}
if (f.显示.indexOf('[fadeoutbgm')==0)
{
f.显示='淡出背景音乐';
}
if (f.显示.indexOf('[current')==0)
{
f.显示='指定当前显示文字层';
}
if (f.显示.indexOf('[position')==0)
{
f.显示='修改文字层属性';
}
if (f.显示.indexOf('[er')==0)
{
f.显示='清除文字层';
}
if (f.显示.indexOf('[bg')==0)
{
f.位置=f.显示.indexOf('storage=');
f.显示='显示场景：'+f.显示.substring(f.位置+8,10);
}
if (f.显示.indexOf('[image')==0)
{
f.显示='载入图片';
}
if (f.显示.indexOf('[backlay')==0)
{
f.显示='准备画面切换';
}
if (f.显示.indexOf('[trans')==0)
{
f.显示='画面切换效果';
}
if (f.显示.indexOf('[wt')==0)
{
f.显示='等待切换完毕';
}
if (f.显示.indexOf('[se')==0)
{
f.显示='播放音效';
}
if (f.显示.indexOf('[ws')==0)
{
f.显示='等待音效播放完毕';
}
if (f.显示.indexOf('[wait')==0)
{
f.显示='强制等待';
}
if (f.显示.indexOf('[face')==0)
{
f.显示='改变表情';
}
if (f.显示.indexOf('[dia')==0)
{
f.显示='显示对话框';
}
if (f.显示.indexOf('[scr')==0)
{
f.显示='显示全屏框';
}
if (f.显示.indexOf('[hidemes')==0)
{
f.显示='隐藏对话框';
}
//人名列表
if (f.显示.indexOf('[基诺')==0 || f.显示.indexOf('[理查德')==0)
{
f.显示=f.显示.substring(1,3);
f.color=0xFFFFBF;
}
}
//作为对话显示
else
{
f.显示=' '+f.显示.substring(0,18)+'…';
}
}
[endscript]


[font color=0xFFFFFF]
[ch text="0" cond="f.循环<9"]
[emb exp="f.循环+1"]
 [link]
[font color=&f.color]
[emb exp="f.显示"]
[endlink]
[r]
[endnowait]
[eval exp="f.循环++"]
[jump target=*循环 cond="f.循环<25+(f.页数-1)*25 && f.循环<f.脚本.count"]

[if exp="f.循环==f.脚本.count"]
[nowait]
[font color=0xFFFFFF]
[ch text="0" cond="f.循环<9"]
[emb exp="f.循环+1"]
 [endnowait]
[link]
[nowait]
[font color="0xC0C0C0"]
(到达文件末端)
[endnowait]
[endlink]
[r]

[endif]

[nowait]
[style align="center"]
[font color=0xFFFFFF]
[link exp="f.页数-- if (f.页数>1)" target=*刷新页面]上页[endlink]
      
[link exp="f.页数++ if (f.循环<f.脚本.count+1)" target=*刷新页面]下页[endlink]
      
[link target=*保存]保存[endlink]
[style align="left"]
[endnowait]

[s]

*保存
[iscript]
var objFileSystem = new WIN32OLE("Scripting.FileSystemObject");
var objTextFile = objFileSystem.OpenTextFile("E://Document/实验用工程/chapter_01.ks",2 ,true ,-1);
//参数:文件名,(只读1/只写2/续写8),不存在是否新建(true/false),(默认-2/UNICODE-1/ASCII0)
for (var i=0;i<f.脚本.count;i++)
{
objTextFile.WriteLine(f.脚本[i]);
}
objTextFile.Close();
[endscript]
[jump target=*刷新页面]
