;------------------------------------------------
;调色板实验
;------------------------------------------------
[bg storage=white]

[iscript]
//起点坐标,起点值,终点值
function drawline(x=0,y=0,st=0xFFFFFF,ed=0x000000)
{
   var dis;
   
   if (ed>=st)
   {
      dis=(ed-st)/255;
      
      for (var i=0;i<256;i++)
      {
         kag.fore.layers[0].colorRect(i,y,1,1,i*dis);
      }
   }
   else
   {
      dis=(st-ed)/255;
      for (var i=0;i<255;i++)
      {
         kag.fore.layers[0].colorRect(255-i,y,1,1,i*dis);
      }
   }
}

with (kag.fore.layers[0])
{
.visible=true;
.loadImages(%["storage"=>"empty"]);
.setSizeToImageSize();
}

  for (var j=0;j<256;j++)
  {
      drawline(0,j,0xFFFF00+j);
  }
//  for (var j=128;j<256;j++)
//  {
//      drawline(0,j,0xFFC8C8);
//  }
[endscript]
