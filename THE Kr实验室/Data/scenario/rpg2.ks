*start
[iscript]
//----------------------------------------------------------------------------
//地图信息类
//----------------------------------------------------------------------------
class MapInfo extends KAGLayer
{
//读入地图障碍
  function MapInfo(storage)
  {
     //创建层
     super.KAGLayer(kag, kag.fore.base);
     //读入
     loadImages(storage);
     setSizeToImageSize();
  }
//销毁
    function finalize()
  {
      super.finalize(...);
  }
//读取层上某点信息
    function find(x,y)
  {   //允许通行=0x47657F;其他=0x9BB3C7
     if (x<=1 || x>=799 || y<=1 || y>=599) return false;
     else return true;
      var color1=getMainPixel(x-f.地图.hero.width/2,y-f.地图.hero.height/2);
      var color2=getMainPixel(x+f.地图.hero.width/2,y-f.地图.hero.height/2);
      var color3=getMainPixel(x-f.地图.hero.width/2,y+f.地图.hero.height/2);
      var color4=getMainPixel(x+f.地图.hero.width/2,y+f.地图.hero.height/2);
//      if (color1==0x47657F && color2==0x47657F && color3==0x47657F && color4==0x47657F) return true;
//      else return false;
  }
}
//----------------------------------------------------------------------------
//地图层
//----------------------------------------------------------------------------
class MapBase extends KAGLayer
{
//读入地图层
  function MapBase(storage)
  {
     //创建层
     super.KAGLayer(kag, kag.fore.base);
     //读入层图片
     loadImages(storage);
     setSizeToImageSize();
     visible=true;
     absolute=0;
  }
//销毁
    function finalize()
  {
      super.finalize(...);
  }
}
//----------------------------------------------------------------------------
//主角层
//----------------------------------------------------------------------------
class Hero extends KAGLayer
{
   var CountStep=new Timer(OnStep,'');//播放行走动画
   var move=false;//移动状态
   var face;//当前朝向
   var step;//当前帧
   
   //取得中点坐标值
      property x
   {
       setter(a)
       {
          x=a;
       }
        getter ()
        {
          return left+width/2;
        }
   }
   property y
   {
       setter(a)
       {
          y=a;
       }
        getter ()
        {
          return top+height/2;
        }
   }
   //创建函数
   function Hero(char="hero",face=0,rate=105)
   {
     //创建层
      super.KAGLayer(kag, kag.fore.base);
     //载入图片
      loadImages(char);
      visible=true;
	 //层大小为角色大小
	 setSize(imageWidth/4,imageHeight/4);
	 CountStep.interval=rate;
	 CountStep.enabled=true;
   }
   //删除函数
    function finalize()
    {
        invalidate CountStep;
      	super.finalize(...);
    }
    //播放行走动画
        function OnStep()
    {
       if (move) 
       {
         if (step<3) step++;
         else step=0;
        }
       else if (step==3) step=0;
       else if (step==1) step=2;
       //根据朝向显示角色图
	   setImagePos(-imageWidth*step/4, -imageHeight*face/4);
    }
}
//----------------------------------------------------------------------------
//NPC层
//----------------------------------------------------------------------------
class NPC extends Hero
{
   var RandomRoute=new Timer(GetNextStep,'');//随机行动
   //创建函数
   function NPC()
   {
    super.Hero(...);
    if(typeof kag.cursorPointed !== "undefined")
	cursor = kag.cursorPointed;
    hitType = htMask;
	hitThreshold = 0;
	left=200;
	top=300;
	CountStep.interval=200;
	RandomRoute.interval=500;
	RandomRoute.enabled=true;
   }
   //删除函数
    function finalize()
    {
      	super.finalize(...);
    }
   //随机行动函数
    function GetNextStep()
    {
      var action=intrandom(0,2);
      //当移动中
      if (this.move)
      //停止
      {
      if (action==0) this.move=false;
      //改变方向
      if (action==1) this.face=intrandom(0,3);
      }
       else this.move=true;
    }
    function MoveOn()
    {
      if (this.face==0) {this.left--;this.top++;}
      if (this.face==1) {this.left++;this.top++;}
      if (this.face==2) {this.left--;this.top--;}
      if (this.face==3) {this.left++;this.top--;}
    }
}
//----------------------------------------------------------------------------
//地图本身
//----------------------------------------------------------------------------
class Map extends KAGLayer
{
   var mapinfo;
   var mapbase;
   var hero;
   var frame;
   var npc=[];
//-----------------------------------------------------------
//创建函数
    function Map()
    {
     //创建层
      mapinfo=new MapInfo("tiles");
      mapbase=new MapBase("tiles");
     //创建主角
      hero=new Hero();
      hero.left=400;
      hero.top=300;
     //创建npc
     npc[0]=new NPC();
     //创建监视器
     frame=new Timer(check,"");
     frame.interval=10;
     frame.enabled=true;
    }
//-----------------------------------------------------------
//删除函数
    function finalize()
    {
        invalidate frame;
      	super.finalize(...);
    }
//-----------------------------------------------------------
//操作检测与碰撞判断
    function check()
    {
      	//取得当前按下的键
      	var vk_left = kag.getKeyState(VK_LEFT);
      	var vk_up = kag.getKeyState(VK_UP);
      	var vk_right = kag.getKeyState(VK_RIGHT);
      	var vk_down = kag.getKeyState(VK_DOWN);
      	
      	if (vk_down) MoveDown();
      	else if (vk_left) MoveLeft();
      	else if (vk_right) MoveRight();
      	else if (vk_up) MoveUp();
      	else hero.move=false;
      	//假如NPC随机移动中,移动NPC
      	if (npc[0].move) npc[0].MoveOn();
    }
//-----------------------------------------------------------
//相关移动
    function MoveDown()
    {
      //改变朝向和状态
      hero.face=0;
      hero.move=true;
      if (hero.y+hero.height/2>=599) return;
      //障碍判断
      var pass=mapinfo.find(hero.x-mapbase.left-1,hero.y-mapbase.top+1);
      if (pass) 
      {
       if (mapbase.top+mapbase.height>600 && hero.y>=300) {mapbase.top--;npc[0].top--;}
       else hero.top++;
       if (mapbase.left<0 && hero.x<400) {mapbase.left++;npc[0].left++;}
       else hero.left--;
      }
      
    }
    function MoveLeft()
    {
      //改变朝向和状态
      hero.face=2;
      hero.move=true;
      if (hero.left<=1) return;
      //障碍判断
      var pass=mapinfo.find(hero.x-mapbase.left-1,hero.y-mapbase.top-1);
      if (pass) 
      {
       if (mapbase.left<0 && hero.x<400) {mapbase.left++;npc[0].left++;}
       else hero.left--;
       if (mapbase.top<0 && hero.y<300) {mapbase.top++;npc[0].top++;}
       else hero.top--;
      }
    }
    function MoveRight()
    {
      //改变朝向和状态
      hero.face=1;
      hero.move=true;
      if (hero.x+hero.width/2>=799) return;
      //障碍判断
      var pass=mapinfo.find(hero.x-mapbase.left+1,hero.y-mapbase.top+1);
      if (pass) 
      {
       if (mapbase.left+mapbase.width>800 && hero.x>=400) {mapbase.left--;npc[0].left--;}
       else hero.left++;
       if (mapbase.top+mapbase.height>600 && hero.y>=300) {mapbase.top--;npc[0].top--;}
       else hero.top++;
      }
    }  
    function MoveUp()
    {
      //改变朝向和状态
      hero.face=3;
      hero.move=true;
      if (hero.top<=1) return;
      //障碍判断
      var pass=mapinfo.find(hero.x-mapbase.left+1,hero.y-1-mapbase.top);
      if (pass) 
      {
       if (mapbase.top<0 && hero.y<300) mapbase.top++;
       else hero.top--;
       if (mapbase.left+mapbase.width>800 && hero.x>=400) mapbase.left--;
       else hero.left++;
      }
    } 
}
f.地图=new Map();
[endscript]

