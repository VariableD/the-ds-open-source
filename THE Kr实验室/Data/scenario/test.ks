*start|
@bg storage="茶室_昼" method=turn time=700 canskip=true
@scr
[nowait]
[r]
告诉我你的名字：[r]
[r]
姓：
@edit name="f.姓" opacity=0
[r]
名：
@edit name="f.名" opacity=0
[r]
[r]
@links text="填好了" target=*确认
[r]
[endnowait]
[s]
*确认|
[commit]
@ct
[r]
@emb exp="f.姓+f.名"
[r]
[r]
世界就[lr]
注定[lr]
要充满[lr]
无聊的[lr]
占位符[lr]
@dia
@npc id="测试" edge=true
现在要开始测试选项
@selstart hidemes=0 hidesysbutton=0
@locate x=0 y=100
@selbutton normal="选择条" target=选择选项 text="选项一" color1=0xFFFFFF color2=0x505050 color3=0x00009B color4=0xF50000
@locate y=170
@selbutton normal="选择条" target=选择选项 text="选项二" color1=0xFFFFFF color2=0x505050 color3=0x00009B color4=0xF50000
@locate y=240
@selbutton normal="选择条" target=选择选项 text="选项三" color1=0xFFFFFF color2=0x505050 color3=0x00009B color4=0xF50000
@selend time=100 timeout=true target=*返回标题 timebar=true bar=时间条 bgimage=选择条 x=96 y=3 bgx= bgy= width=606 outtime=5000 method=universal rule="22" canskip=false
;--------------------------------------
*返回标题|
[deltimebar]
@ct
@dia
选择超时，将返回标题[w]
@gotostart ask=false
;--------------------------------------
*选择选项|
[deltimebar]
@ct
@dia
选择了选项，接下来将返回标题。[w]
@gotostart ask=false
