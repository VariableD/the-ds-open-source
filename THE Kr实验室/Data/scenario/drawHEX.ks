
[menul]

[iscript]
KAGLoadScript('Utils_graphics.tjs');
[endscript]
[image layer=base storage=艾茨格黎姆宅-晴 visible=true]
[image layer=stage storage=储存-底层 visible=true]  
[image layer=0 storage=empty visible=true]
[image layer=1 storage=empty visible=true opacity=128]
[image layer=2 storage=empty visible=true]

[iscript]
//六边形顶点计算
function Hexagon(yc, xc, r)
{
var p1x=xc-r;
var p1y=yc;

var p2x=xc-r/2;
var p2y=yc-Math.sqrt(3)*r/2;

var p3x=xc+r/2;
var p3y=yc-Math.sqrt(3)*r/2;

var p4x=xc+r;
var p4y=yc;

var p5x=xc+r/2;
var p5y=yc+Math.sqrt(3)*r/2;

var p6x=xc-r/2;
var p6y=yc+Math.sqrt(3)*r/2;

var point=[p1y,p1x,p2y,p2x,p3y,p3x,p4y,p4x,p5y,p5x,p6y,p6x];
return point;
}
//能力对应顶点计算
function ability(xc,yc,a1,a2,a3,a4,a5,a6)
{
var point1=Hexagon(xc, yc, a1);
var point2=Hexagon(xc, yc, a2);
var point3=Hexagon(xc, yc, a3);
var point4=Hexagon(xc, yc, a4);
var point5=Hexagon(xc, yc, a5);
var point6=Hexagon(xc, yc, a6);

var point=[point1[0],point1[1],point2[2],point2[3],point3[4],point3[5],point4[6],point4[7],point5[8],point5[9],point6[10],point6[11]];
return point;
}
[endscript]

[eval exp="f.test=Hexagon(140, 225, 101)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]
[eval exp="drawLine(kag.fore.layers[0], f.test[0], f.test[1], f.test[6], f.test[7], 0xFFFFFFFF)"]
[eval exp="drawLine(kag.fore.layers[0], f.test[2], f.test[3], f.test[8], f.test[9], 0xFFFFFFFF)"]
[eval exp="drawLine(kag.fore.layers[0], f.test[4], f.test[5], f.test[10], f.test[11], 0xFFFFFFFF)"]

[eval exp="f.test=Hexagon(140, 225, 81)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]
[eval exp="f.test=Hexagon(140, 225, 61)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]
[eval exp="f.test=Hexagon(140, 225, 41)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]
[eval exp="f.test=Hexagon(140, 225, 21)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]
[eval exp="f.test=Hexagon(140, 225, 0)"]
[eval exp="drawPolygon(kag.fore.layers[0], f.test, 0xFFFFFFFF, false)"]



[eval exp="f.test=ability(140,225,60,20,100,60,80,100)"]
[eval exp="drawPolygon(kag.fore.layers[1], f.test, 0xFF0000FF, true)"]
[eval exp="f.test=ability(140,225,61,21,101,61,81,101)"]
[eval exp="drawPolygon(kag.fore.layers[2], f.test, 0xFFFFFF80, false)"]
[s]
