*start
[iscript]
//----------------------------------------------------------------------------
//地图类
//----------------------------------------------------------------------------
class Map extends KAGLayer
{
var hero;
var npc=[];
var crashcheck=new Timer(OnCheck,"");
//-----------------------------------------------------------
//创建函数
//-----------------------------------------------------------
    function Map(x=0,y=0)
    {
     //创建层
      super.KAGLayer(kag, kag.fore.base);
      
      hitType = htMask;
      hitThreshold = 0;
      focusable = true; 
      loadImages("bg1");
      setSizeToImageSize();
      visible=true;
      absolute=0;
      hero=new Hero();//创建主角
      hero.left=x-hero.width/2;
      hero.top=y-hero.height/2;
      //创建NPC
     npc[1]=new NPC();
     crashcheck.interval=10;
	 crashcheck.enabled=true;
    }
//-----------------------------------------------------------
//删除函数
//-----------------------------------------------------------
    function finalize()
    {
      	super.finalize(...);
    }
//-----------------------------------------------------------
//碰撞检测
//-----------------------------------------------------------
/*
 
一般规则的物体碰撞都可以处理成矩形碰撞，实现的原理就是检测两个矩形是否重叠。我们假设矩形1的参数是：左上角的坐标是(x1,y1)，宽度是w1，高度是h1；矩形2的参数是：左上角的坐标是(x2,y2)，宽度是w2，高度是h2。 

在检测时，数学上可以处理成比较中心点的坐标在x和y方向上的距离和宽度的关系。即两个矩形中心点在x方向的距离的绝对值小于等于矩形宽度和的二分之一，同时y方向的距离的绝对值小于等于矩形高度和的二分之一。下面是数学表达式： 

x方向：| (x1 + w1 / 2) – (x2 + w2/2) | < |(w1 + w2) / 2| 

y方向：| (y1 + h1 / 2) – (y2 + h2/2) | < |(h1 + h2) / 2| 

*/

function OnCheck()
{
  hero.crash=-1;
//  var disX=Math.abs(npc[1].x-hero.x);
//  var disY=Math.abs(npc[1].y-hero.y);
//  var ZoneX=(npc[1].width+hero.width)/2;
//  var ZoneY=(npc[1].height+hero.height)/2;
//  var dis=Math.sqrt(disX*disX+disY*disY);
//  if (dis<ZoneX && dis<ZoneY)
//  {
//  //禁下
//  if (disY<ZoneY && (npc[1].y-hero.y)>0) hero.crash=0;
//  //禁左
//  if (disX<ZoneX && (npc[1].x-hero.x)<0) hero.crash=1;
//  //禁右
//  if (disX<ZoneX && (npc[1].x-hero.x)>0) hero.crash=2;
//  //禁上
//  if (disY<ZoneY && (npc[1].y-hero.y)<0) hero.crash=3;
//  }
}
//-----------------------------------------------------------
//操作
//-----------------------------------------------------------
    function onKeyDown(key,shift)
    {
      if (key==VK_DOWN) hero.face=0;
      if (key==VK_LEFT) hero.face=1;
      if (key==VK_RIGHT) hero.face=2;
      if (key==VK_UP) hero.face=3;
	  hero.move=true;
    }
    function onKeyPress(key,shift)
    {
      if (key==VK_DOWN) hero.face=0;
      if (key==VK_LEFT) hero.face=1;
      if (key==VK_RIGHT) hero.face=2;
      if (key==VK_UP) hero.face=3;
	  hero.move=true;
    }
    function onKeyUp()
    {
	  hero.move=false;
    }
 
}
//----------------------------------------------------------------------------
//NPC类
//----------------------------------------------------------------------------
class NPC extends KAGLayer
{
//-----------------------------------------------------------
//取得中点坐标值(碰撞检测用)
//-----------------------------------------------------------
   property x
   {
       setter(a)
       {
          x=a;
       }
        getter ()
        {
          return left+width/2;
        }
   }
   property y
   {
       setter(a)
       {
          y=a;
       }
        getter ()
        {
          return top+height/2;
        }
   }
//-----------------------------------------------------------
//创建函数
//-----------------------------------------------------------
   function NPC(char="hero")
   { 
     //创建层
      super.KAGLayer(kag, kag.fore.base);
     //载入图片
      loadImages(char);
      visible=true;
	 //层大小为角色大小
	 setSize(imageWidth/4,imageHeight/4);
	 absolute=1000;
	 left=100;
	 top=100;
   }
//-----------------------------------------------------------
//删除函数
//-----------------------------------------------------------
    function finalize()
    {
      	super.finalize(...);
    }
}
//----------------------------------------------------------------------------
//主角类
//----------------------------------------------------------------------------
class Hero extends KAGLayer
{
   var CountFrame=new Timer(Move,'');//位置移动
   var CountStep=new Timer(OnStep,'');//播放行走动画
   var ControlStep=new Timer(Wait,'');//角色演出用
   var pace;//移动步伐
   var move=false;//移动状态
   var crash=-1;//碰撞检测
   var face;//当前朝向
   var step;//当前帧
//-----------------------------------------------------------
//取得中点坐标值(碰撞检测用)
//-----------------------------------------------------------
   property x
   {
       setter(a)
       {
          x=a;
       }
        getter ()
        {
          return left+width/2;
        }
   }
   property y
   {
       setter(a)
       {
          y=a;
       }
        getter ()
        {
          return top+height/2;
        }
   }
//-----------------------------------------------------------
//创建函数
//-----------------------------------------------------------
   function Hero(char="hero",rate=105)
   { 
     //创建层
      super.KAGLayer(kag, kag.fore.base);
     //载入图片
      loadImages(char);
      visible=true;
	 //层大小为角色大小
	 setSize(imageWidth/4,imageHeight/4);
	 CountStep.interval=rate;
	 CountStep.enabled=true;
	 CountFrame.interval=10;
	 CountFrame.enabled=true;
   }
//-----------------------------------------------------------
//删除函数
//-----------------------------------------------------------
    function finalize()
    {
      	super.finalize(...);
    }
//-----------------------------------------------------------
//移动位置
//-----------------------------------------------------------
    function Move()
    {
      if (move)
       {
          if (face==0) 
          {
           if (crash==0) return;
           if (f.地图.top+f.地图.height>600 && y>=300) f.地图.top--;
           else if (top<600-height) top++;
          }
          if (face==1) 
          {
            if (crash==1) return;
            if (f.地图.left<0 && x<400) f.地图.left++;
            else if (left>0) left--;
          }
          if (face==2)
          {
            if (crash==2) return;
            if (f.地图.left+f.地图.width>800 && x>=400) f.地图.left--;
            else if (left<800-width) left++;
          }
          if (face==3) 
          {
            if (crash==3) return;
            if (f.地图.top<0 && y<300) f.地图.top++;
            else if (top>0) top--;
          }
       }
    }
//-----------------------------------------------------------
//行走动画
//-----------------------------------------------------------
    function OnStep()
    {
       if (move) Step();
       else if (step==3) step=0;
       else if (step==1) step=2;
       //根据朝向显示角色图
	setImagePos(-imageWidth*step/4, -imageHeight*face/4);
    }
//-----------------------------------------------------------
//播放下一帧
//-----------------------------------------------------------
    function Step()
    {
     if (step<3) step++;
     else step=0;
    }
//-----------------------------------------------------------
//角色演出
//-----------------------------------------------------------
//-----------角色开始移动---------------
    function MoveOn(count=1)
    {
      //开始移动
      pace=count;
      move=true;
      ControlStep.interval=CountStep.interval;
      ControlStep.enabled=true;
    }
//-----------等待角色移动---------------
    function Wait()
    {
      pace--;
      if (pace==0) 
       {
         move=false;
         ControlStep.enabled=false;
        }
     }
//-----------------------------------------------------------
}
//-----------------------------------------------------------
//类定义结束
//-----------------------------------------------------------


//-----------------------------------------------------------
//执行
//-----------------------------------------------------------
f.地图=new Map(400,300);
[endscript]
