*start
[iscript]

class puzzle extends Layer
{
var drag=false;
var FocusNum=0;
// 透明度プロパティ

var x=0;
var y=0;
property posX
{
  setter(a)
  {
    if (a < -86)
      x = -86;
    else if (a > 390)
      x = 390;
    else
      x = a;
  }
  getter()
  {
    return x;
  }
}
property posY
{
  setter(a)
  {
    if (a < -86)
      y = -86;
    else if (a > 590)
      y = 590;
    else
      y = a;
  }
  getter()
  {
    return y;
  }
}

	//创建函数
	function puzzle(win, par)
	{
		super.Layer(win, par);
		width=800;
		height=600;
		visible=true;
		hitType = htMask;
		hitThreshold = 0;
		
	}
	//删除函数
	function finalize()
	{
		super.finalize(...);
	}
	
	//鼠标点下时
	function onMouseDown(x, y, button, shift)
	{
	 var test=false;
	 for (var num=0;num<=1;num++)
	{
        if (kag.fore.base.cursorX>=kag.fore.layers[num].left && kag.fore.base.cursorX<=kag.fore.layers[num].left+96*1 && kag.fore.base.cursorY>=kag.fore.layers[num].top && kag.fore.base.cursorY<=kag.fore.layers[num].top+96*1)
        {
          test=true;
          FocusNum=num;
          break;
        }
     }
        if (button==mbLeft && test==true) {drag=true;}
		super.onMouseDown(...);
	}
	
	//鼠标移动时
    function onMouseMove(x, y, shift)
	{
        if (drag){
        posX=kag.fore.base.cursorX-48;
        posY=kag.fore.base.cursorY-48;
        kag.fore.layers[FocusNum].left=posX;
        kag.fore.layers[FocusNum].top=posY;
        }
		super.onMouseMove(...);
	}
	//鼠标离开时
    function onMouseUp(x, y, button, shift)
    {
        if (button==mbLeft) {drag=false;}
		super.onMouseUp(...);
    }
}
[endscript]
[eval exp="var test=new puzzle(kag, kag.fore.base)"]
[image layer=0 storage="dragpic" visible="true" left=100 top=100]
[image layer=1 storage="dragpic2" visible="true" left=200 top=100]


