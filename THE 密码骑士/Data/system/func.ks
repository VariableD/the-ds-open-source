;------------------------------------------------------------
;   KAGeXpress ver 3.0 封装宏集
;   Author: Miliardo/2006.11.5
;   Author: 长山真夜/Ver 2.0
;
;   http://kcddp.keyfc.net/
;   (C) 2002-2006，Key Fans Club
;
;------------------------------------------------------------

@jump target=*init
;------------------------------------------------------------
;    初始设定
;------------------------------------------------------------

*init

;@loadplugin module=wuvorbis.dll
;@loadplugin module=wump3.dll
;@loadplugin module=wutcwf.dll
;@loadplugin module=extrans.dll
;@loadplugin module=extNagano.dll

@if exp="global.useconfigMappfont==true"
@mappfont storage=&global.configMappfont
@endif

@macro name=vend
@if exp="tf.KAGeXpress_voice=1"
@eval exp="tf.KAGeXpress_voice=0"
@endhact
@endif
@endmacro

@macro name=p
@vend
@oporig
@endmacro


;------------------------------------------------------------
;    内容调整
;------------------------------------------------------------
;前景图层数量修改（默认为3个）
@autolabelmode mode=true
@erafterpage mode=true
@history type=scroll

@jump storage=first.ks

