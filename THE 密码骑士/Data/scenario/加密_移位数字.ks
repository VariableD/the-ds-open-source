;-------------------------------------------------------------------
;利用简单移位+字母转换数字加密
;-------------------------------------------------------------------
*start
[commit]
[er]
[jump target=*输出 cond="f.明文==''"]

[eval exp="f.明文=min2cap(f.明文)"]
;密钥的处理
[eval exp="f.密钥%26"]
;-----------------------------------------------------------------
;设计对照表
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]
[eval exp="tf.密文=''"]
[eval exp="tf.循环=0"]

*循环
;取得明文中的第N个字母
[eval exp="tf.单字=f.明文.substring(tf.循环,1)"]
;取得字母在表格中的位置
[eval exp="tf.位置=f.表格.indexOf(tf.单字)"]

;没找到的情况
[if exp="tf.位置==-1"]
[eval exp="tf.单字=' '"]
[jump target=*代入]
[endif]

;修改位置
[eval exp="tf.位置+=f.密钥*1"]
[eval exp="tf.位置-=26" cond="tf.位置>25"]
[eval exp="tf.单字=f.表格.substring(tf.位置,1)"]

*代入
[eval exp="tf.密文+=tf.单字"]
[eval exp="tf.循环++"]
[jump target=*循环 cond="tf.循环<f.明文.length"]

*输出
;[er]
[nowait]
移位数字加密：[r]
明文：[emb exp="f.明文"][r]
密文：[emb exp="tf.密文"][r]
数字：[emb exp="cap2num(tf.密文)" cond="tf.密文!=''"][r]
[style align="right"]
[link storage="armor.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]
