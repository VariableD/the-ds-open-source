;-----------------------------------------------------
;信息加密处理
;-----------------------------------------------------
*start
[nowait]
[er]
[r]
[style align="center"]
信息加密处理[r]
[r]
[style align="left"]
请输入明文：[edit name=f.明文 length=400 opacity=0 color="0xFFFFFF"][r][r]
请输入密钥：[edit name=f.密钥 opacity=0 color="0xFFFFFF"][r][r]

　　[link storage="加密_恺撒单字.ks" hint="将某个关键字作为密钥，之后的字母依序后移的加密方式"]恺撒单字加密[endlink]　
[link storage="加密_移位数字.ks" hint="使用简单移位后将全部字母转换为数字的加密（密钥：移位数）"]移位数字加密[endlink]　
[link storage="加密_摩斯电码.ks" hint="摩斯电码加密[同时转化为伪二进制数字]（无需密钥）"]摩斯电码加密[endlink][r]
　　[link storage="加密_栅栏易位.ks" hint="将密文拆分成数行后合并（密钥：行数）"]栅栏易位加密[endlink]　
[link storage="first.ks" target=*start]返回[endlink]

[endnowait]
[s]


*fence
[s]

