*start
;-----------------------------------------------------
[iscript]
//------------------------------------------------
//存储跳舞的小人图片
//------------------------------------------------
function savenote()
{
var fn = System.exePath + "/dance/" + f.明文+"_"+f.样式 +".bmp";
var tmp = new Layer(kag, kag.fore.base);
            tmp.setImageSize(640, 480);
tmp.piledCopy(0, 0, kag.fore.base, 0, 0, 640, 480);
tmp.saveLayerImage(fn, "bmp");
}
//------------------------------------------------
//将大小写进行转换
//------------------------------------------------
function min2cap(str)
{
	var res;
	var i;
	for(i=0; i<str.length; i++)
	{
		var ch = str[i];
		switch(ch)
		{
		case "a": res+="A"; break;
		case "b": res+="B"; break;
		case "c": res+="C"; break;
		case "d": res+="D"; break;
		case "e": res+="E"; break;
		case "f": res+="F"; break;
		case "g": res+="G"; break;
		case "h": res+="H"; break;
		case "i": res+="I"; break;
		case "j": res+="J"; break;
		case "k": res+="K"; break;
		case "l": res+="L"; break;
		case "m": res+="M"; break;
		case "n": res+="N"; break;
		case "o": res+="O"; break;
		case "p": res+="P"; break;
		case "q": res+="Q"; break;
		case "r": res+="R"; break;
		case "s": res+="S"; break;
		case "t": res+="T"; break;
		case "u": res+="U"; break;
		case "v": res+="V"; break;
		case "w": res+="W"; break;
		case "x": res+="X"; break;
		case "y": res+="Y"; break;
		case "z": res+="Z"; break;
		default: res+=ch; break;
		}
	}
	return res;
}
//------------------------------------------------
//将字母转换为数字[1-26]
//------------------------------------------------
function cap2num(str)
{
	var res;
	var i;
	for(i=0; i<str.length; i++)
	{
		var ch = str[i];
		switch(ch)
		{
		case "A": res+="1"; break;
		case "B": res+="2"; break;
		case "C": res+="3"; break;
		case "D": res+="4"; break;
		case "E": res+="5"; break;
		case "F": res+="6"; break;
		case "G": res+="7"; break;
		case "H": res+="8"; break;
		case "I": res+="9"; break;
		case "J": res+="10"; break;
		case "K": res+="11"; break;
		case "L": res+="12"; break;
		case "M": res+="13"; break;
		case "N": res+="14"; break;
		case "O": res+="15"; break;
		case "P": res+="16"; break;
		case "Q": res+="17"; break;
		case "R": res+="18"; break;
		case "S": res+="19"; break;
		case "T": res+="20"; break;
		case "U": res+="21"; break;
		case "V": res+="22"; break;
		case "W": res+="23"; break;
		case "X": res+="24"; break;
		case "Y": res+="25"; break;
		case "Z": res+="26"; break;
		default: res+=ch; break;
		}
	}
	return res;
}
//------------------------------------------------
//将信息转换为摩斯电码
//------------------------------------------------
function mes2mos(str)
{
	var res;
	var i;
	for(i=0; i<str.length; i++)
	{
		var ch = str[i];
		switch(ch)
		{
		//数字档
		case "1": res+="*----/";break;
		case "2": res+="**---/";break;
		case "3": res+="***--";break;
		case "4": res+="****-";break;
		case "5": res+="*****/";break;
		case "6": res+="-****/";break;
		case "7": res+="--***/";break;
		case "8": res+="---**/";break;
		case "9": res+="----*/";break;
		case "0": res+="-----/";break;
		//字母档
		case "A": res+="*-/"; break;
		case "B": res+="-***/"; break;
		case "C": res+="-*-*/"; break;
		case "D": res+="-**/"; break;
		case "E": res+="*/"; break;
		case "F": res+="**-*/"; break;
		case "G": res+="--*/"; break;
		case "H": res+="****/"; break;
		case "I": res+="**/"; break;
		case "J": res+="*---/"; break;
		case "K": res+="-*-/"; break;
		case "L": res+="*-**/"; break;
		case "M": res+="--/"; break;
		case "N": res+="-*/"; break;
		case "O": res+="---/"; break;
		case "P": res+="*--*/"; break;
		case "Q": res+="--*-/"; break;
		case "R": res+="*-*/"; break;
		case "S": res+="***/"; break;
		case "T": res+="-/"; break;
		case "U": res+="**-/"; break;
		case "V": res+="***-/"; break;
		case "W": res+="*--/"; break;
		case "X": res+="-**-/"; break;
		case "Y": res+="-*--/"; break;
		case "Z": res+="--**/"; break;
		//符号档
		case ".": res+="*-*-*-/";break;
		case ",": res+="--**--/";break;
		case ":": res+="---***/";break;
		case "'": res+="*----*/";break;
		case "?": res+="**--**/";break;
		case "-": res+="-****-/";break;
		case "(": res+="-*--*-/";break;
		case ")": res+="-*--*-/";break;
		case "@": res+="*--*-*/";break;
		case "—": res+="-***-/";break;
		case "/": res+="-**-*/";break;
		default: break;
		}
	}
	return res;
}

function mos2num(str)
{
	var res;
	var i;
	for(i=0; i<str.length; i++)
	{
		var ch = str[i];
		switch(ch)
		{
		case "-": res+="0"; break;
		case "*": res+="1"; break;
		case "/": res+=" "; break;
		default: break;
		}
	}
	return res;
}
[endscript]
;-----------------------------------------------------
[return]
