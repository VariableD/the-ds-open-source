;----------------------------------
;跳舞的小人
;经典图片密码制作
;----------------------------------
*start
[eval exp="f.样式='字条'"]
[nowait]
[current layer="message0"]
[er]
[r]
[style align="center"]
跳舞的小人[r]
[r]
[style align="left"]
请输入明文：[edit name=f.明文 length=400 opacity=0 color="0xFFFFFF" maxchars=74][r][r]
请选择样式：[r][r]
[link exp="f.样式='白底'"]白底[endlink]　[link exp="f.样式='黑底'"]黑底[endlink]　[link exp="f.样式='字条'"]字条[endlink]
[r][r]
[link target=*显示图片]显示图片[endlink]　
[link storage="first.ks" target=*start]返回[endlink][r]
[s]

*显示图片
[commit]
[jump target=*start cond="f.明文==''"]
[backlay]
[current layer="message0" page="back"]
[er]
[current layer="message1" page="back"]
[if exp="f.样式=='字条'"]
[image layer=stage page="back" storage="serihusyu_6" visible="true"]
[position layer="message1" page="back" visible="true" left=0 top=150 width=640 height=177 marginl=74 margint=30 opacity=0]
[font shadow="false" color="0x000000"]
[endif]
[if exp="f.样式=='白底'"]
[image layer=stage page="back" storage="white" visible="true"]
[position layer="message1" page="back" visible="true" left=0 top=0 width=640 height=400 marginl=54 margint=84 opacity=0]
[font shadow="false" color="0x000000"]
[endif]
[if exp="f.样式=='黑底'"]
[image layer=stage page="back" storage="black" visible="true"]
[position layer="message1" page="back" visible="true" left=0 top=0 width=640 height=400 marginl=54 margint=74 opacity=0]
[font shadow="false" color="0xFFFFFF"]
[endif]
[trans method="crossfade" time=200]
[wt]
[current layer="message1" page="fore"]
;=========================================
;处理成大写
[eval exp="f.明文=min2cap(f.明文)"]
[eval exp="tf.循环=0"]
[eval exp="tf.每行数=1"]

*循环
;读取某个字母
[eval exp="tf.单字=f.明文.substring(tf.循环,1)"]
[nowait]
;当不是空格的情况,判断下个字是否为空格
[if exp="tf.单字<='Z' && tf.单字>='A'"]
[eval exp="tf.下一字=f.明文.substring(tf.循环+1,1)"]
[graph storage="&tf.单字" cond="tf.下一字!=' '"]
[graph storage="&(tf.单字+'_')" cond="tf.下一字==' '"]
[endif]

;标点换行
[if exp="tf.单字=='.' || tf.单字==':' || tf.单字=='?' || tf.单字=='!'"]
[eval exp="tf.每行数=0"]
[r]
[endif]
;超过行数换行
[if exp="tf.每行数==15"]
[eval exp="tf.每行数=0"]
[r]
[endif]

[eval exp="tf.循环++"]
[eval exp="tf.每行数++"]
[endnowait]
[jump target=*循环 cond="tf.循环<f.明文.length"]

[current layer="message0" page="fore"]
[locate x=0 y=0]
[nowait]
			
[link exp="savenote()"]保存[endlink]　
[link target=*清除]返回[endlink]
[endnowait]

[s]

*清除
[backlay]
[current layer="message1" page="back"]
[er]
[layopt layer="message1" visible="false" page="back"]
[freeimage layer=stage page="back"]
[trans method="crossfade" time=200]
[wt]
[jump target=*start]


