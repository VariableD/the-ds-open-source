;-------------------------------------------------------------------
;利用字母替换进行加密
;-------------------------------------------------------------------
*start
[commit]
[er]
[jump target=*输出 cond="f.明文==''"]
[jump target=*输出 cond="f.密钥==''"]
[eval exp="f.明文=min2cap(f.明文)"]
[eval exp="f.密钥=min2cap(f.密钥)"]
;-------------------------------------------------------------------
;字母原位置
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]
;-------------------------------------------------------------------
;根据密钥单词进行处理
[eval exp="tf.密钥=''"]
*循环
;取得密钥单词中的第N个字母
[eval exp="tf.单字=f.密钥.substring(tf.循环,1)"]
;当密钥中不存在单字时，将单字加入
[eval exp="tf.密钥+=tf.单字" cond="tf.密钥.indexOf(tf.单字)==-1"]
[eval exp="tf.循环++"]
[jump target=*循环 cond="tf.循环<f.密钥.length"]
;到这里为止,tf.密钥里记录了正确的密钥[无重复字母]
;-------------------------------------------------------------------
;将密钥中的字母从字母表中去除
[eval exp="tf.对照=tf.密钥"]
[eval exp="tf.循环=0"]
*循环去除
;取得提示中的第N个字母
[eval exp="tf.单字=f.表格.substring(tf.循环,1)"]
;当密钥中不存在单字时，将单字加入最终字母表
[eval exp="tf.对照+=tf.单字" cond="tf.密钥.indexOf(tf.单字)==-1"]
[eval exp="tf.循环++"]
[jump target=*循环去除 cond="tf.循环<26"]
;到这里为止,tf.对照中记录了对应的替换顺序
;------------------------------------------------------------------------------
[eval exp="tf.密文=''"]
[eval exp="tf.循环=0"]

*循环替换
;取得明文中的第N个字母
[eval exp="tf.单字=f.明文.substring(tf.循环,1)"]
;取得该字在正常表中的位置
[eval exp="tf.位置=f.表格.indexOf(tf.单字)"]
;将该字替换为对照表中相同位置的字母
[eval exp="tf.单字=tf.对照.substring(tf.位置,1)"]
[eval exp="tf.密文+=tf.单字"]
[eval exp="tf.循环++"]
[jump target=*循环替换 cond="tf.循环<f.明文.length"]

*输出
[nowait]
恺撒单字加密：[r]
密钥：[emb exp="tf.密钥"][r][r]
对照表：[r]
[emb exp="f.表格"][r]
[emb exp="tf.对照"][r][r]
明文：[emb exp="f.明文"][r]
密文：[emb exp="tf.密文"][r]
[style align="right"]
[link storage="armor.ks" target=*start]返回[endlink]
[style align="left"]
[endnowait]
[s]
