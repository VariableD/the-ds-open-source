;-------------------------------------------------
;tf.单行长度 栅栏每行所含的文字数
;tf.栅栏 记录所有文字的栅栏数组
;tf.循环 循环用计数器[当前输出的是一行的第几个字母]
;tf.列数  循环用计数器[当前输出的是第几列的某个字母]
;-------------------------------------------------
*start
[commit]
[er]

*将文字进行分行
[if exp="f.提示>0"]
[eval exp="tf.单行长度=f.密文.length\f.提示"]
[jump target=*提示设置 cond="f.密文.length%f.提示>0"]
[endif]

*开始处理
[eval exp="tf.栅栏=[]"]
[eval exp="tf.循环=0"]
[er]
[nowait]
栅栏易位破译：[r]
拆分：[r]
[endnowait]
*循环
;取得每行的字母
[nowait]
[eval exp="tf.栅栏[tf.循环]=f.密文.substring(tf.循环*tf.单行长度,tf.单行长度)"]
[emb exp="tf.栅栏[tf.循环]"][r]
[eval exp="tf.循环++"]
[endnowait]

[jump target=*循环 cond="tf.循环<=f.提示"]

*输出
[nowait]
明文：[r]
[endnowait]
;-------------------------------------------------------------

[eval exp="tf.循环=0"]

;当所有列输出完毕，停止输出
*输出明文
[eval exp="tf.列数=0"]
;-------------------------------------------------------------
*内循环
;输出一列的信息
[nowait]
[emb exp="tf.栅栏[tf.列数].substring(tf.循环,1)"]
[endnowait]

[eval exp="tf.列数++"]
[jump target=*内循环 cond="tf.列数<=f.提示"]
;-------------------------------------------------------------
[eval exp="tf.循环++"]
[jump target=*输出明文 cond="tf.循环<=tf.单行长度"]
;-------------------------------------------------------------
[r]
[nowait]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]

*提示设置
[er]
[nowait]
提示：字数无法被行数整除，请选择以下处理方式：[r]
[link exp="tf.单行长度++" target=*开始处理 hint="警告：可能导致错位而无法正确解读"]所有行增加一个单字的长度[endlink][r]
[link target=*逐行指定 hint="指定每一行的长度"]自行指定每一行的长度[endlink][r]
[endnowait]
[s]

*逐行指定
[er]
[nowait]
密文总长度：[emb exp="f.密文.length"][r][r]

[eval exp="tf.循环=0"]
[eval exp="tf.栅栏=[]"]
[eval exp="f.每行字数=[]"]
[endnowait]
*输入框
[nowait]
　　第[emb exp="tf.循环+1*1"]行：[edit color="0xFFFFFF" opacity=0 name="&('f.每行字数['+tf.循环+']')"][r]
[eval exp="tf.循环++"]
[endnowait]
[jump target=*输入框 cond="tf.循环<f.提示"]
[nowait]
[r]
[link target=*单行输出]确认[endlink][r]
[endnowait]
[s]


*单行输出
[commit]
[er]
[eval exp="tf.循环=0"]
[eval exp="tf.开始位置=0"]
[eval exp="tf.最长字数=0"]

[nowait]
栅栏易位破译：[r]
[r]
拆分：[r]
[endnowait]

*逐行循环
[nowait]
[eval exp="tf.每行字数=f.每行字数[tf.循环]"]
[eval exp="tf.最长字数=tf.每行字数" cond="tf.每行字数>tf.最长字数"]
@eval exp="tf.当前行=f.密文.substring(tf.开始位置,tf.每行字数)"
@emb exp="tf.当前行"
[r]
@eval exp="tf.栅栏[tf.循环]=tf.当前行"]
[endnowait]
[eval exp="tf.开始位置+=f.每行字数[tf.循环]*1"]
[eval exp="tf.循环++"]
[jump target=*逐行循环 cond="tf.循环<f.提示"]
[nowait]
[r]

[eval exp="tf.循环=0"]

明文：[r]
;当所有列输出完毕，停止输出
*输出明文二
[eval exp="tf.列数=0"]
;-------------------------------------------------------------
*内循环二
;输出一列的信息
[nowait]
[emb exp="tf.栅栏[tf.列数].substring(tf.循环,1)" cond="tf.栅栏[tf.列数].length>tf.循环"]
[endnowait]

[eval exp="tf.列数++"]
[jump target=*内循环二 cond="tf.列数<f.提示"]
;-------------------------------------------------------------
[eval exp="tf.循环++"]
[jump target=*输出明文二 cond="tf.循环<=tf.最长字数"]
;-------------------------------------------------------------
[r]
[nowait]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]








