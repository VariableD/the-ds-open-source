;-----------------------------------------------------------------
;tf.单字 当前取得的单个文字
;tf.位置 取得的字母在表格中的位置
;tf.明文 输出的明文显示
;tf.循环
;tf.位移数
;-----------------------------------------------------------------
*start
[commit]
[er]
[jump target=*输出 cond="f.密文==''"]

[eval exp="f.密文=min2cap(f.密文)"]
;密钥的处理
[eval exp="f.提示%26"]

;-----------------------------------------------------------------
;全部输出
[if exp="f.提示==0"]
[eval exp="tf.位移数=0"]
;-----------------------------------------------------------------
*循环输出
;设计对照表
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]
[eval exp="tf.明文=''"]
[eval exp="tf.循环=0"]
;-----------------------------------------------------------------
*内循环
;取得密文中的第N个字母
[eval exp="tf.单字=f.密文.substring(tf.循环,1)"]
;取得字母在表格中的位置
[eval exp="tf.位置=f.表格.indexOf(tf.单字)"]
[if exp="tf.位置==-1"]
[eval exp="tf.单字=' '"]
[jump target=*代入]
[endif]
;修改位置
[eval exp="tf.位置-=tf.位移数"]
[eval exp="tf.位置+=26" cond="tf.位置<0"]
[eval exp="tf.单字=f.表格.substring(tf.位置,1)"]
*代入
[eval exp="tf.明文+=tf.单字"]
[eval exp="tf.循环++"]
[jump target=*内循环 cond="tf.循环<f.密文.length"]
;-----------------------------------------------------------------
[nowait]
[emb exp="tf.明文"]　(位移数：[emb exp="tf.位移数"])[r]
[endnowait]

[eval exp="tf.位移数++"]
[jump target=*循环输出 cond="tf.位移数<=25"]
;-----------------------------------------------------------------
[nowait]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]

[s]
[endif]
;------------------------------------------------------------------

*单字输出
;设计对照表
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]
[eval exp="tf.明文=''"]
[eval exp="tf.循环=0"]
*循环
;取得密文中的第N个字母
[eval exp="tf.单字=f.密文.substring(tf.循环,1)"]
;取得字母在表格中的位置
[eval exp="tf.位置=f.表格.indexOf(tf.单字)"]
;没找到的情况
[if exp="tf.位置==-1"]
[eval exp="tf.单字=' '"]
[jump target=*代入二]
[endif]

;修改位置
[eval exp="tf.位置-=f.提示"]
[eval exp="tf.位置+=26" cond="tf.位置<0"]

[eval exp="tf.单字=f.表格.substring(tf.位置,1)"]

*代入二
[eval exp="tf.明文+=tf.单字"]

[eval exp="tf.循环++"]
[jump target=*循环 cond="tf.循环<f.密文.length"]

*输出
[er]
[nowait]
简单移位破译：[r]
密文：[emb exp="f.密文"][r]
明文：[emb exp="tf.明文"][r]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]





