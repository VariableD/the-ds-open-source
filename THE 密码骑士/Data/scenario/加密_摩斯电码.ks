;------------------------------------------
;摩斯电码加密[同时转化为伪二进制数字串]
;------------------------------------------
*start
[commit]
[er]
[jump target=*输出 cond="f.明文==''"]

[eval exp="f.明文=min2cap(f.明文)"]
[eval exp="tf.摩斯电码=mes2mos(f.明文)"]
[eval exp="tf.数字=mos2num(tf.摩斯电码)"]

*输出
[nowait]
摩斯电码加密：[r]
明文：[emb exp="f.明文"][r]
电码：[emb exp="tf.摩斯电码"][r]
数字：[emb exp="tf.数字"][r]
[style align="right"]
[link storage="armor.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]
