;-------------------------------------------------------------------
;利用分行进行加密
;-------------------------------------------------------------------
*start
[commit]
[er]

;创建数组用于存储对应字母
[eval exp="tf.栅栏=[]"]
[eval exp="tf.循环=0"]
[jump target=*输出 cond="f.明文==''"]
[jump target=*输出 cond="f.密钥==''"]

*循环
;依次取得每次N个字母
[eval exp="tf.单字=f.明文.substring(tf.循环,f.密钥)"]

[eval exp="tf.行数=0"]
*内循环
;将每次的N个字母分别写入栅栏的第N行
[eval exp="tf.栅栏[tf.行数]+=tf.单字.substring(tf.行数,1)"]
[eval exp="tf.行数++"]
[jump target=*内循环 cond="tf.行数<f.密钥"]
[eval exp="tf.循环+=f.密钥*1"]
[jump target=*循环 cond="tf.循环<f.明文.length"]

*结果显示
[nowait]
栅栏易位加密：[r][r]
明文：[emb exp="f.明文"][r][r]
拆分：[r]
[endnowait]

[eval exp="tf.循环=0"]
*输出密文
[nowait]
[emb exp="tf.栅栏[tf.循环]"][r]
[eval exp="tf.循环++"]
[endnowait]
[jump target=*输出密文 cond="tf.循环<f.密钥"]
[r]
[nowait]
密文：[r]
[endnowait]

[eval exp="tf.循环=0"]
*输出
[nowait]
[emb exp="tf.栅栏[tf.循环]"]
[eval exp="tf.循环++"]
[endnowait]
[jump target=*输出 cond="tf.循环<f.密钥"]
[r]
[nowait]
[style align="right"]
[link storage="armor.ks" target=*start]返回[endlink]
[style align="left"]
[endnowait]
[s]
