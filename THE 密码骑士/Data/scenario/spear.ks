;-----------------------------------------------------
;短篇密码破译
;-----------------------------------------------------
*start
[nowait]
[er]
[r]
[style align="center"]
短句密文破译[r]
[r]
[style align="left"]
请输入密文：[edit name=f.密文 length=400 opacity=0 color="0xFFFFFF"][r][r]
请输入关键字：[edit name=f.提示 opacity=0 color="0xFFFFFF"][r][r]

[link storage="破译_简单移位.ks" target=*start hint="简单的将26个字母的顺序进行移动（关键字：0-25）"]简单移位[endlink]　
[link storage="破译_埃特巴什.ks" target=*start hint="字母表正数第几位用倒数第几位替换，不需要关键字"]埃特巴什[endlink]　
[link storage="破译_恺撒单字.ks" target=*start hint="将某个关键字作为密钥，之后的字母依序后移的加密方式（关键字：密钥本身）"]恺撒单字[endlink]　
[link storage="破译_栅栏易位.ks" target=*start hint="将密文拆分成数行后解读（关键字：行数）"]栅栏易位[endlink]　
[link storage="first.ks" target=*start]返回[endlink]

[endnowait]
[s]


