;-----------------------------------------------------
;f.密文 输入的信息，等待解码的短信息
;f.提示 密钥或某些参数
;-----------------------------------------------------
*start
[call storage="macro.ks"]
[rclick enabled="false"]
;------------------------------------------------------
[backlay]
[image layer="base" storage="castle_E07_a" page="back"]
;-------------------------------------------------------
[current layer="message0" page="back"]
[position layer="message0" page="back" color="0xFFFFFF" opacity=0]
[er]
[nowait]
[style align="center"]
[r]
-THE 密码骑士-[r]
-Version 1.0 The Dancing Men-[r]
[r][r][r][r]
[link storage=dance.ks target=*start hint="图片密码制作"]The Dancing Men[endlink][r]
[link storage=armor.ks target=*start hint="信息加密处理"]Armor Factory[endlink][r]
[link storage=spear.ks target=*start hint="短句密文破译"]Spear of Longinus[endlink][r]
;[link target=*onager hint="长篇密文分析"]弩炮（未实装）[endlink][r]
[link exp="kag.shutdown()" hint="退出程序"]Exit[endlink]
[style align="left"]

[locate y=350]
[style align="center"]
[font size=15]
(C) 2008 Dual-Dimension Factory[r]
Variable D
[resetfont]
[style align="left"]
[endnowait]

[trans method="crossfade" time=500]
[wt]
[current layer="message0" page="fore"]
[s]

*onager
[jump target=*start]
