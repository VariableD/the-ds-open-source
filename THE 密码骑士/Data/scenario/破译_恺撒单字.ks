;-----------------------------------------------------------------
;tf.单字 当前取得的单个文字
;tf.位置 取得的字母在表格中的位置
;tf.明文 输出的明文显示
;tf.密钥 
;tf.循环
;f.表格 正常字母顺序 
;tf.对照 与正常字母顺序的对照表
;-----------------------------------------------------------------

*start
[commit]
[er]
[jump target=*输出 cond="f.密文==''"]
[jump target=*输出 cond="f.提示==''"]

[eval exp="f.密文=min2cap(f.密文)"]
[eval exp="f.提示=min2cap(f.提示)"]

;字母原位置
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]

;取得密钥
[eval exp="tf.密钥=''"]
[eval exp="tf.循环=0"]

*循环
;取得提示中的第N个字母
[eval exp="tf.单字=f.提示.substring(tf.循环,1)"]
;当密钥中不存在单字时，将单字加入
[eval exp="tf.密钥+=tf.单字" cond="tf.密钥.indexOf(tf.单字)==-1"]
[eval exp="tf.循环++"]
[jump target=*循环 cond="tf.循环<f.提示.length"]
;------------------------------------------------------------------------------
;将密钥中的字母从字母表中去除
[eval exp="tf.对照=tf.密钥"]
[eval exp="tf.循环=0"]
*循环去除
;取得提示中的第N个字母
[eval exp="tf.单字=f.表格.substring(tf.循环,1)"]
;当密钥中不存在单字时，将单字加入最终字母表
[eval exp="tf.对照+=tf.单字" cond="tf.密钥.indexOf(tf.单字)==-1"]
[eval exp="tf.循环++"]
[jump target=*循环去除 cond="tf.循环<26"]
;------------------------------------------------------------------------------
*明文替换
[eval exp="tf.明文=''"]
[eval exp="tf.循环=0"]

*循环替换
;取得密文中的第N个字母
[eval exp="tf.单字=f.密文.substring(tf.循环,1)"]
;取得该字在对照表中的位置
[eval exp="tf.位置=tf.对照.indexOf(tf.单字)"]
;将该字替换为明文中相同位置的字母
[eval exp="tf.单字=f.表格.substring(tf.位置,1)"]
[eval exp="tf.明文+=tf.单字"]
[eval exp="tf.循环++"]
[jump target=*循环替换 cond="tf.循环<f.密文.length"]


*输出
[nowait]
恺撒单字破译：[r]
密钥：[emb exp="tf.密钥"][r][r]
对照表：[r]
[emb exp="f.表格"][r]
[emb exp="tf.对照"][r][r]
密文：[emb exp="f.密文"][r]
明文：[emb exp="tf.明文"][r]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]

