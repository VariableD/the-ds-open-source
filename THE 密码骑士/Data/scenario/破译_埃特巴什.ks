;tf.明文
;tf.循环

*start
[commit]
[er]
[jump target=*输出 cond="f.密文==''"]

[eval exp="f.密文=min2cap(f.密文)"]

;创建对照表
[eval exp="f.表格='ABCDEFGHIJKLMNOPQRSTUVWXYZ'"]
[eval exp="f.对照='ZYXWVUTSRQPONMLKJIHGFEDCBA'"]

;------------------------------------------------------------------------------
*明文替换
[eval exp="tf.明文=''"]
[eval exp="tf.循环=0"]

*循环替换
;取得密文中的第N个字母
[eval exp="tf.单字=f.密文.substring(tf.循环,1)"]
;取得该字在对照表中的位置
[eval exp="tf.位置=f.对照.indexOf(tf.单字)"]
[jump target=*代入 cond="tf.位置==-1"]

;将该字替换为字母表中的对应字
[eval exp="tf.单字=f.表格.substring(tf.位置,1)"]

*代入
[eval exp="tf.明文+=tf.单字"]
[eval exp="tf.循环++"]
[jump target=*循环替换 cond="tf.循环<f.密文.length"]

*输出
[nowait]
埃特巴什：[r]
密文：[emb exp="f.密文"][r]
明文：[emb exp="tf.明文"][r]
[style align="right"]
[link storage="spear.ks"]返回[endlink]
[style align="left"]
[endnowait]
[s]





