;载入简单指令宏文件
[call storage="macro.ks"]

*logo
[bg storage=empty time=100]
[startanchor]

[eval exp="f.all=[]"]
[eval exp="f.all[0]='empty'"]

[rclick enabled="false"]
[history enabled="false" output="false"]

*title
[backlay]
[image layer="stage" page="back" storage="BG_fan_02"]
[layopt layer="message0" visible="false" page="back"]
[position layer="message1" color="0xFFFFFF" opacity=0 page="back" visible="true"]
[current layer="message1" page="back"]
[style align="center"]
[nowait]
[r][r][r][r][r][r][r][r][r][r][r]
[link target=*图片分解]图片分解[endlink][r]
[link target=*图片合成]图片合成[endlink][r]
[link target=*图片转换]NS->KR图片转换[endlink][r]
[link exp="kag.shutdown()"]关闭程序[endlink]
[endnowait]
[style align="left"]
[trans method="crossfade" time="500"]
[wt]
[s]

*图片分解
[jump storage="divide.ks" target=*start]
[s]
*图片转换
[jump storage="NStoKR.ks" target=*start]
[s]
*图片合成
[jump storage="combine.ks" target=*start]
[s]

