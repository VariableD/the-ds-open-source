*start
;------------------------------------------------------------
;补充系统设定
;------------------------------------------------------------
;[p]过后自动消去文字
[erafterpage mode="true"]
;------------------------------------------------------------
;宏定义
;------------------------------------------------------------
;等待
[macro name=lr]
[l][r]
[endmacro]

;显示背景
[macro name=bg]
[backlay]
[image layer=stage storage=%storage page=back visible="true" left=0 top=0 grayscale=%grayscale|false mcolor=%mcolor mopacity=%mopacity]
[trans method=%method|crossfade time=%time|700 rule=%rule stay=%stay from=%from]
[wt canskip=%canskip|true]
[endmacro]

;显示人物
[macro name=fg]
[backlay]
[image layer=%layer|0 storage=%storage page=back visible="true" pos=%pos|center]
[trans method=%method|crossfade time=%time|500]
[wt canskip=%canskip|true]
[endmacro]

;消除背景
[macro name=clbg]
[backlay]
[freeimage layer=stage page=back]
[trans method=%method|crossfade time=%time|500 rule=%rule stay=%stay from=%from]
[wt canskip=%canskip|true]
[endmacro]

;消除人物
[macro name=clfg]
[backlay]
[freeimage layer=%layer|0 page=back]
[trans method=%method|crossfade time=%time|300 rule=%rule stay=%stay from=%from]
[wt canskip=%canskip|true]
[endmacro]

;自定义对话框[小]
[macro name=dia]
[backlay]
[position page="back" layer="message0" visible="true" color=0x000000 opacity=128 left=25 top=400 width=750 height=175 marginl=10 marginr=10 margint=10 marginb=10]
[trans method="crossfade" time=200]
[wt]
[current layer="message0" page="fore"]
[endmacro]
;自定义对话框[大]
[macro name=scr]
[backlay]
[position page="back" layer="message0" visible="true" color=0x000000 opacity=128 left=25 top=25 width=750 height=550 marginl=25 marginr=25 margint=25 marginb=25]
[trans method="crossfade" time=200]
[wt]
[current layer="message0" page="fore"]
[endmacro]
;自定义对话框[全屏]
[macro name=menu]
[backlay]
[position page="back" layer="message0" visible="true" color=0x000000 opacity=0 left=0 top=0 width=800 height=600 marginl=0 marginr=0 margint=0 marginb=0]
[trans method="crossfade" time=200]
[wt]
[current layer="message0" page="fore"]
[endmacro]
;------------------------------------------------------------

[iscript]

//---------------------------------------------------------------------------------------------------
//分割图片用图层类
//---------------------------------------------------------------------------------------------------
class FireDropLayer extends KAGLayer
{
var x;
var y;

     //创建函数
     function FireDropLayer(win, par)
     {
     	super.KAGLayer(win, par);
		absolute=1000;
		left=200;
		top=0;
		x=left;
		y=top;
		visible=true;
		drawFrame(f.y,f.x);
     }	
     //销毁函数
	function finalize()
	{
	   super.finalize(...);
	}
	
	//描绘格数
	function drawFrame(xnum=1,ynum=1)
	{

		var layer=kag.fore.layers[1];

		layer.loadImages(%["storage"=>"empty", "visible"=>true]);
		//横
		layer.colorRect(x, y, width, 1        , 0xFF0000, 128);
		//竖
		layer.colorRect(x, y, 1        , height, 0xFF0000, 128);

         for (var i=1; i<=xnum; i++)
           {
           	 layer.colorRect(x, y*1+(height\xnum)*i-1,  width, 1, 0xFF0000, 128);
           }
         for (var i=1; i<=ynum; i++)
           {
           	 layer.colorRect(x*1+(width\ynum)*i-1, y, 1, height, 0xFF0000, 128);
           }
	}
}

//---------------------------------------------------------------------------------------------------
//分离输出图片
//---------------------------------------------------------------------------------------------------
    function dividePicture()
    {
         for (var k=0;k<f.all.count;k++)
     {
        f.载入层.loadImages(f.all[k]);
        f.载入层.setSizeToImageSize();
        var out;
        var clipwidth=f.载入层.width/f.x;
        var clipheight=f.载入层.height/f.y;
    	
		var num=1;

           //外循环
          for (var i=0;i<f.y;i++)
        {
              //内循环
               for (var j=0;j<f.x;j++)
            {
                out=new Layer(kag, kag.fore.base);
                out.setImageSize(clipwidth,clipheight);
                out.operateRect(0,0,f.载入层,clipwidth*j,clipheight*i,clipwidth,clipheight);
				var file=Storages.chopStorageExt(Storages.extractStorageName(f.all[k]));
				var str=(string)num;
				if (num<10) str="0"+str;
                var path = System.exePath+"/savedata/"+file+"/"+file+"_" +str+".png";
                out.saveLayerImagePng(path);
				num++;
            }
         }
      }
    }
//---------------------------------------------------------------------------------------------------
//NS->KR处理
//---------------------------------------------------------------------------------------------------
    function NStoKr()
    {
     for (var k=0;k<f.all.count;k++)
     {
        f.载入层.loadImages(f.all[k]);
        f.载入层.setSizeToImageSize();
        
        var out;
        var path;
        var frame=f.x*2;
        var clipwidth=f.载入层.width/frame;
        var clipheight=f.载入层.height;
        
         //内循环
          for (var j=0;j<frame;j++)
            {
                out=new Layer(kag, kag.fore.base);
                out.setImageSize(clipwidth,clipheight);
                out.operateRect(0,0,f.载入层,clipwidth*j,0,clipwidth,clipheight);
               if (j%2==0) 
               {
                 path = System.exePath+Storages.chopStorageExt(Storages.extractStorageName(f.all[k]))+"_"+j\2+".bmp";
               }
               if (j%2!=0)
                {
                  out.adjustGamma(1, 255, 0, 1, 255, 0, 1, 255, 0);
                  path = System.exePath+Storages.chopStorageExt(Storages.extractStorageName(f.all[k]))+"_"+(j-1)\2+"_m.bmp";
                }
               out.saveLayerImage(path, "bmp24");
               invalidate out;
            }
        
     }	 
    }
//---------------------------------------------------------------------------------------------------
//图片合成相关
//---------------------------------------------------------------------------------------------------
function loadPicture()
{
var temp;
var clipwidth;
var clipheight;

f.合成层.fillRect(0, 0, 1, 1, dfAlpha);

//取得每帧长宽
    for (var i=0;i<f.all.count;i++)
    {
          temp=new Layer(kag, kag.fore.base);
          temp.loadImages(f.all[i]);
          temp.setSizeToImageSize();
          if (clipwidth<temp.width) clipwidth=temp.width;
          if (clipheight<temp.height) clipheight=temp.height;
    }

    f.x=f.all.count\f.y;
    if (f.all.count%f.y!=0) f.x++;
    f.合成层.setSize(clipwidth*f.y, clipheight*f.x);
    f.合成层.drawFrame(f.x,f.y);

          var x=-1;
          var y=0;
          
for (var i=f.all.count-1;i>=0;i--)
  {
          x++;
          if (x>=f.y) {x=0;y+=1;}
          
          temp=new Layer(kag, kag.fore.base);
          temp.loadImages(f.all[i]);
          temp.setSizeToImageSize();

          f.合成层.operateRect(clipwidth*x,clipheight*y,temp,0,0,clipwidth,clipheight);

          
    }
}
//---------------------------------------------------------------------------------------------------
 //文件拖拽响应
 //---------------------------------------------------------------------------------------------------
    function onFileDrop(files)
    {
    //记录载入的图片名
	f.all=files;
	
    //图片分解用
	if (f.载入层!=void)
	{
	  f.载入层.loadImages(files[0]);
	  f.载入层.setSizeToImageSize();
	  f.载入层.drawFrame(f.y,f.x);
	}
   //图片合成用
	if (f.合成层!=void)
	{
	  loadPicture();
	}
    }  
kag.onFileDrop = onFileDrop;
[endscript]
;----------------------
[return]
