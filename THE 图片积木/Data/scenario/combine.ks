*start
[image layer="stage" storage="bgd_dot"]
[eval exp="f.x=1"]
[eval exp="f.y=1"]
[iscript]
f.合成层=new FireDropLayer(kag, kag.fore.base);
[endscript]
;-------------------------------------------------------
*刷新画面
[current layer="message1" page="fore"]
[er]
[position layer="message0" page="fore" visible="true" width=200 marginl=25 margint=25 marginb=25 marginr=25 color="0x000000" opacity=128]

[current layer="message0" page="fore"]
[er]
[nowait]
[style align="center"]
图片合成[r][r]
请将要处理的图片拖拽到窗口上来
[style align="left"]

[r][r]
每行帧数：[r]
[edit name=f.y bgcolor="0x000000" opacity=0 color="0xFFFFFF" length=160][r]
[r]
[style align="center"]
[link target=*格数修改]格数修改[endlink][r]
[link target=*图片输出]图片输出[endlink][r]
[r]
[link storage="first.ks" exp="invalidate f.合成层;f.合成层=void;kag.goToStart()"]返回标题[endlink][r]
[style align="left"]
[s]

*格数修改
[commit]
[eval exp="f.y=1" cond="f.y<=0"]
[iscript]
f.合成层=new FireDropLayer(kag, kag.fore.base);
    loadPicture();
[endscript]
[jump target=*刷新画面]


*图片输出
[iscript]
var  path = System.exePath+"/savedata/combine/"+Storages.chopStorageExt(Storages.extractStorageName(f.all[-1]))+".png";
f.合成层.saveLayerImagePng(path);
[endscript]
[jump target=*刷新画面]
